# Setup Prerequisites#
* This project is built using the [Ionic Framework](https://ionicframework.com/docs/).
* You will need to install [Node.js](https://nodejs.org/en/) and NPM before running this app.
* Python2
* CocoaPods

## Requirements ##

A few items are required in order to build and run this app.  

Various APIs and Resources used  
Required:  
PIKA

Optional:  
Wordpress - Wordpress API is currently used for alerts and news items  
libcal - is used to provide information regarding libraries.  
datahub - datahub is our own in house resource that is used for push notifications and events.  

Android Devices  
Refer to Ionic Docs https://ionicframework.com/docs/intro/deploying/  
1. Java JDK  
2. Android Studio  

iOS Devices  
1. Xcode  

---

## File Structure ##

### Dangerous Files ###
* Several directories should NOT be modified unless you really know what you're doing. These are managed by Node and Ionic.   
  `node_modules`  
  `platforms`  
  `plugins`  
  `www`

### Working Files ###
* Most work will be done in the src folder. There you will find all the components, services, etc that makeup an angular app.
* The resources folder is used to update your app's splash screen and icons for the app. 

---

## Setup ##
* AppConfig.ts needs to be configured to turn features on/off and include your own resources. Arlington County has provided a sample for reference. 
* This app uses Google Analytics however it is not required. If you wish to use google analytics, plugin your `TRACKING_IOS_ID` and `TRACKING_ANDROID_ID` and change `APP_ENABLE_GOOGLE_ANALYTICS` to true in the config file.
* Library Locations - ??
* PIKA_BASE_URL is a required resource and should be set to your root url i.e "https://libcat.arlingtonva.us/"
* The rest of the items in the config file are optional but will require further setup on your end to ensure they will work. 

---

## Building for Android ##
If iOS platform has already been added, a couple plugins need to be removed in order for the Android platform to build correctly.  
NOTE: In order to build for android you will need to have a google-services.json file. Refer to [Android Guide](https://developers.google.com/android/guides/google-services-plugin)  
`ionic cordova platform rm ios`  
`ionic cordova plugin rm ionic-plugin-keyboard`  
`ionic cordova plugin rm phonegap-plugin-push`  

`ionic cordova platform add android@6.2.3`  
`ionic cordova plugin add phonegap-plugin-push@2.0.0`  
`ionic cordova build android`  
to build for production version add flags  
`ionic cordova build android --prod --release`

## Running Android ## 
`ionic cordova run android --device`

---

## Build for iOS
If Android platform has already been added, a couple plugins need to be removed in order for the iOS platform to build correctly.  
`ionic cordova platform rm android`  
`ionic cordova plugin rm phonegap-plugin-push`  
  
`ionic cordova platform add ios`  
`ionic cordova plugin add phonegap-plugin-push`  
`ionic cordova plugin add ionic-plugin-keyboard`  
`ionic cordova build ios`  
to build for production version add flags  
`ionic cordova build ios --prod --release`

## Running for iOS ## 
* After building file, open the xcworkspace file in xCode
* Select an emulator or a plugged in device and hit run

---

### Troubleshooting steps ###
Error: UnhandledPromiseRejectionWarning: Error: /Applications/Android Studio.app/Contents/gradle/gradle-4.4/bin/gradle: Command failed with exit code EACCES
`sudo chmod 755 "/Applications/Android Studio.app/Contents/gradle/gradle-4.4/bin/gradle"`

Error: xcode-select: error: tool 'xcodebuild' requires Xcode, but active developer directory '/Library/Developer/CommandLineTools' is a command line tools instance
`sudo xcode-select -s /Applications/Xcode.app/Contents/Developer`

Error: UnhandledPromiseRejectionWarning: SyntaxError: Unexpected token I in JSON at position 0
`ionic cordova platform rm ios`  
`ionic cordova platform add ios`  
---

## Deploying ##

### iOS ###
* After building, open the app's .xcworkspace file in xCode.
* In xCode, select "Generic iOS Device" from device list
* Click 'Project' at the top, then select archive. This will create an archive an open a new window.
* Select your archive and click 'Upload to Store' and follow instructions.
* Go to [iTunes Connect](https://itunesconnect.apple.com) and follow instructions to deploy.


### Android ###
* After building, there will be an android-release-unsigned.apk file. This file needs to be zipaligned and then signed. 
* Refer to ionic docs for zip aligning and signing. https://ionicframework.com/docs/intro/deploying/
* After signing your file, go to [google play developer console](https://play.google.com/apps/publish/) and follow instructions to deploy.