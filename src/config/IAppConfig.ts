export interface IAppConfig { 
    PIKA_BASE_URL:string;
    TRACKING_IOS_ID:string;
    TRACKING_ANDROID_ID:string;
    LIBRARY_WEBSITE_URL:string;
    LIBRARY_ECOLLECTION_URL:string;
    LIBCAL_BASE_URL:string;
    LIBRARY_LOCATIONS:any[];
    APP_TITLE_HTML:string;
    APP_COPYRIGHT:string;
    APP_NAME:string;
    HELP_ACCOUNT_URL:string;
    HELP_FEEDBACK_EMAIL:string;
    WORDPRESS_BASE_URL:string;
    LIBSYS_BASE_URL:string;
    DATAHUB_BASE_URL:string;
    MEETING_ROOMS_URL:string;
    APP_SHOW_EVENTS:boolean;
    APP_SHOW_ALERTS:boolean;
    APP_SHOW_NEWS:boolean;
    APP_ENABLE_GOOGLE_ANALYTICS:boolean;
    APP_ENABLE_MEETING_ROOMS:boolean;
    APP_ENABLE_PUSH_NOTIFICATIONS:boolean;
}