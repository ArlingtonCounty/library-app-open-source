import { IAppConfig } from './IAppConfig';

export const AppConfig:IAppConfig = {
    //GOOGLE Analytics
    TRACKING_IOS_ID: "",
    TRACKING_ANDROID_ID: "",
    
    //Location Settings
    LIBRARY_LOCATIONS: [
        { campusId: "a", name: "Aurora Hills Branch Library" },
        { campusId: "c", name: "Central Library" },
        { campusId: "h", name: "Cherrydale Branch Library" },
        { campusId: "o", name: "Columbia Pike Branch Library" },
        { campusId: "u", name: "Connection Crystal City" },
        { campusId: "g", name: "Glencarlyn Branch Library" },
        { campusId: "p", name: "Plaza Branch Library" },
        { campusId: "s", name: "Shirlington Branch Library" },
        { campusId: "w", name: "Westover Branch Library" }
    ],
    //Labels
    APP_TITLE_HTML: 'ARLINGTON <span class="bold-headline">PUBLIC LIBRARY</span>',
    APP_COPYRIGHT: "Copyright 2017 Arlington Public Library",
    APP_NAME: "Arlington Public Library app",
    
    //External Resources
    PIKA_BASE_URL: "https://libcat.arlingtonva.us/",
    LIBCAL_BASE_URL: "",
    LIBRARY_WEBSITE_URL: "",
    LIBRARY_ECOLLECTION_URL: "",
    HELP_ACCOUNT_URL: "",
    HELP_FEEDBACK_EMAIL: "",
    WORDPRESS_BASE_URL: "",
    LIBSYS_BASE_URL: "",

    // Resource for managing push notifications and events
    DATAHUB_BASE_URL: "",
    // Resource for managing meeting rooms. Identity server
    MEETING_ROOMS_URL: "",
    
    //Turn features on/off
    APP_SHOW_EVENTS: false,
    APP_SHOW_ALERTS: false,
    APP_SHOW_NEWS: false,
    APP_ENABLE_GOOGLE_ANALYTICS: false,
    APP_ENABLE_MEETING_ROOMS: false,
    APP_ENABLE_PUSH_NOTIFICATIONS: false
}