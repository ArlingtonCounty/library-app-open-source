import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, Events, AlertController, App } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Observable } from 'rxjs/Observable';
import { select } from '@angular-redux/store';

import { HomePage, HelpPage, LibrariesPage, PushNotificationsPage, AccountPage, MeetingRoomsPage, CatalogPage } from '../components/components';
import { PushNotificationsProvider } from '../components/settings/pushNotificationsProvider';
import { AnalyticsProvider } from '../providers/providers';

import { NewPage } from '../components/components'
import { AppConfig } from '../config'

import './rxjs-operators';

export interface ITracking {
  track() : void
}

@Component({
  templateUrl: 'app.html',
})
export class MyApp {
  rootPage = HomePage;
  @ViewChild(Nav) nav: Nav;
  copyright:string;
  menuItems:any[];

  @select('newsItem')
  readonly newsItem$: Observable<any>;

  constructor(private platform: Platform, 
              private pushNotificationsProvider:PushNotificationsProvider, 
              events:Events,
              private alertCtrl:AlertController,
              app:App,
              private analytics:AnalyticsProvider,
              private statusBar:StatusBar,
              private splashScreen:SplashScreen) {
    var self = this;
    platform.ready().then(() => {
      this.copyright = AppConfig.APP_COPYRIGHT;
      this.menuItems = [
        { name: "Home", page: null},
        { name: "Catalog Search", page: CatalogPage},
        { name: "My Accounts", page: AccountPage},
        { name: "Library Locations", page: LibrariesPage },
        { name: "Help", page: HelpPage },
        { name: "eCollection", page: null, href: AppConfig.LIBRARY_ECOLLECTION_URL },
        { name: "Full Site", page: null, href: AppConfig.LIBRARY_WEBSITE_URL }
      ];
      
      if (AppConfig.APP_ENABLE_PUSH_NOTIFICATIONS) {
        var pushNotificationPage = { name: "Push Notifications", page: PushNotificationsPage };
        this.menuItems.splice(4, 0, pushNotificationPage)
      }
      if (AppConfig.APP_ENABLE_MEETING_ROOMS) {
        var meetingRoomPage = { name: "Meeting Rooms", page: MeetingRoomsPage };
        this.menuItems.splice(4, 0, meetingRoomPage)
      }

      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      if (AppConfig.APP_ENABLE_PUSH_NOTIFICATIONS) {
        pushNotificationsProvider.register(function(id) {
          self.nav.push(AccountPage, { "accountId": id });
        });
      }

      if(platform.is('ios') || platform.is('android')){
        try {
          analytics.startTracker();
        }
        catch (ex){
          console.log(ex)
        }
      }

      if (this.platform.is('android')) {
        this.statusBar.styleLightContent();
      } else {
        this.statusBar.styleDefault();
      }
      this.splashScreen.hide();

      this.newsItem$.subscribe(newsItem => {
        if (newsItem && newsItem.hasOwnProperty('title')) {
          let confirm = this.alertCtrl.create({
            title: `View new alert?`,
            buttons: [
                {
                    text: 'Cancel',
                    handler: () => {
                        console.log('canceled')
                    }
                },
                {
                    text: 'Yes',
                    handler: () => {
                      this.nav.push(NewPage, {newsItem: newsItem});
                    }
                }
            ]
          });
          confirm.present();
        }
      })
      
    }); 
  }

  go(menuItem) {
    if(menuItem.page){
      this.nav.push(menuItem.page);
    }
    else if(menuItem.href) {
      window.open(menuItem.href, '_system');
    }
    else if(!(this.nav.getActive().instance instanceof HomePage)) {
      this.nav.popToRoot();
    }
    else if(this.nav.getActive().instance instanceof HomePage) {
      console.log('Already on home page');
    }
    else {
      let alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: 'Unimplemented.',
          buttons: ['OK']
      });
      alert.present();
    }
  }

  ngAfterViewInit() {
    this.platform.ready().then(() => {
      if(this.platform.is('ios') || this.platform.is('android')) {
        this.nav.viewDidEnter.subscribe((data) => {
            //Check for component Tracking
            //Execute component specific tracking if available
            if(data.instance.track) {
              data.instance.track();
            } 
            //Othwerwise, default to 
            else {
                this.analytics.trackView("Unknown");
                alert("Component not setup for tracking");
            }
        }, function(error){
          console.log("Tracking Exception", error)
        });
      }
    });
  }
}
