

export class NavParamsMock {
    public data:Object = {};
    public get(key): any {
        return this.data[key];
    }
    public set(key, value) {
        this.data[key] = value; 
    }
}

export class LoadingControllerMock {
    create() { 
        return {
            present() { },
            dismissAll() { }
        }
    }
}

export class PlatformMock {
    is(platform:string) : boolean {
        return true;
    }
}

export class NavControllerMock { 

    push(page, obj){
        
    }
}