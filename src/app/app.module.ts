import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { BrowserModule } from "@angular/platform-browser";
import { HttpModule } from '@angular/http';

import { Ng2Webstorage } from 'ng2-webstorage';
import { NgCalendarModule  } from 'ionic2-calendar';

import { Device } from '@ionic-native/device';
import { Diagnostic } from '@ionic-native/diagnostic';
import { EmailComposer } from '@ionic-native/email-composer';
import { Geolocation } from '@ionic-native/geolocation';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Keyboard } from '@ionic-native/keyboard';
import { Push } from '@ionic-native/push';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { File } from '@ionic-native/file';

import { NgRedux, NgReduxModule } from '@angular-redux/store';
import { store } from '../store/store';
import { IAppState } from '../store/IAppState';

import { AppConfig } from '../config';

// pages
import {
  HomePage,
  IntroPage,
  LibrariesPage,
  LibraryPage,
  AccountPage,
  BookPage,
  HoldsPage,
  FreezeSelected,
  AccountsManagementPage,
  CheckoutsPage,
  FinesPage,
  ReorderPage,
  UpdatePage,
  FAQPage,
  HelpPage,
  CatalogPage,
  BrowsePage,
  ListPage,
  CatalogFilterPage,
  PushNotificationsPage,
  EventsPage,
  EventPage,
  EventsFilterPage,
  NewsPage,
  NewPage,
  LoginPage,
  RequestHoldPage,
  MeetingRoomsPage,
  ReservePage,
  CategoriesPage,
  FormatPage,
  BaseLibraryItem,
  CheckoutOverdrivePage,
  AccountListPage,
  BarcodePage,
  ListTitlesPage } from '../components/components'


import {
  // web service providers
  SierraProvider,
  PikaProvider,
  PikaTempProvider,
  LibcalProvider,

  // functionality providers
  AccountService,
  ItemProvider,
  EventsProvider,
  StorageProvider,
  LibcalProxyProvider,
  WordpressProvider,
  LinkProvider,
  LibraryInfoProvider,
  AnalyticsProvider } from '../providers/providers'

// Component Providers
import {
  CheckoutsProvider,
  HoldsProvider,
  LibrariesProvider,
  FinesProvider,
  PushNotificationsProvider,
  CatalogProvider,
  AlertsProvider,
  FaqProvider,
  NewsProvider,
  MeetingRoomsProvider,
  AccountActions,
  PushActions,
  ListActions } from '../components/components'

// custom pipes
import { CustomDatePipe } from '../pipes/pipes';

// directives
import { SearchReadOnly } from '../directives/searchReadOnly';
import { Pdf417 } from '../directives/pdf417';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    IntroPage,
    CatalogPage,
    CatalogFilterPage,
    AccountPage,
    LibrariesPage,
    LibraryPage,
    LoginPage,
    HoldsPage,
    FreezeSelected,
    BookPage,
    CustomDatePipe,
    EventsPage,
    EventPage,
    BrowsePage,
    NewsPage,
    NewPage,
    EventsFilterPage,
    AccountsManagementPage,
    CheckoutsPage,
    FinesPage,
    ReorderPage,
    UpdatePage,
    ListPage,
    HelpPage,
    FAQPage,
    ReservePage,
    SearchReadOnly,
    PushNotificationsPage,
    RequestHoldPage,
    MeetingRoomsPage,
    CategoriesPage,
    FormatPage,
    BaseLibraryItem,
    CheckoutOverdrivePage,
    BarcodePage,
    Pdf417,
    AccountListPage,
    ListTitlesPage
  ],
  imports: [
    Ng2Webstorage,
    NgCalendarModule,
    IonicModule.forRoot(MyApp, {
      mode: 'md'
    }),
    BrowserModule,
    HttpModule,
    NgReduxModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    IntroPage,
    CatalogPage,
    CatalogFilterPage,
    AccountPage,
    LibrariesPage,
    LibraryPage,
    LoginPage,
    HoldsPage,
    FreezeSelected,
    BookPage,
    BrowsePage,
    EventPage,
    EventsPage,
    EventsFilterPage,
    ReservePage,
    AccountsManagementPage,
    NewsPage,
    NewPage,
    CheckoutsPage,
    ListPage,
    FinesPage,
    ReorderPage,
    UpdatePage,
    HelpPage,
    FAQPage,
    PushNotificationsPage,
    RequestHoldPage,
    MeetingRoomsPage,
    CategoriesPage,
    FormatPage,
    BaseLibraryItem,
    CheckoutOverdrivePage,
    BarcodePage,
    AccountListPage,
    ListTitlesPage
  ],
  providers: [
    SierraProvider,
    PikaProvider,
    LibcalProvider,
    AccountService,
    LibrariesProvider,
    CatalogProvider,
    ItemProvider,
    EventsProvider,
    ItemProvider,
    CheckoutsProvider,
    HoldsProvider,
    FinesProvider,
    PushNotificationsProvider,
    StorageProvider,
    AlertsProvider,
    FaqProvider,
    NewsProvider,
    LibcalProxyProvider,
    MeetingRoomsProvider,
    WordpressProvider,
    LinkProvider,
    LibraryInfoProvider,
    AnalyticsProvider,
    PikaTempProvider,
    AccountActions,
    PushActions,
    ListActions,
    //native providers
    Device,
    Diagnostic,
    EmailComposer,
    Geolocation,
    InAppBrowser,
    Keyboard,
    Push,
    StatusBar,
    SplashScreen,
    ScreenOrientation,
    { provide: "AppConfig", useValue: AppConfig },
    File
  ]
})
export class AppModule {
  constructor(ngRedux: NgRedux<IAppState>) {
    ngRedux.provideStore(store);
  }
}
