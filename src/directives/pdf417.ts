import {Directive, Renderer, ElementRef, Input} from "@angular/core";

declare var PDF417; 


@Directive({
  selector : '[pdf417]'
})
export class Pdf417 {
  @Input('pdf417') public barcodeText: string;

  constructor(public renderer: Renderer, public elementRef: ElementRef) {}

  ngOnChanges(changes){
    this.updateBarcode();
  }

  updateBarcode(){
        console.log("barcode changes");
        PDF417.init(this.barcodeText);             

        var barcode = PDF417.getBarcodeArray();

        // block sizes (width and height) in pixels
        var bw = 2;
        var bh = 2;

        // create canvas element based on number of columns and rows in barcode
        var container = this.elementRef.nativeElement;
        
        if(container.firstChild){
            container.removeChild(container.firstChild);
        }

        var canvas = document.createElement('canvas');
        canvas.width = bw * barcode['num_cols'];
        canvas.height = bh * barcode['num_rows'];
        container.appendChild(canvas);

        var ctx = canvas.getContext('2d');                    

        // graph barcode elements
        var y = 0;
        // for each row
        for (var r = 0; r < barcode['num_rows']; ++r) {
            var x = 0;
            // for each column
            for (var c = 0; c < barcode['num_cols']; ++c) {
                if (barcode['bcode'][r][c] == 1) {                        
                    ctx.fillRect(x, y, bw, bh);
                }
                x += bw;
            }
            y += bh;
        }

  }
}