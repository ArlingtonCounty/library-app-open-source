import {Directive, Renderer, ElementRef} from "@angular/core";
@Directive({
  selector : '[readonly]'
})
export class SearchReadOnly {
  constructor(public renderer: Renderer, public elementRef: ElementRef) {}

  ngOnInit() {
    //search bar is wrapped with a div so we get the child input
    const searchInput = this.elementRef.nativeElement.querySelector('input');
    setTimeout(() => {
      //delay required or ionic styling gets finicky
      this.renderer.setElementAttribute(searchInput, "readonly", "true");
    }, 0);
  }
}