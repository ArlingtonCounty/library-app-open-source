import { IAppState } from './IAppState';
import {
     REMOVE_ACCOUNT
    , GET_ACCOUNTS
    , GET_PROFILE
    , SET_SELECTED_USERNAME
    , GET_CHECKOUTS
    , SET_ACCOUNT_LOADING
    , GET_LISTS
    , ADD_ACCOUNT
    , UPDATE_ACCOUNT } from '../components/account/account.actions';

import {
    GET_LIST_TITLES
} from '../components/account/list/list.actions';

import {
    GET_NEWS_ITEM
} from '../components/settings/push.actions';

const initialState = {
  profile: null,
  localAccounts: [],
  accountsLoaded: false,
  selectedAccountUsername: "",
  checkouts: null,
  accountLoading: false,
  lists: null,
  newsItem: null
}

function getProfile(state, action) : IAppState {
    return Object.assign({}, state, {
        profile: action.profile,
        accountLoading: state.accountLoading && state.checkouts === null
    })
}

function getAccounts(state, action) : IAppState {
    return Object.assign({}, state, {
        localAccounts: action.accounts,
        accountsLoaded: true
    });
}

function setSelectedAccount(state, action) : IAppState {
    if(state.selectedAccountUsername !== action.username) {
        return Object.assign({}, state, {
            selectedAccountUsername: action.username,
            profile: null,
            checkouts: null,
            accountLoading: true
        });
    } else {
        return state;
    }
}

function removeAccount(state, action) : IAppState {
    return Object.assign({}, state, {
        localAccounts: state.localAccounts.filter(account => account.barcode != action.account.barcode)
    });
}

function addAccount(state, action) : IAppState {
  return Object.assign({}, state, {
    localAccounts: [...state.localAccounts, action.account],
    profile: action.profile || null,
    accountLoading: false,
    selectedAccountUsername: action.account.username
  })
}

function updateAccount(state, action) : IAppState {
  var newState = Object.assign({}, state, {
    localAccounts: state.localAccounts.map(a => {
      if(a.username === action.account.username) {
        return action.account;
      }
      else {
        return a;
      }
    })
  });
  console.log("New State", newState)
  return newState;
}

function getCheckouts(state, action) : IAppState {
    return Object.assign({}, state, {
        checkouts: action.checkouts,
        accountLoading: state.accountLoading && state.profile === null
    })
}

export function reducer(state = initialState, action) {
    console.log(state, action);
    switch(action.type) {
        case REMOVE_ACCOUNT:
            return removeAccount(state, action);
        case ADD_ACCOUNT:
            return addAccount(state, action);
        case UPDATE_ACCOUNT:
          return updateAccount(state, action);
        case GET_ACCOUNTS:
            return getAccounts(state, action);
        case SET_SELECTED_USERNAME:
            return setSelectedAccount(state, action);
        case GET_PROFILE:
            return getProfile(state, action);
        case GET_CHECKOUTS:
            return getCheckouts(state, action);
        case SET_ACCOUNT_LOADING:
            return Object.assign({}, state, {accountLoading: true, profile: null, checkouts: null});
        case GET_LISTS:
            return Object.assign({}, state, {lists: action.lists});
        case GET_LIST_TITLES:
            return Object.assign({}, state, {selectedListTitles: action.titles});
        case GET_NEWS_ITEM:
            return Object.assign({}, state, {newsItem: action.newsItem});
        default:
            return state;
    }
}
