import { Profile, LocalAccount } from '../providers/accountService';

export interface IAppState {
    accountLoading: boolean,
    profile: Profile,
    localAccounts: LocalAccount[],
    accountsLoaded: boolean,
    selectedAccountUsername: string,
    checkouts: any[],
    lists: any[],
    newsItem: any
};
