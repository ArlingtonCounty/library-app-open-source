import { Injectable } from '@angular/core';
import { AppConfig } from '../config';

@Injectable()
export class LibraryInfoProvider {

    constructor() { }

    returnLibraryLocationTags() {
        return AppConfig.LIBRARY_LOCATIONS;
    }
}