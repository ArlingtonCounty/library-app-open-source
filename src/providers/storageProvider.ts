import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { LocalStorageService } from 'ng2-webstorage/dist/services/localStorage';
import { Observable, Subject } from 'rxjs';
import { LocalAccount } from './accountService';
import { File } from '@ionic-native/file';
import { NgRedux } from '@angular-redux/store';
import { IAppState } from '../store/IAppState';
import { Http } from '@angular/http';

declare var window;

@Injectable()
export class StorageProvider {
    private db:any;
    private migrationComplete:Subject<any>;

    constructor(private localStorage:LocalStorageService, private file: File, private platform:Platform, private ngRedux:NgRedux<IAppState>, private http:Http) {
      this.db = window.openDatabase("data.db", "1", "data", 1024 * 1024);
      this.db.transaction(tx => {
          tx.executeSql("CREATE TABLE IF NOT EXISTS settings (showIntro boolean)");
          tx.executeSql("CREATE TABLE IF NOT EXISTS accounts (id INTEGER PRIMARY KEY ASC, username TEXT, sortOrder INTEGER, data TEXT)");
          tx.executeSql("CREATE TABLE IF NOT EXISTS dismissedalerts (alertId INTEGER)");
      });

      platform.ready().then(() => {
        file.createDir(this.file.dataDirectory, 'users', false)
          .then(_ => {
            this.migrateToFileAPI();
          })
          .catch(err => {
            console.log('User Directory already exists!');
            this.migrateToFileAPI();
          });
        });
    }

    private retrieve(key:string) : any {
        var obj = this.localStorage.retrieve(key);
        return obj;
    }

    private store(key:string, obj:any) {
        this.localStorage.store(key,obj);
    }

    migrateToFileAPI() {
      this.db.transaction(tx => {
        tx.executeSql("SELECT * FROM accounts ORDER BY sortOrder", [], (transaction, result) => {
            console.log("Accounts to migrate", result);
            var accountSubscriptions = [];

            for(var i = 0; i < result.rows.length; i++) {
                var a = JSON.parse(result.rows.item(i).data);
                var account = new LocalAccount(a.username, a.password, a.displayName, a.nickname, a.patronId, a.checkoutsAlert, a.checkoutsWarning, a.finesAlert, a.holdsAlert, a.expiresAlert, a.barcode, a.homeLibraryCode, a.patronNumber);
                console.log("Adding to file system.", account);

                this.addAccount(account).subscribe(localAccount => {
                  if(localAccount) {
                    console.log("Account migrated!", localAccount);
                    this.ngRedux.dispatch({
                      type: 'account/ADD',
                      account: localAccount,
                      profile: null,
                      username: null
                    })

                    this.db.transaction(tx => {
                      tx.executeSql("DELETE FROM accounts WHERE username=?", [localAccount.username], (transaction, result)=> {
                        console.log("Finished Migrating", localAccount.username);
                      });
                  });
                }
              });
            }
        });
      });
    }


    // migrate local storage data into sql
    migrateLocalStorage(): Observable<boolean> {
        var subject = new Subject();
        var accounts = this.retrieve('accounts');
        if (accounts) {
            accounts.forEach(account => {
                this.addAccount(account).map(f => {
                    console.log(f)
                })
            })
        }
        this.store('accounts', '');

        var alerts = this.retrieve('dismissedAlerts');
        if (alerts) {
            alerts.forEach(id => {
                this.addDismissedAlertIds(id).map(f => {
                    console.log(f)
                })
            })
        }
        this.store('dismissedAlerts', '');

        var intro = this.retrieve('goToIntro');
        if (intro === false) {
            this.showIntro().map(f => {
                });
            this.updateShowIntro(false).map(usi => {
                console.log(usi)
            })
        }
        this.store('goToIntro', '')

        subject.complete()
        return subject;
    }

    //ALERTS CRUD Operations
    getDismissedAlertIds() : Observable<Number[]>{
        var subject = new Subject();
        var returnArray = []
        this.db.transaction(tx => {
            tx.executeSql("SELECT * FROM dismissedalerts", [], (transaction, result) => {
                for(var i = 0; i < result.rows.length; i++) {
                    returnArray.push(result.rows.item(i).alertId);
                }
                subject.next(returnArray);
            });
        });

       return subject;
    }

    //Account CRUD Operations
    addDismissedAlertIds(id) : Observable<{}> {
        var subject = new Subject();
        this.db.transaction(tx => {
            tx.executeSql(`INSERT INTO dismissedalerts (alertId) VALUES (${id})`, [], (transaction, result)=> {
                subject.next(true);
            });
        });

        return subject;
    }

    //INTRO CRUD Operations
    showIntro() : Observable<boolean> {
        var subject = new Subject();
        this.db.transaction(tx => {
            tx.executeSql("SELECT * FROM settings", [], (transaction, result) => {

                if(result.rows.length > 0) {
                    subject.next(result.rows.item(0).showIntro);
                }
                else {
                    tx.executeSql("INSERT INTO SETTINGS (showIntro) values (1)")
                    subject.next(true);
                }
            });
        })
        return subject;
    }

    updateShowIntro(val:boolean) : Observable<boolean> {
        var subject = new Subject();
        this.db.transaction(tx => {
            tx.executeSql("UPDATE settings SET showIntro=0", [], (transaction, result) => {
                subject.next(false);
            });
        });
        return subject;
    }


    //Account CRUD Operations
    addAccount(account:LocalAccount) : Observable<LocalAccount> {
        console.log("Add Account From Storage provider");
        var subject = new Subject();

        this.platform.ready().then(() => {
          this.file.writeFile(this.file.dataDirectory, `users/${account.username}.txt`, JSON.stringify(account), { replace: true })
            .then(a => {
              console.log("File written!");
              subject.next(account);
            })
            .catch(e => {
              //What to do here?
              console.log("ERROR!", e);
              subject.next(account);
            })
        });

        return subject;
    }

    removeAccount(account:LocalAccount) : Observable<boolean> {
        var subject = new Subject();

        this.file.removeFile(this.file.dataDirectory, `users/${account.username}.txt`)
          .then(a => {
            subject.next(true);
          })
          .catch(err => {
            console.log('Unable to remove account', err);
          });

        return subject;
    }

    updateAccount(account:LocalAccount) : Observable<{}> {
        var subject = new Subject();

        this.platform.ready().then(() => {
          this.file.writeFile(this.file.dataDirectory, `users/${account.username}.txt`, JSON.stringify(account), { replace: true })
          .then(() => {
            subject.next(true);
          })
          .catch(err=> {
            console.log("ERROR updating account", account.username);
          });
        })

        return subject;
    }

    updateAccountItem(account:LocalAccount, columnToChange:string[], value) : Observable<{}> {
        var subject = new Subject();
        this.getAccount(account.username).subscribe(s => {
            var acct = s;
            for(var i=0; i<columnToChange.length; i++) {
                acct[columnToChange[i]] = value[i];
            }

            this.updateAccount(acct).subscribe(su => {
              subject.next(true);
            });
        });
        return subject;
    }

    getAccounts() : Observable<any> {
      var subject = new Subject();

      this.platform.ready().then(() => {
        this.file.listDir(this.file.dataDirectory, 'users')
        .then(e => {
          var accounts = [];
          e.forEach(a => {
            if(a.isFile) {
              accounts.push(this.http.get(a.toURL()).map(res => res.text()))
            }
          })

          if(accounts.length === 0) {
            subject.next([]);
          }
          else {
            Observable.forkJoin(...accounts).subscribe(results => {
              var parsed = results.map(m => {
                var dataStr = m.toString();
                try {
                  var json = JSON.parse(dataStr);
                  return LocalAccount.fromJSON(json);
                } catch(e) {
                  console.log("error parsing json", e, m);
                  //Sometimes the user account gets written twice
                  //Try to manually correct the issue.
                  var lastIndex = dataStr.indexOf('}');
                  var firstObject = dataStr.substr(0, lastIndex + 1);

                  try {
                    console.log("2nd try", firstObject);
                    var js = JSON.parse(firstObject);
                    return LocalAccount.fromJSON(js);

                  } catch(err) {
                    console.log("Error again!", err)
                  }
                }
              }, []).sort((a,b) => {
                return a.sortOrder - b.sortOrder;
              });

              subject.next(parsed);
            });
          }

        })
        .catch(err => {
          console.log(err);
        })
      });

      return subject;
    }

    getAccount(username:string) : Observable<LocalAccount> {
        var subject = new Subject();

        this.platform.ready().then(() => {
          this.getAccounts().subscribe(accounts => {
            var accountExists = accounts.find(f=> f.username === username);
            subject.next(accountExists || null);
          });
        });

       return subject;
    }

    reorderAccounts(accounts:LocalAccount[]) : Observable<{}> {
        var subject = new Subject();

        this.platform.ready().then(() => {
          var updates = [];
          accounts.forEach((a, index) => {
            a.sortOrder = index;
            console.log(a.nickname, index);
            updates.push(this.updateAccount(a))
          });

          Observable.forkJoin(...updates).subscribe(results => {
            subject.next(true);
          });
        });

        return subject;
    }
}
