
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { AppConfig } from '../config';

@Injectable()
export class LibcalProvider {

    constructor(private http:Http){}

    getLibraries() {
        //get today's hours for all libraries
        return this.http.get(`${AppConfig.LIBCAL_BASE_URL}api_hours_today.php?iid=83&format=json&weeks=1`);
    }

    getLibrary(lid:Number) {
        //get 1 week of hours for an individual library 
        return this.http.get(`${AppConfig.LIBCAL_BASE_URL}api_hours_grid.php?iid=83&format=json&weeks=2&lid=${lid}`);
    }



}


//
