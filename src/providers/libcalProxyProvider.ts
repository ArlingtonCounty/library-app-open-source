import { Injectable } from '@angular/core';
import { Http, Headers, URLSearchParams } from '@angular/http';
import { AppConfig } from '../config';

@Injectable()
export class LibcalProxyProvider {

    rootUrl = AppConfig.MEETING_ROOMS_URL;

    constructor(private http:Http){ }

    getLocations() {
        return this.http.get(`${this.rootUrl}meetingrooms/locations`).map(s => {
            let body = s.json();
            var publicItems = []
            body.forEach(mr => {
                if(mr.public == 1) {
                    publicItems.push(mr);
                }
            });
            return publicItems;
        });
    }

    getCategories(locationId:number) {
        return this.http.get(`${this.rootUrl}meetingrooms/categories/${locationId}`).map(s => {
            let body = s.json();
            return body;
        });
    }

    getSpaces(categoryId:number) {
        return this.http.get(`${this.rootUrl}meetingrooms/category/${categoryId}`).map(s => {
            let body = s.json();
            return body;
        });
    }

    reserveSpace(data) {
        var headers = new Headers();
        headers.append('Content-Type', 'text')
        var jsonString = JSON.stringify(data);

        return this.http.post(`${this.rootUrl}meetingrooms/reserve`, jsonString, {
            headers: headers
        }).map(res => {
            var body = res.json();
            return body;
        });
    }
}
