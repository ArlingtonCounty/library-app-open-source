import { Injectable } from '@angular/core';
import {Http } from '@angular/http';
import { AppConfig } from '../config';


@Injectable()
export class WordpressProvider {
    baseUrl:string;

    constructor(private http:Http){
        this.baseUrl = AppConfig.WORDPRESS_BASE_URL;
    }

    getPostsByCategory(category:string, perPage?:number, page?:number, tags?:number) {
        var perPageQuery = '';
        if(perPage) {
            perPageQuery = `&per_page=${perPage}`;
        }
        var pageQuery = '';
        if(page) {
            pageQuery = `&page=${page}`;
        }
        var tagsQuery = '';
        if (tags) {
          tagsQuery = `&tags[]=${tags}`;
        }

        return this.http.get(`${this.baseUrl}posts/?_embed&categories[]=${category}${perPageQuery}${pageQuery}${tagsQuery}`);
    }

    getPostsByTag(tag:string) {
        return this.http.get(`${this.baseUrl}posts/?_embed&tags[]=${tag}`);
    }

    getPostsById(id:number) {
        return this.http.get(`${this.baseUrl}posts/${id}`);
    }

    getFaqs() {
        return this.http.get(`${this.baseUrl}ufaq?ufaq-category=261`);
    }
}
