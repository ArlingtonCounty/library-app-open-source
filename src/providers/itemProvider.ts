import { Injectable } from '@angular/core';

import { PikaProvider } from './pikaProvider';


export class Item { 
    title:string;
    author:string;
    publisher:string[];
    edition:string;
    genre:string;
    series:string;
    physical:string;
    lccn:string;
    contents:string;
    format:string;
    formatCategory:string;
    language:string;
    description:string;
    ratingData:string;

    public formattedPublisher() : string {
        if(!this.publisher) return "";

        return this.publisher.join(", ");
    }

    static fromJson(obj:any) : Item {
        var i = new Item();
        
        i.title = obj.title_display;
        i.author = obj.author;
        i.publisher = obj.publisher;
        i.edition = obj.edition;
        i.genre = obj.genre;
        i.series = obj.series;
        i.physical = obj.physical;
        i.lccn = obj.lccn;
        i.contents = obj.contents;
        i.format = obj.format;
        i.formatCategory = obj.formatCategory;
        i.language = obj.language;
        i.description = obj.description;
        i.ratingData = obj.ratingData;

        return i;
    }
}

@Injectable()
export class ItemProvider {

    constructor(private pika:PikaProvider) {

    }

    getItemById(id:string) {
        return this.pika.itemApi("getItem", id)
        .map(r => {
            var body = r.json();
            return Item.fromJson(body.result);
        });
    }

    //This api doesn't seem to return any useful information
    checkAvailability(id:string) {
        return this.pika.itemApi("getItemAvailability", id)
        .map(r => {
            var body = r.json();
            return body.result;
        });
    }

    //This api doesn't seem to work
    getBookcoverById(id:string){
        return this.pika.itemApi('getBookcoverById', id)
        .map( r=> {
            var body = r.json();
            return body.result;
        })
    }

}