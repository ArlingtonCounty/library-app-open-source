import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { AppConfig } from '../config';

declare var analytics;
const iOSTracking:string = AppConfig.TRACKING_IOS_ID;
const AndroidTracking:string = AppConfig.TRACKING_ANDROID_ID;

export class AnalyticsDimensions {
    public static BookTitle:number = 1;
    public static Library:number = 2;
}

@Injectable()
export class AnalyticsProvider {

    constructor(private platform:Platform) { }

    getTrackingId() : string {
        var id = '';
        if(this.platform.is('ios')) {
            id = iOSTracking;
        }
        else {
            id = AndroidTracking;
        }
        return id;
    }

    trackingStarted:boolean = false;
    startTracker(callback?) {
        if(!AppConfig.APP_ENABLE_GOOGLE_ANALYTICS)
            callback();

        var self = this;
        if(!this.trackingStarted){
         analytics.startTrackerWithId(this.getTrackingId(), function(){
             self.trackingStarted = true;
             if(callback) { 
                 callback();
             }
         });
        }
        else {
            if(callback) { 
                callback();
            }
        }
    }

    trackView(view:string, dimensions?:any) {
        if (!AppConfig.APP_ENABLE_GOOGLE_ANALYTICS)
            return;

        var options = {};
        if(dimensions) { 
            options["customDimensions"] = dimensions
        }
        this.startTracker(function(){
            analytics.trackView(view, options, function() {console.log('tracking success: ', view)}, function(error) { console.log(error, view);});
        });
    }

    trackEvent(action:string, label?:string, value?:number){
        if (!AppConfig.APP_ENABLE_GOOGLE_ANALYTICS)
            return;
            
        this.startTracker(function(){
            analytics.tracKEvent("LibraryMobile", action, label, value);
        });
    }
}


export class AnalyticsProviderMock extends AnalyticsProvider {
    constructor() { 
        super(null);
    }

    trackView(view:string) {

    }
}