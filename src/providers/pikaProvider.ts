import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'
import { Http, Response } from '@angular/http';
import { AppConfig } from '../config';

@Injectable()
export class PikaProvider { 
    constructor(private http:Http){

    }

    userApi(method:string, username:string, password:string) : Observable<Response> {
        return this.http.get(`${AppConfig.PIKA_BASE_URL}API/UserAPI?method=${method}&username=${username}&password=${password}`);
    }

    searchApi(method:string, searchTerm:string, facets:any, sort?:string, page?:number) : Observable<Response> {
        if(facets) {
            var keys = Object.keys(facets);
            keys.forEach(key => {
                var facet = facets[key];
                var facetKeys = Object.keys(facet);
                facetKeys.forEach(facetKey => {
                    if(facet[facetKey] === true)
                        searchTerm += `&filter[]=${key}:"${facetKey}"`;
                });
            });
        }
        let pageHold = ''
        if(page) {
            pageHold = `&page=${page}`
        }
        let sortParam = '';
        if(sort){
            sortParam = `&sort=${sort}`;
        }

        return this.http.get(`${AppConfig.PIKA_BASE_URL}API/SearchAPI?method=${method}&q=${searchTerm}${sortParam}${pageHold}`);
    }

    barcodeSearch(barcode:string){
        return this.http.get(`${AppConfig.PIKA_BASE_URL}API/SearchAPI?method=getRecordIdForItemBarcode&barcode=${barcode}`)
    }

    itemApi(method:string, id:string) : Observable<Response> {
        return this.http.get(`${AppConfig.PIKA_BASE_URL}API/ItemAPI?method=${method}&id=${id}`);
    }

    listApi(method:string) : Observable<Response> {
        return this.http.get(`${AppConfig.PIKA_BASE_URL}API/ListAPI?method=${method}`);
    }

    getUserLists(username, password) : Observable<Response> {
        return this.http.get(`${AppConfig.PIKA_BASE_URL}API/ListAPI?method=getUserLists&username=${username}&password=${password}`);
    }

    getUserListTitles(listId) : Observable<Response> {
        return this.http.get(`${AppConfig.PIKA_BASE_URL}API/ListAPI?method=getListTitles&id=${listId}`);
    }    

    placeHold(username, password, bibliographyId, campusId){
        return this.http.get(`${AppConfig.PIKA_BASE_URL}API/UserAPI?method=placeHold&username=${username}&password=${password}&bibId=${bibliographyId}&campus=${campusId}`);
    }

    placeOverdriveHold(username, password, overDriveId?:string, recordId?:string){
        return this.http.get(`${AppConfig.PIKA_BASE_URL}API/UserAPI?method=placeOverDriveHold&username=${username}&password=${password}&overDriveId=${overDriveId}`);
    }

    getListTitles(listId:number) {
        return this.http.get(`${AppConfig.PIKA_BASE_URL}Search/AJAX?method=GetListTitles&id=list:${listId}&scrollerName=Homepage&coverSize=large&showRatings=0&numTitlesToShow=25`);
    }

    getCategories(){
        return this.http.get(`${AppConfig.PIKA_BASE_URL}Browse/AJAX?method=getActiveBrowseCategories&branch=0`);
    }

    getTitlesForCategory(categoryId, page?) {
        return this.http.get(`${AppConfig.PIKA_BASE_URL}Browse/AJAX?method=getBrowseCategoryInfo&textId=${categoryId}`);
    }

    getMoreTitlesForCategory(categoryId, page) {
        return this.http.get(`${AppConfig.PIKA_BASE_URL}Browse/AJAX?method=getMoreBrowseResults&textId=${categoryId}&pageToLoad=${page}`);
    }

    getItemDescription(bibId){
        return this.http.get(`${AppConfig.PIKA_BASE_URL}API/ItemAPI?method=getDescriptionByRecordId&recordId=${bibId}`);
    }

    getCopyAndHoldCounts(id) {
        return this.http.get(`${AppConfig.PIKA_BASE_URL}API/ItemAPI?method=getCopyAndHoldCounts&recordId=${id}`);
    }
}
