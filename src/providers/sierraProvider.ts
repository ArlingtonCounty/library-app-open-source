import { Injectable } from '@angular/core';
import {Http, Headers } from '@angular/http';

@Injectable()
export class SierraProvider { 
    token:string = "";
    constructor(private http: Http) { }
    
    getAuthToken() {
        var headers = new Headers();
        headers.append('Authorization', 'Basic ' + "");
        return this.http.post(`token`, "grant_type=client_credentials", {
            headers: headers
        }).map(res => {
            let body = res.json();
            this.token = body.access_token;
            console.log(this.token);
            return true;
        });
    }

    getBranches() {
        //add authorization token
        var headers = new Headers();
        headers.append('Authorization', 'Bearer ' + this.token);
        return this.http.get(`branches`, {
            headers: headers
        });
    }
}