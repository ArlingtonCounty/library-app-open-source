import { Injectable } from '@angular/core';
import { AlertController, Platform } from 'ionic-angular';

@Injectable()
export class LinkProvider { 
    constructor(private alertCtrl:AlertController, private platform:Platform) { }


    callPhoneNumber(phoneNumber) {
        if (this.platform.is('ios')) {
            let alert = this.alertCtrl.create({
                title: phoneNumber,
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        handler: () => {
                            console.log('Cancel clicked');
                        }
                    },
                    {
                        text: 'Call',
                        handler: () => {
                            window.open('tel:' + phoneNumber, '_system');
                        }
                    }
                ]
            });
            alert.present();
        }
        else {
            window.open('tel:' + phoneNumber, '_system');
        }
    }

    openInMaps(lat, lng, title) {
        let url = "";
        const isAndroid = this.platform.is('android');
        
        if(isAndroid){
            url = `geo:${lat},${lng}?q=${title}`;
            window.open(url, '_system', 'location=yes');
        }
        else {
            url = `http://maps.apple.com/?q=${title}&ll=${lat},${lng}&near=${lat},${lng}`;
            window.open(url, '_system', 'location=yes');
        }
        
    }

}
