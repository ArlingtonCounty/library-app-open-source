import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { AppConfig } from '../config';

import moment from 'moment-timezone';

export class Event {
    distanceInMeters: number;
    latitude: number;
    longitude: number;
    eventKey: number;
    eventName: string;
    eventDsc: string;
    eventStartDtm: Date;
    eventEndDtm: Date;
    eventStartDate: Date;
    eventEndDate: Date;
    eventStartTime: Date;
    eventEndTime: Date;
    featuredEventInd: boolean;
    endingAgeRangeQty: number;
    eventUrlText: string;
    beginningAgeRangeQty: number;
    familyFriendlyInd: boolean;
    parkingAvailableCode: string;
    roomName: string;
    contactName: string;
    contactEmailText: string;
    contactPhoneNbr: string;
    freeOfChargeInd: boolean;
    eventCostDsc: string;
    busStopNearInd: boolean;
    metroRailInd: boolean;
    bikeShareInd: boolean;
    streetAddressText: string;
    cityName: string;
    stateCode: string;
    zipCode: number;
    locationStatusPendingInd: boolean;
    locationLongitudeCrd: number;
    locationLatitudeCrd: number;
    locationName:string;
    locationUrlText: string;
    eventExpiredInd: boolean;
    eventTopicKeyList: number;
    registrationRequiredInd: boolean;

    static fromJson(obj:any) : Event {
        var e = new Event();

        e.distanceInMeters = obj.distanceInMeters;
        e.latitude = obj.latitude;
        e.longitude = obj.longitude;

        e.eventKey = obj.vwEventWithLocation.eventKey;
        e.eventName = obj.vwEventWithLocation.eventName;
        e.eventDsc = obj.vwEventWithLocation.eventDsc;
        
        e.eventStartDtm = moment.tz(obj.vwEventWithLocation.eventStartDtm, 'America/New_York').toDate();
        e.eventEndDtm = moment.tz(obj.vwEventWithLocation.eventEndDtm, 'America/New_York').toDate();
        e.eventStartDate =  moment.tz(obj.vwEventWithLocation.eventStartDate, 'America/New_York').toDate();
        e.eventEndDate = moment.tz(obj.vwEventWithLocation.eventEndDate, 'America/New_York').toDate();

        e.eventStartTime = obj.vwEventWithLocation.eventStartTime;
        e.eventEndTime = obj.vwEventWithLocation.eventEndTime;
        
        e.featuredEventInd = obj.vwEventWithLocation.featuredEventInd;
        e.endingAgeRangeQty = obj.vwEventWithLocation.endingAgeRangeQty;
        e.eventUrlText = obj.vwEventWithLocation.eventUrlText;
        e.beginningAgeRangeQty = obj.vwEventWithLocation.beginningAgeRangeQty;
        e.familyFriendlyInd = obj.vwEventWithLocation.familyFriendlyInd;
        e.parkingAvailableCode = obj.vwEventWithLocation.parkingAvailableCode;
        e.roomName = obj.vwEventWithLocation.roomName;
        e.contactName = obj.vwEventWithLocation.contactName;
        e.contactEmailText = obj.vwEventWithLocation.contactEmailText;
        e.contactPhoneNbr = obj.vwEventWithLocation.contactPhoneNbr;
        e.freeOfChargeInd = obj.vwEventWithLocation.freeOfChargeInd;
        e.eventCostDsc = obj.vwEventWithLocation.eventCostDsc;
        e.busStopNearInd = obj.vwEventWithLocation.busStopNearInd;
        e.metroRailInd = obj.vwEventWithLocation.metroRailInd;
        e.bikeShareInd = obj.vwEventWithLocation.bikeShareInd;
        e.streetAddressText = obj.vwEventWithLocation.streetAddressText;
        e.cityName = obj.vwEventWithLocation.cityName;
        e.stateCode = obj.vwEventWithLocation.stateCode;
        e.zipCode = obj.vwEventWithLocation.zipCode;
        e.locationStatusPendingInd = obj.vwEventWithLocation.locationStatusPendingInd;
        e.locationLongitudeCrd = obj.vwEventWithLocation.locationLongitudeCrd;
        e.locationLatitudeCrd = obj.vwEventWithLocation.locationLatitudeCrd;
        e.locationName = obj.vwEventWithLocation.locationName;
        e.locationUrlText = obj.vwEventWithLocation.locationUrlText;
        e.eventExpiredInd = obj.vwEventWithLocation.eventExpiredInd;
        e.eventTopicKeyList = obj.vwEventWithLocation.eventTopicKeyList;
        e.registrationRequiredInd = obj.vwEventWithLocation.registrationRequiredInd;

        return e;
    }

}

export class EventsFilter {
    sortBy: string;
    within: number;
    nearBus: boolean;
    nearMetro: boolean;
    nearBike: boolean;
    nearParking: boolean;
    locationName: string;
}

export const defaultEventsFilter: EventsFilter = {
    sortBy: 'date',
    within: -1,
    nearBus: false,
    nearBike: false,
    nearMetro: false,
    nearParking: false,
    locationName: 'All'
}


@Injectable()
export class EventsProvider {
    public events: Event[];
    public eventsCount: number;
    public eventsLoaded: boolean;
    public eventsLoadedCount: number;
    public eventsFilter: EventsFilter;

    constructor(private http:Http) { 
        this.events = [];
        this.eventsCount = 0;
        this.eventsLoaded = false;
        this.eventsLoadedCount = 0;

        this.eventsFilter = {
            sortBy: 'date',
            within: -1,
            nearBus: false,
            nearBike: false,
            nearMetro: false,
            nearParking: false,
            locationName: 'All'
        }
    }

    eventsApi(loadNext:number, filter:EventsFilter, searchTerm?:string, coords?, reload?:boolean, count?:number){
        if(reload) {
            this.eventsLoaded = false;
        }

        if(!count) {
            count = 25;
        }

        if(loadNext === 0) {
            this.eventsLoaded = false;
            this.events = [];
        }

        let nearBus = '';
        if(filter.nearBus) {
            nearBus = '&NearBus=true';
        }

        let nearBike = '';
        if(filter.nearBike) {
            nearBike = '&NearBikeShare=true';
        }

        let nearMetro = '';
        if(filter.nearMetro) {
            nearMetro = '&NearRail=true';
        }

        let nearParking = '';
        if(filter.nearParking) {
            nearParking = '&ParkingAvailable=true';
        }

        let location = '';
        if(coords) {
            location = `&Lat=${coords.lat}&Lon=${coords.lng}`;
        }

        let withInDistance = '';
        if(filter.within > 0) {
            withInDistance = `&RadiusInMeters=${filter.within * 1609.34}`;
        }

        let locationName = '';
        if(filter.locationName != 'All') {
            location = `&LocationName=${filter.locationName}`;
        }

        var now = new Date();
        console.log(now.toISOString());
        return this.http.get(`${AppConfig.DATAHUB_BASE_URL}api/event/elasticevent?format=json&TopicCode=LIBRARY` 
                            + `&Size=${count}`
                            + `&From=${loadNext}`
                            + location
                            + withInDistance
                            + `&StartDate=${now.toISOString()}`
                            + `&OrderBy=${filter.sortBy}`
                            + nearBus
                            + nearBike
                            + nearMetro
                            + nearParking
                            + locationName
                            + `&SearchTerm=${searchTerm}`)
            .map(r => {
                if(!this.eventsLoaded) {
                    let body= r.json();
                    if(body.items.length > 0) {
                        this.eventsCount = body.count;
                        body.items.forEach(record => {
                            if(Event.fromJson(record).eventEndDtm > new Date()) {
                                this.events.push(Event.fromJson(record));
                            }
                        });
                    }
                    this.eventsLoaded = true;
                    
                }
                return this.events;
            });
    }

    getEventsCount() {
        return this.eventsCount;
    }

    getEventsFilter() {
        return this.eventsFilter;
    }

    setSortBy(sort){
        this.eventsFilter.sortBy = sort;
    }
    getSortBy() {
        return this.eventsFilter.sortBy;
    }

    setWithin(within) {
        this.eventsFilter.within = within;
    }
    getWithin() {
        return this.eventsFilter.within;
    }

    setNearBus(nearBus) {
        this.eventsFilter.nearBus = nearBus;
    }
    getNearBus() {
        return this.eventsFilter.nearBus;
    }

    setNearMetro(nearMetro) {
        this.eventsFilter.nearMetro = nearMetro;
    }
    getNearMetro() {
        return this.eventsFilter.nearMetro;
    }

    setNearBike(nearBike) {
        this.eventsFilter.nearBike = nearBike;
    }
    getNearBike() {
        return this.eventsFilter.nearBike;
    }

    setParking(nearParking) {
        this.eventsFilter.nearParking = nearParking;
    }
    getNearParking() {
        return this.eventsFilter.nearParking;
    }

    setLocationname(locationName) {
        this.eventsFilter.locationName = locationName;
    }
    getLocationName() {
        return this.eventsFilter.locationName;
    }
}