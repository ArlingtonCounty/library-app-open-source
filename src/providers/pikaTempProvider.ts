import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'
import { Http, Response, URLSearchParams, Headers } from '@angular/http';
import { AppConfig } from '../config';


@Injectable()
export class PikaTempProvider { 
    baseUrl:string = AppConfig.LIBCAL_BASE_URL;
    constructor(private http:Http){

    }

    cancelHold(patronId:number, recordId:string, cancelId:string) {
        //&cancelId=b1942360~00
        return this.http.get(`${this.baseUrl}MyAccount/AJAX?method=cancelHold&patronId=${patronId}&recordId=${recordId}&cancelId=${cancelId}`);
    }

    thawHold(patronId:number, recordId:string, holdId:string){
        return this.http.get(`${this.baseUrl}MyAccount/AJAX?method=thawHold&patronId=${patronId}&recordId=${recordId}&holdId=${holdId}`);
    }

    freezeHold(patronId:number, recordId:string, holdId:string){
        return this.http.get(`${this.baseUrl}MyAccount/AJAX?method=freezeHold&patronId=${patronId}&recordId=${recordId}&holdId=${holdId}`);
    }

    cancelOverDriveHold(patronId:number, overDriveId:string) {
        return this.http.get(`${this.baseUrl}OverDrive/AJAX?method=CancelOverDriveHold&patronId=${patronId}&overDriveId=${overDriveId}`)
    }

    returnOverDriveItem(patronId:number, overDriveId:string, transactionId?:string) {
        return this.http.get(`${this.baseUrl}OverDrive/AJAX?method=ReturnOverDriveItem&patronId=${patronId}&overDriveId=${overDriveId}`)
    }

    getDownloadLink(patronId:number, overDriveId:string, formatId:string) {
        return this.http.get(`${this.baseUrl}OverDrive/AJAX?method=GetDownloadLink&patronId=${patronId}&overDriveId=${overDriveId}&formatId=${formatId}`)
    }

    selectOverDriveDownloadFormat(patronId:number, overDriveId:string, formatId:string) {
        return this.http.get(`${this.baseUrl}OverDrive/AJAX?method=SelectOverDriveDownloadFormat&patronId=${patronId}&overDriveId=${overDriveId}&formatId=${formatId}`)
    }
    
    changeHoldLocation(patronId:number, newLocation:string, holdId:string) {
        return this.http.get(`${this.baseUrl}MyAccount/AJAX?method=changeHoldLocation&patronId=${patronId}&holdId=${holdId}&newLocation=${newLocation}`)
    }

    renewItem(patronId:number, recordId:string, renewIndicator:string) {
        return this.http.get(`${this.baseUrl}MyAccount/AJAX?method=renewItem&patronId=${patronId}&recordId=${recordId}&renewIndicator=${renewIndicator}`)
    }

    renewSelected(patronId:number, checkouts:any[]){
        var checkoutString = checkouts.map(c => {
            return `&selected[${patronId}|${c.id}|${c.itemid}|0}]`;
        })
        return this.http.get(`${this.baseUrl}MyAccount/AJAX?method=renewSelectedItems${checkoutString}`);
    }

    renewAll() {
        return this.http.get(`${this.baseUrl}MyAccount/AJAX?method=renewAll`)
    }

    placeOverdriveHold(patronId:number, overDriveId:string, overdriveEmail:string) {
            return this.http.get(`${this.baseUrl}OverDrive/AJAX?method=PlaceOverDriveHold&patronId=${patronId}&overDriveId=${overDriveId}&overdriveEmail=${overdriveEmail}`)
    }

    checkoutOverdrive(patronId:number, overDriveId:string, overdriveEmail:string){
        return this.http.get(`${this.baseUrl}OverDrive/AJAX?method=CheckoutOverDriveItem&patronId=${patronId}&overDriveId=${overDriveId}&overdriveEmail=${overdriveEmail}`);
    }

    readOnline(patronId:number, overDriveId:string, format:string) {
        return this.http.get(`${this.baseUrl}OverDrive/AJAX?method=GetDownloadLink&patronId=${patronId}&overDriveId=${overDriveId}&formatId=${format}&_=${new Date().getTime()}`)
    }

    login(username:string, password:string){
        var headers = new Headers();
        headers.append('Content-Type', 'application/x-www-form-urlencoded');
        
        let urlSearchParams = new URLSearchParams();
        urlSearchParams.append('method', 'login');
        urlSearchParams.append('username', username);
        urlSearchParams.append('password', password);
        urlSearchParams.append('rememberMe', 'true');
        
        let body = urlSearchParams.toString();

        return this.http.post(`${AppConfig.PIKA_BASE_URL}AJAX/JSON?method=loginUser`, body, { headers: headers, withCredentials: true }).map(m=> {
        });
    }

    logout() {
        return this.http.get(`${AppConfig.PIKA_BASE_URL}MyAccount/Logout`, { withCredentials: true });
    }

    authenticateAction(account, action:Observable<Response>) {
        var logout = this.logout();
        var login = this.login(account.username, account.password);
        
        return logout.flatMap(f=> {
            return login.flatMap(f => {
                return action.map(m => {
                    var body = m.json();
                    return body;
                }); 
            });
        });
    }
}