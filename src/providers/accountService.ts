import { Injectable } from '@angular/core';
import { Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { PikaProvider } from './pikaProvider';
import { StorageProvider } from './storageProvider';
import { ToastController, Events } from 'ionic-angular';
import { PushNotificationsProvider } from '../components/settings/pushNotificationsProvider';
import { NgRedux } from '@angular-redux/store';
import { IAppState } from '../store/IAppState';
import { AppConfig } from '../config';

import * as moment from 'moment';

export class Profile {
    id:string;
    username:string;
    barcode:string;
    cat_username:string;
    displayName:string;
    email:string;
    phone:string;
    finesVal:number;
    numCheckedOutIls:number;
    numHoldsIls:number;
    numHoldsAvailableIls:number;
    numHoldsRequestedIls:number;
    numHoldsOverDrive:number;
    numHoldsAvailableOverDrive:number;
    address1:string;
    address2:string;
    expires:string;
    cat_password:string;
    fullname:string;
    update:boolean;
    overdriveEmail:string;
    promptForOverdriveEmail: string;

    static fromJson(obj:any) : Profile {
        var p = new Profile();
        p.id = obj.id;
        p.username = obj.username;
        p.displayName = obj.displayName;
        p.cat_username = p.barcode = obj.cat_username;
        p.email = obj.email;
        p.phone = obj.phone;
        p.finesVal = obj.finesVal;
        p.numCheckedOutIls = obj.numCheckedOutIls;
        p.numHoldsIls = obj.numHoldsIls || 0;
        p.numHoldsAvailableIls = obj.numHoldsAvailableIls || 0;
        p.numHoldsRequestedIls = obj.numHoldsRequestedIls || 0;
        p.numHoldsOverDrive = obj.numHoldsOverDrive || 0;
        p.numHoldsAvailableOverDrive = obj.numHoldsAvailableOverDrive || 0;
        p.address1 = obj.address1
        p.address2 = obj.address2;
        p.expires = obj.expires;
        p.cat_password = obj.cat_password;
        p.fullname = obj.fullname;
        p.update = false;
        p.overdriveEmail = obj.overdriveEmail;
        p.promptForOverdriveEmail = obj.promptForOverdriveEmail;

        return p;
    }
}

export class LocalAccount {
    username: string;
    password: string;
    displayName: string;
    nickname: string;
    patronId: number;
    patronNumber: number;

    barcode:string;

    checkoutsAlert: boolean;
    checkoutsWarning: boolean;
    finesAlert: boolean;
    holdsAlert: boolean;
    expiresAlert: boolean;
    homeLibraryCode:string;

    sortOrder:number;

    anyAlerts() {
        return this.checkoutsAlert || this.finesAlert || this.holdsAlert || this.expiresAlert || this.checkoutsWarning;
    }

    public getName() : string {
        return this.nickname || this.displayName || this.username || "";
    }

    constructor(username?: string,
                password?: string,
                displayName?: string,
                nickname?:string,
                patronId?:number,
                checkoutsAlert?:boolean,
                checkoutsWarning?:boolean,
                finesAlert?:boolean,
                holdsAlert?:boolean,
                expiresAlert?:boolean,
                barcode?:string,
                homeLibraryCode?:string,
                patronNumber?:number) {
        this.username = username;
        this.password = password;
        this.displayName = displayName;
        this.nickname = nickname;
        this.patronId = patronId;
        this.checkoutsAlert = checkoutsAlert;
        this.checkoutsWarning = checkoutsWarning;
        this.finesAlert = finesAlert;
        this.holdsAlert = holdsAlert;
        this.expiresAlert = expiresAlert;
        this.barcode = barcode;
        this.homeLibraryCode = homeLibraryCode;
        this.patronNumber = patronNumber;
    }

    static fromJSON(obj:any) {
      var a = new LocalAccount();

      a.username = obj.username;
      a.password = obj.password;
      a.displayName = obj.displayName;
      a.nickname = obj.nickname;
      a.patronId = obj.patronId;
      a.checkoutsAlert = obj.checkoutsAlert;
      a.checkoutsWarning = obj.checkoutsWarning;
      a.finesAlert = obj.finesAlert;
      a.holdsAlert = obj.holdsAlert;
      a.expiresAlert = obj.expiresAlert;
      a.barcode = obj.barcode;
      a.homeLibraryCode = obj.homeLibraryCode;
      a.patronNumber = obj.patronNumber;

      return a;
    }
}

@Injectable()
export class AccountService {
    public profile: Profile;

    constructor(private pikaProvider:PikaProvider
              , public toastCtrl: ToastController
              , private storageProvider: StorageProvider
              , private pushProvider:PushNotificationsProvider
              , private events:Events
              , private ngRedux:NgRedux<IAppState>) {

    }

    getSavedAccounts() : Observable<LocalAccount[]> {
        return this.storageProvider.getAccounts().map(accounts => {
            var returnArray = [];

            accounts.forEach(a => {
                var localAccount = new LocalAccount(a.username, a.password, a.displayName, a.nickname, a.patronId, a.checkoutsAlert, a.checkoutsWarning, a.finesAlert, a.holdsAlert, a.expiresAlert, a.barcode, a.homeLibraryCode, a.patronNumber);
                returnArray.push(localAccount);
            });

            return returnArray;
        });
    }

    getProfile(account: LocalAccount, changed: boolean) : Observable<any> {
        return this.getUserProfile(account.username, account.password, false, account.nickname);
    }

    getUserLists(account: LocalAccount) {
        return this.pikaProvider.getUserLists(account.username, account.password).map(m=> {
            const response = m.json();
            if(response.result.success) {
                return response.result.lists;
            }
            else
                return [];
        });
    }

    getUserListTitles(list) {
        return this.pikaProvider.getUserListTitles(list.id).map(m=> {
            const response = m.json();
            if(response.result.success) {
                return response.result.titles;
            }
            else {
                return [];
            }
        });
    }

    public getUserProfile(username: string, password: string, newAccount: boolean, nickname: string) : Observable<Profile> {
        return this.pikaProvider.userApi("getPatronProfile", username, password)
            .flatMap(res => {
                return this.storageProvider.getAccount(username).map(account => {
                    //If user is already logged in, return a message

                    let body = res.json();
                    if (body.result.success) {
                        var expires = body.result.profile.expires || "01/01/2999";
                        var year = parseInt(expires.substring(6,8)) + 2000;
                        var month = parseInt(expires.substring(0,2)) - 1;
                        var day = parseInt(expires.substring(3,5));
                        var expireDate = new Date(year,month,day)

                        if (!account || newAccount) {
                            var finesAlert = body.result.profile.finesVal >= 10;
                            var holdsAlert = body.result.profile.numHoldsAvailableIls > 0 || body.result.profile.numHoldsAvailableOverDrive > 0;
                            var expiresAlert = moment().isSameOrAfter(expireDate);
                            var homeLibraryCode = body.result.profile.homeLocationCode;
                            var account = new LocalAccount(username, password, body.result.profile.displayName, nickname, Number(body.result.profile.id), false, false, finesAlert, holdsAlert, expiresAlert, body.result.profile.cat_username, homeLibraryCode, body.result.profile.username);

                            this.storageProvider.addAccount(account).map(s=> {
                                this.showToast('Added Account');
                                this.events.publish("account:created", account);
                                this.updatePushNotifications();
                            });

                            var profile = Profile.fromJson(body.result.profile);

                            this.ngRedux.dispatch({
                              type: 'account/ADD',
                              account: account,
                              profile: profile
                            });

                            return profile;
                        }
                        else { //update properties
                            account.finesAlert = finesAlert = body.result.profile.finesVal >= 10;
                            account.holdsAlert = parseInt(body.result.profile.numHoldsAvailableIls) > 0 || parseInt(body.result.profile.numHoldsAvailableOverDrive) > 0;
                            account.expiresAlert = moment().isSameOrAfter(expireDate);
                            account.displayName = body.result.profile.displayName;
                            account.patronId = Number(body.result.profile.id);
                            account.barcode = body.result.profile.cat_username;
                            account.patronNumber = body.result.profile.username;

                            if(!account.homeLibraryCode) {
                                account.homeLibraryCode = body.result.profile.homeLocationCode;
                            }

                            this.ngRedux.dispatch({
                              type: 'account/UPDATE',
                              account: account
                            });

                            this.storageProvider.updateAccount(account).subscribe(s=> {
                              console.log("Account updated.", account);
                            });
                            return Profile.fromJson(body.result.profile);
                        }
                    }
                    else {
                        if(account) {
                            let UpdateProfile = new Profile();
                            UpdateProfile.update = true;
                            return UpdateProfile;
                        }

                        this.showToast('Incorrect Credentials')
                        return null;
                    }

                });

            });
    }

    updatePushNotifications() {
        if (AppConfig.APP_ENABLE_PUSH_NOTIFICATIONS) {
            this.pushProvider.getSettings().subscribe(s => {
                if (s.length > 0) {
                    var setting = s[0];
                    var overdueInd = setting.OverduesInd;
                    var finesInd = setting.FinesInd;
                    var holdInd = setting.HoldsInd;
                    var miscInd = setting.MiscInd;
    
                    this.getSavedAccounts().subscribe(accounts => {
                        this.pushProvider.updateSettings(finesInd, overdueInd, holdInd, miscInd, accounts).subscribe(s => {
                            console.log("Push Settings Updated", s);
                        });
                    })
                }
            })
        }
    }

    showToast(message: string) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: 2000,
            position: "bottom"
        });
        toast.present(toast);
    }

    logIn(username:string, password:string, nickname:string) : Observable<boolean> {
        return this.storageProvider.getAccount(username)
            .flatMap(m => {
                if(m) {
                    this.showToast('This account is already logged in.');
                    return null;
                }
                else {
                    console.log("Attempting to login user");
                    return this.getUserProfile(username, password, true, nickname).map(map => {
                      if(map) {
                          return true;
                      }
                      else
                        return false;
                    });
                }
            });
    }

    logOut(account: LocalAccount) : Observable<boolean> {
        return this.storageProvider.removeAccount(account).map(s=> {
            this.events.publish("account:removed", account);
            this.updatePushNotifications();
            return s;
        })
    }

    handleError(error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }

    updateLocalAccount(account: LocalAccount) : Observable<{}> {
        return this.storageProvider.updateAccount(account);
    }

    updateLocalAccountItem(account: LocalAccount, column, value) : Observable<{}> {
        return this.storageProvider.updateAccountItem(account, column, value);
    }

    reorder(accounts: LocalAccount[]) : Observable<boolean> {
        //TODO: Implement
        return this.storageProvider.reorderAccounts(accounts).map(m=> {
            return true;
        });
    }
}
