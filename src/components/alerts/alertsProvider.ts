import { Injectable } from '@angular/core';
import { WordpressProvider, StorageProvider } from '../../providers/providers';
import { Observable } from 'rxjs';

import { AppConfig } from '../../config';

export class Alert {
    alertText:string;
    alertId:number;
    dismissedInd:boolean;
    majorInd:boolean;
    alertUrl:string;
}

@Injectable()
export class AlertsProvider {

    constructor(private wp:WordpressProvider, private storageProvider:StorageProvider) {

    }

    getDismissedAlertIds() : Observable<Number[]> {
        return this.storageProvider.getDismissedAlertIds().map(dismissedAlerts => {
            if(!dismissedAlerts){
                dismissedAlerts = [];
            }
            return dismissedAlerts;
        });
    }

    getLibraryAlerts() : Observable<Alert[]> {
        if(AppConfig.APP_SHOW_ALERTS) {
            return this.wp.getPostsByCategory("3").flatMap(m=> {
                var alerts:Alert[] = [];
                let body = m.json();
                return this.getDismissedAlertIds().map(dismissedAlerts => {
                    body.forEach(a => {
                        var alert = new Alert();
                        alert.dismissedInd = dismissedAlerts.find(z=>z === a.id) ? true : false;
                        if(!alert.dismissedInd){
                            alert.alertId = a.id;
                            alert.alertText = a.title.rendered;
                            alert.majorInd = a.categories.find(z => z === 9);
                            alert.alertUrl = a.link;
    
                            alerts.push(alert);
                        }
                    });
                    return alerts;
                })
            });
        }
        else {
            return Observable.from([]);
        }
    }

    dismissAlert(alert:Alert){
        this.storageProvider.getDismissedAlertIds().subscribe(dismissedAlerts => {
            this.storageProvider.addDismissedAlertIds(alert.alertId).subscribe();
        })
    }
}