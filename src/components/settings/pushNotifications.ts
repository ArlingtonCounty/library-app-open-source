import { Component } from '@angular/core';

import { PushNotificationsProvider } from './pushNotificationsProvider';
import { AccountService, LocalAccount } from '../../providers/accountService';

import { AnalyticsProvider } from '../../providers/providers';
import { ITracking } from '../../app/app.component';
import { AppConfig } from '../../config';

@Component({
  selector: 'page-pushNotifications',
  templateUrl: 'pushNotifications.html'
})
export class PushNotificationsPage implements ITracking {
    inputs = {
        finesEnabled: false,
        overduesEnabled: false,
        holdsEnabled:false,
        alertsEnabled:false
    }
    public savedAccounts: LocalAccount[];
    title:string;

    constructor(private pushProvider:PushNotificationsProvider
            , private accountService:AccountService
            , private analytics:AnalyticsProvider) {
                this.title = AppConfig.APP_TITLE_HTML;
    }

    track() {
      this.analytics.trackView('PushNotificationsPage');
    }

    ionViewWillEnter() {
        this.accountService.getSavedAccounts().subscribe(accounts => this.savedAccounts = accounts);
        this.pushProvider.getSettings().subscribe(s => {
            if(s.length > 0){
                var setting = s[0];
                console.log(setting)
                this.inputs.overduesEnabled = setting.OverduesInd;
                this.inputs.finesEnabled = setting.FinesInd;
                this.inputs.holdsEnabled = setting.HoldsInd;
                this.inputs.alertsEnabled = setting.MiscInd;
            }
        });
    }

    updateSettings(ev) {
        console.log('update settings in component', ev);

        this.pushProvider.updateSettings(this.inputs.finesEnabled, this.inputs.overduesEnabled, this.inputs.holdsEnabled, this.inputs.alertsEnabled, this.savedAccounts).subscribe(s=> {
            console.log("result", s);
        });
    }

}