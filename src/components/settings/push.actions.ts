import { NgRedux } from '@angular-redux/store';
import { IAppState } from '../../store/IAppState';
import { Injectable } from '@angular/core';
import { NewsProvider } from '../news/newsProvider';

export const GET_NEWS_ITEM = 'news\GET';

@Injectable()
export class PushActions {
    constructor(
        private ngRedux:NgRedux<IAppState>, 
        private newsProvider:NewsProvider){
    }

    getNewsItem(id) {
        this.ngRedux.dispatch({
            type: GET_NEWS_ITEM,
            newsItem: []
        });

        this.newsProvider.getNewsItem(id).subscribe(newsItem => {
            this.ngRedux.dispatch({
                type: GET_NEWS_ITEM,
                newsItem: newsItem
            });
        });
    }
}
