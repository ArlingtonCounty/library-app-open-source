import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Device } from '@ionic-native/device';
import { Push, RegistrationEventResponse, NotificationEventResponse, AndroidPushOptions } from '@ionic-native/push';
import { Platform } from 'ionic-angular';
import { NewPage } from '../../components/components'
import { PushActions } from './push.actions';
import { AppConfig } from '../../config';



@Injectable()
export class PushNotificationsProvider {
    registrationId:string;
    baseUrl:string = `${AppConfig.DATAHUB_BASE_URL}notification/`;

    constructor(private http:Http,
                private device:Device,
                private platform:Platform,
                private actions:PushActions,
                private pushPlugin:Push){

    }

    register(notificationReceivedCallback) {
        var self = this;
        if(this.isWebview()) {
            var push = this.pushPlugin.init({
                android: {
                  senderID: "librarymobile-152621"
                },
                ios: {
                    alert: 'true',
                    badge: 'false',
                    sound: 'false'
                }
            });

            console.log('registering device for push')
            push.on('registration').subscribe(data =>  {
                console.log(data)
                var registration = data as RegistrationEventResponse
                self.registrationId = registration.registrationId;
                console.log('Registering device with id ', registration.registrationId)
            });

            push.on('notification').subscribe(s => {
                // data.message,
                // data.title,
                // data.count,
                // data.sound,
                // data.image,
                // data.additionalData
                var response = s as NotificationEventResponse;
                console.log("Push Notification Received", response);

                if(response.additionalData.post_id) {
                    var newsItemId = parseInt(response.additionalData.post_id)
                    this.actions.getNewsItem(newsItemId)
                }
                else if (response.additionalData.moredata.post_id) {
                    var newsItemId = parseInt(response.additionalData.moredata.post_id)
                    this.actions.getNewsItem(newsItemId)
                }
                else if(response.additionalData
                    && !response.additionalData.foreground
                    && response.additionalData.id) {

                    //Route to accounts
                    if(notificationReceivedCallback){
                        notificationReceivedCallback(response.additionalData.id);
                    }
                }
            });
        }


    }

    hasPermissions() {
        var promise = this.pushPlugin.hasPermission();

        promise.then(result => {
            console.log(result);
        });
        return promise;
    }

    isWebview() {
        return !this.platform.is('core');
    }

    getPlatform() {
        console.log(this.platform.platforms());
        return this.platform.is('ios') ? 'iOS' : 'Android';
    }

    getDeviceId() {
        if(!this.isWebview()){
            return "platformtestid";
        }
        return this.device.uuid;
    }

    getPushId() {
        if(!this.isWebview())
            return "pushid123456";
        return this.registrationId;
    }

    updateSettings(finesInd:boolean, overduesInd:boolean, holdsEnabled:boolean, alertsEnabled:boolean, accounts) {
        console.log("settings called");
        var name = this.getPlatform();
        var deviceId = this.getDeviceId();
        var pushId = this.getPushId();

        var accountString = "";
        accounts.forEach(a => accountString += `&AccountNumber=${a.barcode}&PatronNumber=${a.patronNumber}` );
        return this.http.post(`${this.baseUrl}savelibrarysettings?PlatformName=${name}&PlatformId=${deviceId}&PushId=${pushId}&FinesInd=${finesInd}&OverduesInd=${overduesInd}&HoldsInd=${holdsEnabled}&MiscInd=${alertsEnabled}${accountString}`, "").map(m=> {
            var body = m.json();
            console.log("Settings Update", body);
            return body;
        });
    }

    getSettings() {
        var deviceId = this.getDeviceId();
        return this.http.get(`${this.baseUrl}librarysettings?platformId=${deviceId}&platformName=${this.getPlatform()}`).map(m=> {
            var body = m.json();
            console.log(body);
            return body;
        });
    }
}
