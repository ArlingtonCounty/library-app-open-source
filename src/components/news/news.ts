import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NewsProvider } from './newsProvider';
import { NewPage } from './new';

import { AnalyticsProvider } from '../../providers/providers';
import { ITracking } from '../../app/app.component';
import { AppConfig } from '../../config';

@Component({
  selector: 'page-news',
  templateUrl: 'news.html'
})
export class NewsPage implements ITracking {
  news: any[];
  nextPageAvailable: boolean = true;
  page: number = 1;
  perPage: number = 20;
  title:string;

  constructor(private newsProvider: NewsProvider
    , private navCtrl: NavController
    , private analytics: AnalyticsProvider) { 
      this.news = [];
      this.title = AppConfig.APP_TITLE_HTML;
    }

  track() {
    this.analytics.trackView('NewsPage');
  }

  ionViewWillEnter() {
    if (this.news.length <= 0) {
      this.loadNews();
    }
  }

  goToNew(newsItem) {
    this.navCtrl.push(NewPage, { newsItem: newsItem });
  }

  loadNews() {
    this.newsProvider.getNews(this.perPage, this.page).subscribe(s => {
      s.forEach(newsItem => {
        this.news.push(newsItem);
      })

      if (this.perPage > s.length) {
        this.nextPageAvailable = false;
      }
    })
  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');

    setTimeout(() => {
      if (this.nextPageAvailable) {
        this.page++
        this.loadNews();
      }
      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 1500);
  }
}