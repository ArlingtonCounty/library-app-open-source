import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { WordpressProvider } from '../../providers/wordpressProvider';
import { AppConfig } from '../../config';

export class NewsItem {

  constructor(json:any){
    this.raw = json;
    this.title = json.title.rendered;

    this.content = json.content.rendered;
    var scriptTags = this.content.indexOf("<script");
    this.removeScriptTags(scriptTags);

    this.excerpt = json.excerpt.rendered;
    this.linkUrl = json.link;

    var sticky = false;
    json.tags.forEach(tag => {
      if (tag == 296) {
        sticky = true;
      }
    });
    this.sticky = sticky;

    this.imageUrl = "";
    
    if (json.hasOwnProperty('_embedded')) {
        var featuredMedia = json._embedded['wp:featuredmedia'];
        if(featuredMedia && featuredMedia.length > 0) {
            var media = featuredMedia[0];
            if(media.media_details && media.media_details.sizes && media.media_details.sizes.full){
                this.imageUrl = media.media_details.sizes.full.source_url
                    //get the demo to work
                    .replace(/\/wp-api.org\//, "/demo.wp-api.org/");
            }
        }
    }
  }

  removeScriptTags(scriptTags:number) {
      if(scriptTags > 0) {
        var start = this.content.slice(0,scriptTags)
        var scriptEndTag = this.content.indexOf("</script>");
        var end = this.content.slice(scriptEndTag + 9);
        this.content = start + end;

        var moreTags = this.content.indexOf("<script");
        this.removeScriptTags(moreTags);
    }
  }

  title:string;
  excerpt:string;
  content:string;
  imageUrl:string;
  linkUrl:string;
  sticky:boolean;

  raw:Object;
}


@Injectable()
export class NewsProvider {

    constructor(private wp:WordpressProvider) { }

    getNews(perPage?:number, page?:number, tag?:number) : Observable<NewsItem[]> {
        if(AppConfig.APP_SHOW_NEWS) {
            return this.wp.getPostsByCategory("280", perPage, page, tag).map(m => {
                let body = m.json();
                let newsItems = new Array<NewsItem>();
    
                body.forEach(post => {
                    newsItems.push(new NewsItem(post));
                });
                return newsItems;
            });
        }
        else {
            return Observable.from([]);
        }
    }

    getNewsItem(id) : Observable<NewsItem> {
        return this.wp.getPostsById(id).map(m => {
            let body = m.json();
            let newsItem = new NewsItem(body)

            return newsItem;
        });
    }
}
