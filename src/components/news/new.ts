import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';

import { AnalyticsProvider } from '../../providers/providers';
import { ITracking } from '../../app/app.component';
import { AppConfig } from '../../config';

@Component({
  selector: 'page-new',
  templateUrl: 'new.html'
})
export class NewPage implements ITracking {
    newsItem;
    loaded:boolean = false;
    title:string;

    constructor(private navParams: NavParams
            , private analytics:AnalyticsProvider){ 
                this.title = AppConfig.APP_TITLE_HTML;
            }

    track() {
      this.analytics.trackView('NewPage');
    }

    ionViewWillEnter(){
        this.newsItem = this.navParams.get('newsItem');
        this.loaded = true;
    }

    goToNewsItem() {
        window.open(this.newsItem.linkUrl, '_system');
    }

}