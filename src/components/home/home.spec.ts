import { HomePage } from './home';
import { CustomDatePipe } from '../../pipes/customDate'
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from '@angular/core';

import { NavController, ModalController, AlertController } from 'ionic-angular';
import { AccountService, EventsProvider, AnalyticsProvider, AnalyticsProviderMock } from '../../providers/providers';
import { CatalogProvider } from '../catalog/catalogProvider';
import { AlertsProvider } from '../alerts/alertsProvider';
import { NewsProvider } from '../news/newsProvider';
import { NavControllerMock } from '../../app/app.mocks';

describe('Home Page:', () => {

    let comp: HomePage;
    let fixture: ComponentFixture<HomePage>;
    let de: DebugElement;
    var navControllerMock;

    beforeEach(() => {
        navControllerMock = new NavControllerMock();

        TestBed.configureTestingModule({
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            declarations: [HomePage, CustomDatePipe],
            providers: [
                { provide: NavController, useValue: navControllerMock},
                { provide: ModalController, useValue: ModalController },
                { provide: AlertController, useValue: AlertController },
                { provide: CustomDatePipe, useValue: CustomDatePipe },
                { provide: AccountService, useValue: AccountService },
                { provide: EventsProvider, useValue: EventsProvider },
                { provide: CatalogProvider, useValue: CatalogProvider },
                { provide: AlertsProvider, useValue: AlertsProvider },
                { provide: NewsProvider, useValue: NewsProvider },
                { provide: AnalyticsProvider, useValue: new AnalyticsProviderMock() }
            ],
        });
        fixture = TestBed.createComponent(HomePage);
        // #trick
        // If you want to trigger ionViewWillEnter automatically de-comment the following line
        // fixture.componentInstance.ionViewWillEnter();
        fixture.detectChanges();
        comp = fixture.componentInstance;
        de = fixture.debugElement;
    });

    describe('.constructor()', () => {
        it('Should be defined', () => {
            expect(comp).toBeDefined();
        });
    });

    describe('Google Analytics', () => {
        it('Should track the correct page', () => {
            var page = comp.track();
            expect(page).toEqual("HomePage");
        })
    });

    describe('Home', () => {
        it('Should default to "Hot"', () => {
            expect(comp.view).toBe("hot");
        })

        it('GoToEvent() Should Call navCtrl push', () => {
            spyOn(navControllerMock, 'push');
            
            comp.goToEvent({});

            expect(navControllerMock.push).toHaveBeenCalled();
        });
    });
});
