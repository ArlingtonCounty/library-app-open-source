import { Component, ViewChild } from '@angular/core';

import { NavController, ModalController, AlertController, Platform, Content } from 'ionic-angular';

import { LocalAccount, AccountService, Event, EventsProvider, defaultEventsFilter, StorageProvider } from '../../providers/providers';

import { NewsPage, LoginPage, EventsPage, AccountPage, HoldsPage, LibrariesPage, BrowsePage, CatalogPage, EventPage, NewPage, BookPage, IntroPage } from '../../components/components'
import { CatalogProvider } from '../catalog/catalogProvider';

import { AlertsProvider, Alert } from '../alerts/alertsProvider';
import { NewsProvider, NewsItem } from '../news/newsProvider';

import { AnalyticsProvider } from '../../providers/providers';
import { ITracking } from '../../app/app.component';

import { Observable } from 'rxjs/Observable';
import { select } from '@angular-redux/store';
import { AccountActions } from '../account/account.actions';
import { AppConfig } from '../../config';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements ITracking {
  @ViewChild(Content) content: Content;

  @select('localAccounts')
  readonly localAccounts$ : Observable<LocalAccount[]>;

  title:string;
  pikaUrl:string;
  view:string;
  showAlerts:boolean;
  showEvents:boolean;
  showNews:boolean;
  public accounts:LocalAccount[];
  public events:Event[];
  public featuredTitles:any[];
  public alerts:any;
  public news:any[];

  constructor(private navCtrl: NavController
            , private accountService: AccountService
            , private modalCtrl:ModalController
            , private eventsProvider:EventsProvider
            , private catalogProvider:CatalogProvider
            , private alertsProvider:AlertsProvider
            , private newsProvider:NewsProvider
            , private alertCtrl:AlertController
            , private analytics:AnalyticsProvider
            , private storageProvider:StorageProvider
            , private platform:Platform
            , private actions:AccountActions) {

    this.title = AppConfig.APP_TITLE_HTML;
    this.pikaUrl = AppConfig.PIKA_BASE_URL.substring(0, AppConfig.PIKA_BASE_URL.length-1);
    this.showAlerts = AppConfig.APP_SHOW_ALERTS;
    this.showEvents = AppConfig.APP_SHOW_EVENTS;
    this.showNews = AppConfig.APP_SHOW_NEWS;

    this.accounts = [];
    this.view = AppConfig.APP_SHOW_NEWS ? "hot" : "lists";
    this.featuredTitles = [];

    this.platform.ready().then(() => {
      //Load accounts stored locally for quick presentation
      this.actions.getAccounts();

      setInterval(() => {
        //Check if home page is visible
        var name = this.navCtrl.getActive().name;
        if(name === 'HomePage') {
          //Load profiles to check for alerts
          this.actions.getAllProfiles();
        }
      }, 60000);  //Refresh every minute

      setTimeout(() => {
        //update push notification settings on starting up app
        this.accountService.updatePushNotifications();
      }, 1000);

      this.actions.getAllProfiles();
    });
  }

  track() {
      var page = "HomePage";
      this.analytics.trackView(page);
      return page;
  }

  //Links
  goToLibraries(){
    this.navCtrl.push(LibrariesPage);
  }

  goToEvents() {
    this.navCtrl.push(EventsPage);
  }

  goToNews() {
    this.navCtrl.push(NewsPage);
  }

  goToBrowse() {
    this.navCtrl.push(BrowsePage);
  }

  goToAccount(account) {
    this.navCtrl.push(AccountPage, {account: account});
  }

  goToHolds() {
    this.navCtrl.push(HoldsPage);
  }

  goToCatalog() {
    console.log("Height", this.content.contentHeight);
    console.log("Top", this.content.contentTop);
    setTimeout(() => this.navCtrl.push(CatalogPage), 0);
  }

  ionViewWillEnter() {
    this.eventsProvider.eventsApi(0, defaultEventsFilter, "", false, false, 25).subscribe(s => {
      this.events = s.slice(0,10);
    });

    this.catalogProvider.getFeaturedTitles().subscribe(s=> {
      this.featuredTitles = s;
    });

    this.alerts = [];
    this.alertsProvider.getLibraryAlerts().subscribe(s => {
      this.alerts = s;
    });

    this.news = []
    // tag 296 is for sticky items
    this.newsProvider.getNews(1, 1, 296).subscribe(s => {
      s.forEach(newsItem => {
        this.news.unshift(newsItem);
      })
    }, error => {
      let alert = this.alertCtrl.create({
          title: 'Error',
          message: 'Unable to retrieve data.  Check your internet connection.',
          buttons: ['OK']
      });
      alert.present();
    });

    this.newsProvider.getNews(5).subscribe(s => {
      s.forEach(newsItem => {
        if (!newsItem.sticky) {
          this.news.push(newsItem);
        }
      })
    }, error => {
      let alert = this.alertCtrl.create({
          title: 'Error',
          message: 'Unable to retrieve data.  Check your internet connection.',
          buttons: ['OK']
      });
      alert.present();
    });
  }

  ionViewDidLoad() {
    console.log("Height", this.content.contentHeight);
    console.log("Top", this.content.contentTop);

    this.storageProvider.migrateLocalStorage().subscribe(a => console.log(a), err => console.error(err), () => {
      this.storageProvider.showIntro().subscribe(intro => {
        if (intro == null || intro) {
          this.storageProvider.updateShowIntro(false).subscribe(s => {
            this.navCtrl.setRoot(IntroPage);
          });
        }
      })
    });
  }

  openModal() {
    let loginModal = this.modalCtrl.create(LoginPage);
    loginModal.onDidDismiss(dismiss => {
      this.accountService.getSavedAccounts().subscribe(accounts => {
        this.accounts = accounts;
      });
    });
    loginModal.present();
  }

  goToBook(bookId) {
    var self = this;
    self.catalogProvider.getBookById(bookId).subscribe(s =>{
        if(s.results.length === 1){
          self.navCtrl.push(BookPage, { book: s.results[0] });
        }
        else {
          let alert = this.alertCtrl.create({
              title: 'Error',
              subTitle: 'Unable to find scanned title, try keyword search.',
              buttons: ['OK']
          });
          alert.present();
        }
      });
  }

  goToEvent(event) {
    this.navCtrl.push(EventPage, {event: event});
  }

  goToNewsItem(newsItem:NewsItem){
    // window.open(newsItem.linkUrl, '_system');
    this.navCtrl.push(NewPage, {newsItem: newsItem});
  }

  scan() {
    var self = this;
    (<any>window).cordova.plugins.barcodeScanner.scan(
      function (result) {
          if(!result.cancelled){
            self.goToBook(result.text);
          }
      },
      function (error) {
          let alert = this.alertCtrl.create({
              title: 'Sorry Unable to find scanned title, try keyword search.',
              subTitle: error,
              buttons: ['OK']
          });
          alert.present();
      },
      {
          "preferFrontCamera" : false, // iOS and Android
          "showFlipCameraButton" : true, // iOS and Android
          "prompt" : "Place a barcode inside the scan area", // supported on Android only
          "resultDisplayDuration": 0, //Android prevent result display
          "disableAnimations" : true
      }
   );
  }

  dismissAlert(alert:Alert){
    alert.dismissedInd = true;
    this.alertsProvider.dismissAlert(alert);
    var index = this.alerts.findIndex(f=> f === alert.alertId);
    this.alerts.splice(index, 1);
  }

  goToAlert(alert:Alert){
    window.open(alert.alertUrl, '_system');
  }
}
