import { Component, ViewChild, HostListener } from '@angular/core';
import { NavController, LoadingController, Loading, AlertController, Platform } from 'ionic-angular';

import { Geolocation } from '@ionic-native/geolocation';
import { Diagnostic } from '@ionic-native/diagnostic';

import { Library, LibrariesProvider } from './librariesProvider';
import { LibraryPage } from '../components';

import { AnalyticsProvider } from '../../providers/providers';
import { ITracking } from '../../app/app.component';

import _ from 'lodash';
import geolib from 'geolib';
import { AppConfig } from '../../config';

declare var google;

@Component({
  selector: 'page-libraries',
  templateUrl: 'libraries.html'
})
export class LibrariesPage implements ITracking {
  @ViewChild('map') map;
  loader:Loading;
  libraries:Library[];
  view:string;
  openNow:boolean;
  coords:any;
  mapClass:string;
  googleMap:any;
  markers:any[] = [];
  lastUpdate:Date = null;
  title:string;
  @HostListener('window:library', ['$event'])
    testListener(event) {
      this.goToLibrary(event.detail);
    }
  
  constructor(public navCtrl: NavController
            , private librariesProvider:LibrariesProvider
            , private loadingCtrl:LoadingController
            , private analytics:AnalyticsProvider
            , private alertCtrl:AlertController
            , private platform:Platform
            , private diagnostic:Diagnostic
            , private geolocation:Geolocation) {
    this.title = AppConfig.APP_TITLE_HTML;
    this.view = 'all';
    this.openNow = false;
    
  }

  track() {
      this.analytics.trackView('LibrariesPage');
    }

  ionViewWillEnter() {
    this.mapClass = "hide-map";
    this.view = 'all';
    this.markers = [];

    this.googleMap = new google.maps.Map(this.map.nativeElement, { 
        center: {lat: 38.883828, lng: -77.107297},
        zoom: 12
    });

    this.loadData();
  }

  loadLibraries(libraries, callback) {
    this.libraries = libraries;
    this.loader.dismissAll();
    this.lastUpdate = new Date();
    if(callback) callback();
  }

  locationCheck() {
    if(this.platform.is('ios') || this.platform.is('android')){
      return this.diagnostic.isLocationEnabled();
    } else {
      return new Promise((resolve, reject) => {
        resolve(true);
      });
    }
  }

  loadData(successCallback?, errorCallback?) {
    this.loader = this.loadingCtrl.create();
    this.loader.present();

    this.librariesProvider.getLibraries().subscribe(s => {
        //if geolocation is enabled 
        //calculate distance for all
        //sort by distance
        this.locationCheck().then(success=> {
          if(success){
            this.geolocation.getCurrentPosition({ timeout: 300000, maximumAge: 300000, enableHighAccuracy: false})
              .then((resp) => {
                var recenter = !this.coords;

                var coords = { 
                  lat: resp.coords.latitude, 
                  lng: resp.coords.longitude,
                  lon: resp.coords.longitude,
                  latitude: resp.coords.latitude, 
                  longitude: resp.coords.longitude, 
                };
                this.coords = coords;
              
                if(this.coords && recenter){
                  var position = new google.maps.LatLng(coords.lat, coords.lng);
                  this.googleMap.setCenter(position);
                  new google.maps.Marker({
                      'position' : position,
                      'map'  : this.googleMap,
                      'icon' : {
                          'path'        : google.maps.SymbolPath.CIRCLE,
                          'scale'       : 8,
                          'strokeColor' : '#2e8be8',
                          'fillColor'   : '#005cb8',
                          'fillOpacity' : 1.0,
                      }
                  });
                }

                s.forEach(library => {
                  if(library.latitude && library.longitude){
                    var l = {
                      lat: Number(library.latitude),
                      lng: Number(library.longitude),
                      latitude: Number(library.latitude),
                      longitude: Number(library.longitude)
                    };
                    
                    library.distance = geolib.getDistance(coords, l) / 1609.34;
                  }
                });
                this.libraries = _.sortBy(s, ["distance"]);
                this.loadLibraries(this.libraries, successCallback);
              }, (error) => {
                this.loadLibraries(s, successCallback);
              }).catch((error) => {
                this.libraries = s;
                this.loader.dismissAll();
                if(errorCallback) errorCallback();
            });
          }
          else { //Location Disabled
            this.loadLibraries(s, successCallback);
          }
        }, error => {  //Error determining location availability
          this.loadLibraries(s, successCallback);
        })
    }, error => {  //Error retrieving libraries
      this.loader.dismissAll();
      let alert = this.alertCtrl.create({
          title: 'Error',
          message: 'Unable to retrieve data.  Please check your internet connection.',
          buttons: ['OK']
      });
      alert.present();
      
    });
  }

  goToLibrary(id) {
    this.navCtrl.push(LibraryPage, {id: id});
  }  

  showMap() {
      this.mapClass = 'show-map';
      setTimeout(t=> {
        var center = this.googleMap.getCenter();
        google.maps.event.trigger(this.googleMap, 'resize');
        this.googleMap.setCenter(center);
      }, 100)

      this.markers.forEach(marker => {
        marker.map = null;
      });

      this.markers = [];

      this.libraries.forEach(library => {
        
        if(!library.name.startsWith("-")){
          var l = {
              lat: Number(library.latitude),
              lng: Number(library.longitude)
            };
          let marker = new google.maps.Marker({
              position: l,
              map: this.googleMap,
              title: library.name
            });
          this.markers.push(marker);
          var infowindow = new google.maps.InfoWindow({
            content: `<p>
                        <a href="#" onclick="window.dispatchEvent(new CustomEvent('library', { detail: ${library.id} } )); return false;">
                          <strong>${library.name}</strong>
                        </a>
                      </p>
                      <p>${(library.currentlyOpen ? 'Open Today ' : '')} ${library.hoursFormatted}</p>
                      <p>${(library.distance ? library.distance.toFixed(1) + " miles away." : "")}</p>`
          });
          marker.addListener('click', function() {
            infowindow.open(this.googleMap, marker);
          });
        }
      });
         
  }

  hideMap() {
    this.mapClass = "hide-map";
  }

  doRefresh(refresher) {
      this.loadData(success => {
        refresher.complete();
        if(this.view === 'map'){
          this.showMap();
        }
      }, error => {
        refresher.complete();
      });
      
  }
}