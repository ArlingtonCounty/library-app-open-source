
import { Injectable } from '@angular/core';

import { LibcalProvider } from '../../providers/libcalProvider'

import _ from 'lodash';
import $ from 'jquery';

export class LibraryHours {
    date:Date;
    dateString:string;
    hoursFormatted:string;
    status:string;
    currentlyOpen:boolean;
    isPast:boolean;

    static fromJson(obj:any) : LibraryHours{
        var lh = new LibraryHours();
        lh.dateString = obj.date;
        lh.date = new Date(obj.date + 'T12:00:00.000Z');
        lh.hoursFormatted = obj.rendered;
        lh.currentlyOpen = obj.times && obj.times.currently_open ? obj.times.currently_open : obj.currently_open;
        
        //See if the date is in the past
        var d = new Date();
        var today = new Date(d.toDateString());
        
        lh.isPast = lh.date.getTime() < today.getTime();
        return lh; 
    }
}

export class Library {
  id:number;
  name:string;
  address:string;
  url:string;
  currentlyOpen:boolean;
  status:string;
  hoursFormatted:string;
  image:string;
  phoneNumbers:any[];
  parentId:number;
  parentLibrary:Library;

  latitude:number;
  longitude:number;

  distance:Number;

  hours:LibraryHours[] = [];

  static fromJson(obj:any, lid:Number) : Library {
      var l = new Library();
      
      if(!obj) return null;

      l.id = obj.lid || lid;
      l.name = obj.name;
      l.url = obj.url;
      
      l.latitude = obj.lat;
      l.longitude = obj.long;

      l.image = obj.fn;
      l.phoneNumbers = [];

      l.parentId = obj.parent_lid || null;
      var html = $.parseHTML(obj.contact);
      //try to parse out the phone numbers and address
      $(html).each((index, element) => {
          var j = $(element);
          if(j.attr('id') || j.attr('class')){
            if(j.attr('id') === 'contact-address') {
                l.address = j.attr('data-address-street');
            }
            else if(j.attr('id') === 'branch-image') {
                l.image = j.attr('data-src');
            }
            else if (j.attr('class') === 'contact-telephone') {
                
                l.phoneNumbers.push({
                    title: j.attr('data-title'),
                    number: j.attr('data-tel')
                })
            }
          }
      });


      if(obj.address){l
          var split = obj.address.split("$");
          if(split.length > 1)
          l.address = split[1];
      }
      
      if(!obj.weeks){
        l.hours.push(LibraryHours.fromJson(obj.times));
        l.currentlyOpen = obj.times.currently_open;
        l.status = obj.times.status;
        l.hoursFormatted = obj.rendered;
      }
      else {
        obj.weeks.forEach(week => {
            var keys = Object.keys(week);
            keys.forEach(key => {
                var h = LibraryHours.fromJson(week[key]);
                
                if(!h.isPast && l.hours.length < 7) l.hours.push(h);
            });
            l.currentlyOpen = (_.find(l.hours, a => a.currentlyOpen === true) !== undefined);
        });
      } 
      
      return l;
  }
} 

@Injectable()
export class LibrariesProvider { 
    constructor(private libcal:LibcalProvider) {} 

    getLibraries() {
        return this.libcal.getLibraries().map(b => {
            let body = b.json();
            var libs = new Array<Library>();
            body.locations.forEach(entry => libs.push(Library.fromJson(entry, null)));

            libs.forEach(library => {
                if(library.parentId){
                    library.parentLibrary = _.find(libs, {id: library.parentId}); 
                }
            })
            return libs;
        });  
    }

    getLibrary(id:number) {
        return this.libcal.getLibrary(id).map(b => {
            let body = b.json();
            var library = Library.fromJson(body["loc_" + id], id);
            return library;
        });
    }
}