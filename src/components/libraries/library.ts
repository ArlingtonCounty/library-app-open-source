import { Component, ViewChild } from '@angular/core';
import { NavParams, LoadingController, AlertController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { Library, LibrariesProvider } from './librariesProvider';

import { AnalyticsProvider, AnalyticsDimensions, LinkProvider } from '../../providers/providers';
import { ITracking } from '../../app/app.component';
import { AppConfig } from '../../config';

declare var google;

@Component({
  selector: 'page-library',
  templateUrl: 'library.html'
})
export class LibraryPage implements ITracking {
  @ViewChild('librarymap') map;
  library:Library;
  lat = 0;
  lng = 0;
  zoom  = 12;
  loaded:boolean = false;

  googleMap:any;
  marker:any;
  title:string;

  constructor(private navParams: NavParams
            , private librariesProvider:LibrariesProvider
            , private loadingCtrl:LoadingController
            , private linkProvider:LinkProvider
            , private analytics:AnalyticsProvider
            , private alertCtrl:AlertController
            , private geolocation:Geolocation) {
    this.title = AppConfig.APP_TITLE_HTML;
    this.library = new Library();
  }

  track() {
    var dimensions = {};
    
    dimensions[AnalyticsDimensions.Library] = this.navParams.get('id');

    this.analytics.trackView('LibraryPage', dimensions);
  }

  ionViewWillEnter() {
    var self = this;
    let id = this.navParams.get('id');
    let loader = this.loadingCtrl.create();
    loader.present();
    this.librariesProvider
      .getLibrary(id).subscribe(s => {
          this.library = s;
          
          let position = { lat: Number(s.latitude), lng: Number(s.longitude) };
          var map = new google.maps.Map(this.map.nativeElement, { 
            center: position,
            zoom: 13
          });
          this.marker = new google.maps.Marker({
            position: position,
            map: map
          });
          var infowindow = new google.maps.InfoWindow({
            content: `<p>
                        <strong>${this.library.name}</strong>
                      </p>
                      <p>${(this.library.hours[0].hoursFormatted !== 'Closed' ? 'Open Today ' : '')} ${this.library.hours[0].hoursFormatted}</p>
                      <p></p>
                      <p>${(this.library.distance ? this.library.distance.toFixed(1) + " miles away." : "")}</p>`
          });
          this.marker.addListener('click', function() {
            infowindow.open(map, self.marker);
          });
          
          this.geolocation.getCurrentPosition({ timeout: 300000, maximumAge: 300000, enableHighAccuracy: false})
          .then((resp) => {
            var userPosition = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
              new google.maps.Marker({
                  'position' : userPosition,
                  'map'  : map,
                  'icon' : {
                      'path'        : google.maps.SymbolPath.CIRCLE,
                      'scale'       : 8,
                      'strokeColor' : '#2e8be8',
                      'fillColor'   : '#005cb8',
                      'fillOpacity' : 1.0,
                  }
              });
              loader.dismissAll();
              
          }, error => {
            console.log(error);
            loader.dismissAll();
          });
          this.loaded = true;
      }, error => {
        let alert = this.alertCtrl.create({
            title: 'Error',
            message: 'Unable to retrieve data.  Check your internet connection.',
            buttons: ['OK']
        });
        alert.present();
        loader.dismissAll();
      });
  }

  call(phoneNumber) {
    this.linkProvider.callPhoneNumber(phoneNumber);
  }

  openInMaps(library) {
    this.linkProvider.openInMaps(library.latitude, library.longitude, library.name);
  }
}