import { Component, ViewChild } from '@angular/core';

import { NavController, NavParams, Platform } from 'ionic-angular';
import { Event } from '../../providers/providers'
import { Library, LibrariesProvider } from '../libraries/librariesProvider';
import { LibraryPage } from '../libraries/library';

import { AnalyticsProvider, LinkProvider } from '../../providers/providers';
import { ITracking } from '../../app/app.component';
import { DomSanitizer } from '@angular/platform-browser'
import { AppConfig } from '../../config';

import _ from 'lodash';

declare var google;

@Component({
  selector: 'page-event',
  templateUrl: 'event.html'
})
export class EventPage implements ITracking {
    @ViewChild('map') map;
    lat = 0;
    lng = 0;
    zoom  = 12;
    marker:any;
    isIos:boolean;
    
    public event: Event;
    public eventsCount: number;
    libraries:Library[];
    mapUrl;
    title:string;

    constructor(private navCtrl: NavController
              , private navParams: NavParams
              , private librariesProvider:LibrariesProvider
              , private platform:Platform
              , private linkProvider:LinkProvider
              , private domSanitizer:DomSanitizer
              , private analytics:AnalyticsProvider) { 
                  this.title = AppConfig.APP_TITLE_HTML;
                  this.event = this.navParams.get('event');
                  this.isIos = platform.is('ios');
                  this.generateMapUrl();
    }

    track() {
      this.analytics.trackView('EventPage');
    }

    ionViewWillEnter() {
        this.event = this.navParams.get('event');
        if(this.event.locationLatitudeCrd && this.event.locationLongitudeCrd) {
            this.generateMap()
        }

        this.librariesProvider.getLibraries().subscribe(s=> {
            this.libraries = s;
        })
    }

    call(phoneNumber) {
        this.linkProvider.callPhoneNumber(phoneNumber);
    }

    openInBrowser(link: string) {
        window.open(link, '_system');
    }

    generateMapUrl() {
        var coords = this.event.locationLatitudeCrd + "," + this.event.locationLongitudeCrd;
        if (this.isIos) {
            this.mapUrl = this.domSanitizer.bypassSecurityTrustUrl("http://maps.apple.com/?q=" + coords);
        }
        else {
            this.mapUrl = this.domSanitizer.bypassSecurityTrustUrl('geo:' + coords + "?q=" + this.event.locationName);
        }
    }

    goToLibrary() {
        if (this.event.locationName) {
            let locationName = ''
            if (this.event.locationName === 'Central Library') {
                locationName = 'Central Library'
            }
            else {
                locationName = this.event.locationName.substring(0, this.event.locationName.length - 8)
            }

            let library: Library = _.find(this.libraries, { 'name': locationName })
            this.navCtrl.push(LibraryPage, { id: library.id });
        }
    }

    generateMap() {
        let position = { lat: Number(this.event.locationLatitudeCrd), lng: Number(this.event.locationLongitudeCrd) };
          
        var map = new google.maps.Map(this.map.nativeElement, { 
            center: position,
            zoom: 13
        });
        this.marker = new google.maps.Marker({
            position: position,
            map: map
        });
    }
}