import { Component } from '@angular/core';

import { NavParams, ViewController } from 'ionic-angular';
import { EventsProvider, EventsFilter, LibraryInfoProvider } from '../../providers/providers';


@Component({
  templateUrl: 'eventsfilter.html'
})
export class EventsFilterPage {
  sortBy: string;
  within: number;
  nearBus: boolean;
  nearMetro: boolean; 
  nearBike: boolean;
  nearParking: boolean;
  locationName: string;
  libraries;

  public filter: EventsFilter;
  public dismissed: boolean = false;

  constructor(public viewCtrl: ViewController,
    private navParams: NavParams,
    private eventsProvider: EventsProvider,
    private libraryInfoProvider: LibraryInfoProvider) {
    this.filter = this.navParams.get('filter');
    this.libraries = this.libraryInfoProvider.returnLibraryLocationTags();

    this.sortBy = this.eventsProvider.getSortBy();
    this.within = this.eventsProvider.getWithin();
    this.nearBus = this.eventsProvider.getNearBus();
    this.nearBike = this.eventsProvider.getNearBike();
    this.nearMetro = this.eventsProvider.getNearMetro();
    this.nearParking = this.eventsProvider.getNearParking();
    this.locationName = this.eventsProvider.getLocationName();
  }

  ionViewDidLeave() {
    if(!this.dismissed) {
      this.filter = this.navParams.get('filter');
    }
  }

  apply() {
    this.dismissed = true;
    this.eventsProvider.setSortBy(this.sortBy);
    this.eventsProvider.setWithin(this.within);
    this.eventsProvider.setNearBus(this.nearBus || false);
    this.eventsProvider.setNearBike(this.nearBike || false);
    this.eventsProvider.setNearMetro(this.nearMetro || false);
    this.eventsProvider.setParking(this.nearParking || false);
    this.eventsProvider.setLocationname(this.locationName);
    this.viewCtrl.dismiss(this.eventsProvider.getEventsFilter());
  }

  cancel() {
    this.viewCtrl.dismiss();
  }

  onSortChange(ev) {
    console.log(ev);
    this.sortBy = ev;
  }

  onWithinChange(ev) {
    this.within = ev;
  }

  onNearBusChange(ev) {
    this.nearBus = ev;
  }

  onNearMetroChange(ev) {
    this.nearMetro = ev;
  }

  onNearBikeChange(ev) {
    this.nearBike = ev;
  }

  onNearParkingChange(ev) {
    this.nearParking = ev;
  }

  onLocationNameChange(ev) {
    this.locationName = ev;
  }

  reset() {
    this.eventsProvider.setSortBy('date');
    this.eventsProvider.setWithin(-1)
    this.eventsProvider.setNearBus(false)
    this.eventsProvider.setNearBike(false)
    this.eventsProvider.setNearMetro(false)
    this.eventsProvider.setParking(false)
    this.eventsProvider.setLocationname('All')

    this.sortBy = this.eventsProvider.getSortBy();
    this.within = this.eventsProvider.getWithin();
    this.nearBus = this.eventsProvider.getNearBus();
    this.nearBike = this.eventsProvider.getNearBike();
    this.nearMetro = this.eventsProvider.getNearMetro();
    this.nearParking = this.eventsProvider.getNearParking();
    this.locationName = this.eventsProvider.getLocationName();

    this.filter = this.eventsProvider.getEventsFilter();
  }

}
