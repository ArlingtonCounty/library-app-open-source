import { Component, ElementRef, ViewChild } from '@angular/core';

import { NavController, LoadingController, PopoverController, Content, AlertController, Platform} from 'ionic-angular';
import { EventsProvider, Event, EventsFilter } from '../../providers/providers';
import { EventPage, EventsFilterPage } from '../components';
import { Geolocation } from '@ionic-native/geolocation';
import { Keyboard } from '@ionic-native/keyboard';
import { Diagnostic } from '@ionic-native/diagnostic';
import { AnalyticsProvider } from '../../providers/providers';
import { ITracking } from '../../app/app.component';
import { AppConfig } from '../../config';

@Component({
  selector: 'page-events',
  templateUrl: 'events.html'
})
export class EventsPage implements ITracking {
  public events: Event[];
  public totalEvents: number;
  public view: string;
  public eventsToLoad: number;
  public eventsLoadedCount: number;
  public filter: EventsFilter;
  public coords:any;
  public searchText:string = '';
  title:string;

  @ViewChild(Content) content: Content;
  @ViewChild('searchbar') searchbar:ElementRef;

    constructor(private navCtrl: NavController
              , private eventsProvider: EventsProvider
              , private loadingCtrl:LoadingController
              , public popoverCtrl: PopoverController
              , private analytics:AnalyticsProvider
              , private alertCtrl:AlertController
              , private platform:Platform
              , private diagnostic:Diagnostic
              , private geolocation:Geolocation
              , private keyboard:Keyboard) { 
      this.title = AppConfig.APP_TITLE_HTML;
      this.view = 'list';
      this.eventsToLoad = 0;
      this.eventsLoadedCount = 0;
      this.filter = this.eventsProvider.getEventsFilter();
    }

    track() {
      this.analytics.trackView('EventsPage');
    }

    locationCheck() {
      if(this.platform.is('ios') || this.platform.is('android')){
        return this.diagnostic.isLocationEnabled();
      } else {
        return new Promise((resolve, reject) => {
          resolve(true);
        });
      }
    }

    ionViewWillEnter() {
      let loader = this.loadingCtrl.create();
      loader.present();

      this.locationCheck().then(successCallback => {
        if(successCallback){
          this.geolocation.getCurrentPosition({ timeout: 300000, maximumAge: 300000, enableHighAccuracy: false })
            .then((resp) => {

              var coords = {
                lat: resp.coords.latitude,
                lng: resp.coords.longitude,
              };
              this.coords = coords;
              this.getEvents(this.eventsToLoad, this.filter, this.coords);
              loader.dismissAll();
            }, (error) => {
              this.coords = false;
              this.getEvents(this.eventsToLoad, this.filter, this.coords);
              loader.dismissAll();
            }).catch((error) => {
              this.coords = false;
              this.getEvents(this.eventsToLoad, this.filter, this.coords);
              loader.dismissAll();
            });
        }
        else {
          this.coords = false;
          this.getEvents(this.eventsToLoad, this.filter, this.coords);
          loader.dismissAll();
        }
      });
    }

    closeKeyboard() {
      this.searchbar.nativeElement.blur();
      this.keyboard.close();
    }

    searchEvent(event: any) {
      this.eventsToLoad = 0;
      this.getEvents(this.eventsToLoad, this.filter, this.coords, true);
    }

    getEvents(eventsToLoad, filter, coords, reload?, count?) {
      this.eventsProvider.eventsApi(eventsToLoad, filter, this.searchText, coords, reload, count).subscribe(
        s => {
          console.log(s, this.events);
          this.events = s;
        },
        err => {
          let alert = this.alertCtrl.create({
              title: 'Error',
              message: 'Unable to retrieve data.  Check your internet connection.',
              buttons: ['OK']
          });
          alert.present();
        },
        () => {
          this.totalEvents = this.eventsProvider.getEventsCount();
          this.eventsLoadedCount = this.events.length;
        }
      )
    }

    goToEvent(event) {
      console.log('go to event called');
      this.navCtrl.push(EventPage, {event: event});
    }

    showFilter(ev) {
      let popover = this.popoverCtrl.create(EventsFilterPage, { ev: ev, filter: this.filter});
      popover.onDidDismiss(f => {
        if(f) {
          console.log(f, this.filter)
          this.filter = f;

          this.eventsToLoad = 0;
          this.getEvents(this.eventsToLoad, this.filter, this.coords, true);
        }
      });
      popover.present({
        ev: event
      });
    }

    doInfinite(infiniteScroll) {
      console.log('Begin async operation');

      setTimeout(() => {
        this.eventsToLoad += 25;
        if(this.eventsToLoad < this.totalEvents) {
          this.getEvents(this.eventsToLoad, this.filter, this.coords, true);
        }
        console.log('Async operation has ended');
        infiniteScroll.complete();
      }, 1500);
    }

    ionViewWillUnload() {
      this.eventsProvider.setSortBy('date');
      this.eventsProvider.setWithin(-1)
      this.eventsProvider.setNearBus(false)
      this.eventsProvider.setNearBike(false)
      this.eventsProvider.setNearMetro(false)
      this.eventsProvider.setParking(false)
      this.eventsProvider.setLocationname('All')
    }
}