import { Component } from '@angular/core';
import { LoadingController, NavParams, AlertController } from 'ionic-angular';
import { FinesProvider, Fine} from './finesProvider';

import { AnalyticsProvider } from '../../../providers/providers';
import { ITracking } from '../../../app/app.component';
import { AppConfig } from '../../../config';

@Component({
  selector: 'page-fines',
  templateUrl: 'fines.html'
})
export class FinesPage implements ITracking {
    fineSubscription;
    fines: Fine[];
    total: number = 0;
    loaded:boolean = false;
    title:string;

    constructor(private navParams: NavParams
            , private finesProvider: FinesProvider
            , private loadingCtrl:LoadingController
            , private analytics:AnalyticsProvider
            , private alertCtrl:AlertController) {
                this.title = AppConfig.APP_TITLE_HTML;
    }

    track() {
      this.analytics.trackView('FinesPage');
    }

    ionViewWillEnter() {
        this.loadPage();
    }

    ionViewDidUnload() {
        if(this.fineSubscription) this.fineSubscription.dispose();
    }

    loadPage() {
        let loader = this.loadingCtrl.create();
        loader.present();
        let accountNumber = this.navParams.get('account')
        this.fineSubscription = this.finesProvider.getFines(accountNumber).subscribe(
            s => {
                this.fines = s;

                this.fines.forEach(f => {
                    this.total += f.amount;
                });
                this.loaded = true;
                loader.dismissAll();
            }, error => {
                let alert = this.alertCtrl.create({
                    title: 'Error',
                    message: 'Unable to retrieve data.  Check your internet connection.',
                    buttons: ['OK']
                });
                alert.present();
                loader.dismissAll();
            });
    }

    doRefresh(refresher) {
        this.total = 0;
        this.loaded = false;

        setTimeout(() => {
            this.fines = [];
            this.loadPage();

            refresher.complete();
        }, 200);
    }

    payFines() {
        window.open(`${AppConfig.LIBSYS_BASE_URL}/iii/ecom/pay.do?lang=eng&scope=8&ptype=0&tty=800`, '_system');
    }

}
