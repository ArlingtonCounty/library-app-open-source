import { Injectable } from '@angular/core';
import { PikaProvider, LocalAccount } from '../../../providers/providers';

export class Fine {
    amount: number;
    details: Object[];
    message: string;
    reason: string;

    static fromJson(obj: any): Fine {
        var f = new Fine();

        f.amount = Number(obj.amount.substring(1));
        f.details = obj.details;
        f.message = obj.message;
        f.reason = obj.reason;

        return f;
    }
}

@Injectable()
export class FinesProvider {
    private fines = [];

    constructor(private pikaProvider: PikaProvider) { }


    getFines(account: LocalAccount) {
        return this.pikaProvider.userApi("getPatronFines", account.username, account.password)
            .map(res => {
                let body = res.json();
                console.log(body)
                if (body.result.success) {
                    var holdTempArray = [];

                    for (var i = 0; i < body.result.fines.length; i++) {
                        var fines = Fine.fromJson(body.result.fines[i]);
                        holdTempArray.push(fines);
                    }

                    this.fines = holdTempArray;
                }
                else {
                    this.fines = []
                }
                return this.fines;
            });
    }
}
