import { Component, NgZone } from '@angular/core';
import { ViewController, AlertController, LoadingController } from 'ionic-angular';
import { AccountService } from '../../../providers/providers';

import { AnalyticsProvider } from '../../../providers/providers';
import { ITracking } from '../../../app/app.component';

import { AccountActions } from '../account.actions';
import { AppConfig } from '../../../config';

declare var cordova;
declare var window;

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage implements ITracking {
  inputs = {
    username: "",
    password: "",
    nickname: "",
  }
  barcodeUrl:string;
  showHelp:boolean;
  title:string;

  constructor(private viewCtrl: ViewController
            , private accountService: AccountService
            , private actions:AccountActions
            , private zone:NgZone
            , private loadingCtrl:LoadingController
            , private alertCtrl:AlertController
            , private analytics:AnalyticsProvider) { 
              this.title = AppConfig.APP_TITLE_HTML;
            }

  track() {
      this.analytics.trackView('LoginPage');
  }

  dismiss(accountAdded) : void {
    console.log("dismiss called");
    this.viewCtrl.dismiss(accountAdded);
  }

  toggleHelp() {
      this.showHelp = this.showHelp ? false : true;
  }

  signIn() {
      var self = this;
      let loader = this.loadingCtrl.create();
      loader.present();
      console.log('inputs', self.inputs);

      this.accountService
          .logIn(self.inputs.username, self.inputs.password, self.inputs.nickname)
          .subscribe(
            login => {
              loader.dismissAll();
              if(login) {
                this.actions.setSelectedUsername(self.inputs.username);
                self.dismiss(true);
              }
            },
            error => { console.log(error); loader.dismissAll(); },
            () => { console.log('done'); loader.dismissAll(); } );
  }

  requestPinReset() {
    window.open(`${AppConfig.PIKA_BASE_URL}MyAccount/RequestPinReset`, '_system');
  }

  scan() {
    var self = this;
    cordova.plugins.barcodeScanner.scan(
      function (result) {
        if(!result.cancelled){
            console.log("Barcode", result);
          //you have to use zone so angular can detect the change
          self.zone.run(() => self.inputs.username = result.text );
        }
      },
      function (error) {
          //todo: handle this error better
          console.log(error);
          let alert = this.alertCtrl.create({
            title: 'Scanning Failure',
            subTitle: error,
            buttons: ['OK']
          });
          alert.present();
      },
      {
          "preferFrontCamera" : false, // iOS and Android
          "showFlipCameraButton" : true, // iOS and Android
          "prompt" : "Place a barcode inside the scan area", // supported on Android only
          "resultDisplayDuration": 0, //Android prevent result display
          "disableAnimations" : true
      }
   );
  }

}
