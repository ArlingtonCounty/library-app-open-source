import { NgRedux } from '@angular-redux/store';
import { AccountService, LocalAccount } from '../../providers/providers';
import { CheckoutsProvider } from './checkouts/checkoutsProvider';
import { IAppState } from '../../store/IAppState';
import { Injectable } from '@angular/core';

export const REMOVE_ACCOUNT = 'account/REMOVE';
export const UPDATE_ACCOUNT = 'account/UPDATE';
export const ADD_ACCOUNT = 'account/ADD';
export const GET_ACCOUNTS = 'account/GET_ALL';
export const SET_SELECTED_USERNAME = 'account/SET_USERNAME';
export const SET_ACCOUNT_LOADING = 'account/SET_LOADING';

export const GET_PROFILE = 'profile/GET';

export const GET_CHECKOUTS = 'checkouts/GET';

export const GET_LISTS = 'lists/GET';

@Injectable()
export class AccountActions {
    constructor(
        private ngRedux:NgRedux<IAppState>,
        private accountService:AccountService,
        private checkoutsProvider:CheckoutsProvider
    ) { }

    getAllProfiles() {
        this.accountService.getSavedAccounts().subscribe(accounts => {
            accounts.forEach(account => {
                this.accountService.getProfile(account, true).subscribe(s => {
                    //Do nothing
                })
            })
        });
    }

    getAccounts() {
      if(!this.ngRedux.getState().accountsLoaded) {
        this.accountService.getSavedAccounts().subscribe(accounts => {
            this.ngRedux.dispatch({
                type: GET_ACCOUNTS,
                accounts: accounts
            });
        });
      }
    }

    removeAccount(account) {
        this.accountService.logOut(account).subscribe(result => {
            this.ngRedux.dispatch({
                type: REMOVE_ACCOUNT,
                account: account
            });
        });
    }

    setSelectedUsername(username:string) {
        this.ngRedux.dispatch({
            type: SET_SELECTED_USERNAME,
            username: username
        });
    }

    setAccountLoading(){
        this.ngRedux.dispatch({
            type: SET_ACCOUNT_LOADING
        })
    }

    getProfile(account:LocalAccount){
        this.accountService.getProfile(account, true).subscribe(profile => {
            this.ngRedux.dispatch({
                type: GET_PROFILE,
                profile: profile
            });
        });
    }

    getCheckouts(account:LocalAccount){
        this.checkoutsProvider.getCheckouts(account).subscribe(checkouts => {
            this.ngRedux.dispatch({
                type: GET_CHECKOUTS,
                checkouts: checkouts,
            });
            this.getAccounts();
        });
    }

    getUserLists(account:LocalAccount) {
        this.accountService.getUserLists(account).subscribe(lists => {
            this.ngRedux.dispatch({
                type: GET_LISTS,
                lists: lists
            })
        })
    }

    updateAccount(account:LocalAccount) {
        this.accountService.updateLocalAccount(account).subscribe(s=> {
            //just refresh everything?
            this.getAccounts();
        });
    }
}
