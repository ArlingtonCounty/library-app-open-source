import { Component } from '@angular/core';
import { NavController, ModalController, LoadingController, NavParams } from 'ionic-angular';
import { Profile, LocalAccount } from '../../providers/providers';
import { LoginPage, HoldsPage, AccountsManagementPage, CheckoutsPage, FinesPage, UpdatePage, BarcodePage, ReorderPage, AccountListPage } from './index'

import { AnalyticsProvider } from '../../providers/providers';
import { ITracking } from '../../app/app.component';
import { Observable } from 'rxjs/Observable';
import { select } from '@angular-redux/store';
import { AccountActions } from './account.actions';
import { AppConfig } from '../../config';

@Component({
  selector: 'page-account',
  templateUrl: 'account.html'
})
export class AccountPage implements ITracking {
  @select('localAccounts')
  readonly localAccounts$ : Observable<LocalAccount[]>;

  @select('selectedAccountUsername')
  readonly selectedAccount$ : Observable<string>;

  @select("profile")
  readonly profile$ : Observable<Profile>;

  @select("checkouts")
  readonly checkouts$ : Observable<any[]>;

  @select('accountLoading')
  readonly accountLoading$ : Observable<boolean>;

  @select('lists')
  readonly lists$: Observable<any[]>;

  public profile:Profile;
  public savedAccounts: LocalAccount[] = [];
  public selectedAccount: LocalAccount;
  public holds = [];
  public checkouts = [];
  public checkoutsOverdue: number = 0;
  public lists = [];
  public loader:any;

  private subscriptions:any[] = [];
  title:string;

  constructor(private navCtrl: NavController
            , private actions: AccountActions
            , private navParams: NavParams
            , private loadingCtrl:LoadingController
            , private modalCtrl:ModalController
            , private analytics:AnalyticsProvider) {

    this.profile = new Profile();
    this.title = AppConfig.APP_TITLE_HTML;

    this.actions.getAccounts();

    var account:LocalAccount = this.navParams.get('account');
    var accountId = this.navParams.get('accountId');

    if(!this.selectedAccount) {
      if(account) {
        this.actions.setSelectedUsername(account.username);
      }
      else if(accountId) {
        this.actions.setSelectedUsername(accountId);
      }
    }
  }

  track() {
      var page = 'AccountPage';
      this.analytics.trackView(page);
      return page;
  }

  onChange(account: LocalAccount) {
    this.actions.setSelectedUsername(account.username);
  }

  redirectToLogin() {
    var self = this;
    //show login page
    let loginModal = self.modalCtrl.create(LoginPage);
    loginModal.onDidDismiss(data => {
      if(!data && this.savedAccounts.length == 0)
        this.navCtrl.popToRoot();
    });
    loginModal.present();
  }

  ionViewWillEnter() {
    this.subscriptions.push(this.localAccounts$.subscribe(s => {
      this.savedAccounts = s;

      if(this.savedAccounts.length == 0) {
        setTimeout(() => {
            this.redirectToLogin()
        }, 100);
      }
    }));

    this.subscriptions.push(this.selectedAccount$.subscribe(username => {
      this.selectedAccount = this.savedAccounts.find(account => account.username === username);

      if(!this.selectedAccount && this.savedAccounts.length > 0) {
        this.actions.setSelectedUsername(this.savedAccounts[0].username);
      }

    }));

    this.subscriptions.push(this.accountLoading$.subscribe(accountLoading => {
        if(accountLoading && !this.loader) {
          this.loader = this.loadingCtrl.create();
          this.loader.present();
        }

        if(this.loader && !accountLoading) {
          this.loader.dismissAll();
          this.loader = null;
        }
      }));

      this.subscriptions.push(this.checkouts$.subscribe(checkouts => {
        this.checkouts = checkouts;
        this.checkoutsOverdue = checkouts ? checkouts.filter(c=> c.overdue).length : 0;
      }));

      this.subscriptions.push(this.profile$.subscribe(profile => {
        this.profile = profile;

      }));

      this.subscriptions.push(this.lists$.subscribe(lists => {
        this.lists = lists;
      }));

      this.selectedAccount$.subscribe(selectedUserName => {
        if(this.savedAccounts) {
          let account = this.savedAccounts.find(a => a.username === selectedUserName);
          if(account){
            this.actions.getProfile(account);
            this.actions.getCheckouts(account);
            this.actions.getUserLists(account);
          }
        }
      });
  }

  ionViewWillLeave() {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
    this.subscriptions = [];
  }

  goToHolds() {
    this.navCtrl.push(HoldsPage, {account: this.selectedAccount});
  }

  goToCheckouts() {
    this.navCtrl.push(CheckoutsPage, {account: this.selectedAccount});
  }

  goToFines() {
    this.navCtrl.push(FinesPage, {account: this.selectedAccount});
  }

  goToLists() {
    this.navCtrl.push(AccountListPage, {account: this.selectedAccount});
  }

  goToUpdate() {
    this.navCtrl.push(UpdatePage, {currentAccount: this.selectedAccount });
  }

  showBarcode() {
    const barcodeModal = this.modalCtrl.create(BarcodePage, { account: this.selectedAccount, profile: this.profile });
      barcodeModal.onDidDismiss(dismiss => {
        console.log("barcode dismissed");
      });
      barcodeModal.present();
  }

  goToAccount() {
      this.navCtrl.push(AccountsManagementPage, {account: this.selectedAccount});
  }

  goToReorder() {
    this.navCtrl.push(ReorderPage);
  }

  doRefresh(refresher) {
    setTimeout(() => {

      this.actions.setAccountLoading();
      this.actions.getCheckouts(this.selectedAccount);
      this.actions.getProfile(this.selectedAccount);

      refresher.complete();
    }, 200);
  }

}
