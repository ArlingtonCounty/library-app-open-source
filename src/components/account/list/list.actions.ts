import { NgRedux } from '@angular-redux/store';
import { IAppState } from '../../../store/IAppState';
import { Injectable } from '@angular/core';
import { AccountService } from '../../../providers/providers';

export const GET_LIST_TITLES = 'titles\GET';

@Injectable()
export class ListActions {
    constructor(
        private ngRedux:NgRedux<IAppState>, 
        private accountService:AccountService){

    }

    getListTitles(list) {
        this.ngRedux.dispatch({
            type: GET_LIST_TITLES,
            titles: []
        });

        this.accountService.getUserListTitles(list).subscribe(titles => {
            this.ngRedux.dispatch({
                type: GET_LIST_TITLES,
                titles: titles
            });
        });
    }
}
