import { Component } from '@angular/core';
import { NavParams, NavController } from 'ionic-angular';
import { ListTitlesPage } from './listtitles';

import { Observable } from 'rxjs/Observable';
import { select } from '@angular-redux/store';
import { AnalyticsProvider } from '../../../providers/providers';
import { ITracking } from '../../../app/app.component';

@Component({
  selector: 'page-accountlist',
  templateUrl: 'accountlist.html'
})
export class AccountListPage implements ITracking {
    
  @select('lists')
  readonly lists$ : Observable<any[]>;

    constructor(private navParams: NavParams
            , private navCtrl: NavController
            , private analytics:AnalyticsProvider) {
    }

    track() {
      this.analytics.trackView('ListPage');
    }

    ionViewWillEnter() {

    }

    goToListTitles(list) {
        this.navCtrl.push(ListTitlesPage, {list: list});
    }
}
