import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { ListActions } from './list.actions';

import { Observable } from 'rxjs/Observable';
import { select } from '@angular-redux/store';
import { AnalyticsProvider } from '../../../providers/providers';
import { ITracking } from '../../../app/app.component';
import { AppConfig } from '../../../config';

@Component({
  selector: 'page-listtitles',
  templateUrl: 'listtitles.html'
})
export class ListTitlesPage implements ITracking {
    
  @select('selectedListTitles')
  readonly titles$ : Observable<any[]>;

  private list:any;
  title:string;

    constructor(private navParams: NavParams
            , private analytics:AnalyticsProvider
            , private actions:ListActions) {
              this.title = AppConfig.APP_TITLE_HTML;
    }

    track() {
      this.analytics.trackView('ListTitlePage');
    }

    ionViewWillEnter() { 
        this.list = this.navParams.get('list');
        this.actions.getListTitles(this.list);
    }
}
