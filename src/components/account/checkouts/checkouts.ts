import { Component, Injector } from '@angular/core';
import { NavParams, AlertController, ActionSheetController } from 'ionic-angular';
import { CheckoutsProvider, CheckedOutItem } from './checkoutsProvider';
import { Observable } from 'rxjs/Observable';
import { select } from '@angular-redux/store';
import _ from 'lodash';

import { BaseLibraryItem } from '../shared/baseLibraryItem';
import { AnalyticsProvider } from '../../../providers/providers';
import { ITracking } from '../../../app/app.component';
import { AccountActions } from '../account.actions';
import { AppConfig } from '../../../config';

@Component({
  selector: 'page-checkouts',
  templateUrl: 'checkouts.html'
})
export class CheckoutsPage extends BaseLibraryItem implements ITracking {
    CheckedOutItems:CheckedOutItem[];
    checkoutSubscription;
    sortBy:string = 'daysUntilDue';
    displayThumbnails:boolean = true;
    account: any;
    selectMode:boolean = false;
    selectedCheckouts:CheckedOutItem[] = [];
    title:string;
    pikaUrl:string;

    @select("checkouts")
    readonly checkouts$ : Observable<any[]>;

    constructor(private navParams: NavParams
            , private checkoutsProvider: CheckoutsProvider
            , public injector: Injector
            , public actionSheetCtrl: ActionSheetController
            , public alertCtrl: AlertController
            , private analytics:AnalyticsProvider
            , private actions:AccountActions) {

        super(injector)
        this.title = AppConfig.APP_TITLE_HTML;
        this.pikaUrl = AppConfig.PIKA_BASE_URL;
        this.CheckedOutItems = [];
    }

    track() {
      this.analytics.trackView('CheckoutsPage');
    }

    ionViewWillEnter() {
        this.account = this.navParams.get('account');
        
        this.checkoutSubscription = this.checkouts$.subscribe(
            s => {
                this.CheckedOutItems = s;
                
                this.sort(this.sortBy);
            }, error => {
                let alert = this.alertCtrl.create({
                    title: 'Error',
                    message: 'Unable to retrieve data.  Check your internet connection.',
                    buttons: ['OK']
                });
                alert.present();
            });
    }

    ionViewDidLeave() {
        if(this.checkoutSubscription) this.checkoutSubscription.dispose();
    }

    sort(event) {
        this.CheckedOutItems = _.sortBy(this.CheckedOutItems, [event])
    }

    presentActionSheet(checkout:CheckedOutItem) {
        let actionSheet = this.actionSheetCtrl.create({
            buttons: [
                {
                    text: 'View Title in Catalog',
                    icon: 'book',
                    handler: () => {
                        this.goToBook(checkout)
                        console.log('View title clicked');
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });

        if (!checkout.overDriveId) {
            actionSheet.addButton(
                {
                    text: 'Renew Title',
                    icon: 'refresh',
                    handler: () => {
                        debugger;
                        this.renewItem(checkout);
                        console.log('Renew clicked');
                    }
                }
                
            )
        }
        if (checkout.overDriveId) {
            actionSheet.addButton(
                {
                    text: 'Read Online',
                    icon: 'paper',
                    handler: () => {
                        this.readOnline(checkout);
                    }
                }
            )
            var format = checkout.formats[0];
            if (format && format.downloadUrl) {
                actionSheet.addButton(
                    {
                        text: 'Download Overdrive',
                        icon: 'download',
                        handler: () => {
                            this.downloadOverDrive(checkout);
                            console.log('download clicked');
                        }
                    }
                )
            }
            if (checkout.formats.length > 0 && !format.downloadUrl){
                actionSheet.addButton(
                    {
                        text: 'Select Format',
                        icon: 'checkbox',
                        handler: () => {
                            this.selectOverDriveDownloadFormat(checkout);
                            console.log('download clicked');
                        }
                    }
                )
            }
            actionSheet.addButton(
                {
                    text: 'Return Overdrive',
                    icon: 'return-left',
                    handler: () => {
                        this.returnOverDriveItem(checkout);
                    }
                }
            )
        }
        actionSheet.present();
    }

    returnOverDriveItem(checkout: CheckedOutItem) {
        this.confirmAction(checkout, 
                            'Return', 
                            this.checkoutsProvider.returnOverDriveItem(this.account, checkout), 
                            'Cancel', 
                            'Return', 
                            `Unable to return ${checkout.title}`)
            .subscribe(s=> {
                if(s.success){
                    this.presentToast(`Updated ${checkout.title}.`);
                    this.resetPage();
                }
            });
    }

    downloadOverDrive(checkout: CheckedOutItem) {
        this.confirmAction(checkout, 
                            'Download', 
                            this.checkoutsProvider.getDownloadLink(this.account, checkout, checkout.formats[0].name), 
                            'Cancel', 
                            'Download', 
                            `Unable to download ${checkout.title}`)
            .subscribe(s=> {
                if(s.success && s.downloadUrl) {
                    var downloadUrl = _.replace(s.downloadUrl, '\\', "");
                    window.location.href= downloadUrl;
                }
            });
    }

    readOnline(checkout: CheckedOutItem){
        this.confirmAction(checkout, 
                            'Read online', 
                            this.catalogProvider.readOnline(this.account, checkout.overDriveId), 
                            'Cancel', 
                            'Read Online', 
                            `Unable to read ${checkout.title}`)
            .subscribe(s => {
              console.log('Read Online complete', s);
            });
    }

    selectOverDriveDownloadFormat(checkout: CheckedOutItem) {
        let alert = this.alertCtrl.create();
        alert.setTitle(`Select format to download ${checkout.title}?`);
        alert.setMessage('You cannot change format afterwards')

        checkout.formats.forEach(format => {
            alert.addInput({
                type: 'radio',
                label: format.name,
                value: format.id,
                checked: false
            });
        })

        alert.addButton('Cancel');
        alert.addButton({
            text: 'OK',
            handler: format => {
                this.performAction(this.checkoutsProvider.selectOverDriveDownloadFormat(this.account, checkout, format), 'Failed to select Overdrive format')
                    .subscribe(s=> {
                        if(s.success){
                            this.presentToast(`Updated ${checkout.title}.`);
                            this.resetPage();
                        }
                    });
            }
        });
        alert.present();
    }

    renewItem(checkout) {
        this.confirmAction(checkout, 
                            'Renew', 
                            this.checkoutsProvider.renewItem(this.account,checkout), 
                            'Cancel', 
                            'Renew', 
                            `Unable to renew ${checkout.title}`)
            .subscribe(s=> {
                if(s.success) {
                    this.presentToast(`Updated ${checkout.title}.`);
                    this.resetPage();
                }
            });
    }

    renewAll() {
        this.confirmAction({ title: "titles", total: this.CheckedOutItems.length },
                            'Renew All',
                            this.checkoutsProvider.renewAll(this.account),
                            'Cancel',
                            'Renew All',
                            'Failed to renew all')
            .subscribe(s => {
                if (s.success) {
                    this.actionToast(`Renewed ${s.renewed} of ${this.CheckedOutItems.length}`);
                    this.resetPage();
                }
                else if(s.renewed > 0) {
                    this.resetPage();
                }
            },
            err => {
                console.log(err)
                this.resetPage();
            });
    }

    enableSelectMode() {
        this.selectMode = true;
    }

    renewSelected() {
        let selected = [];
        const keys = Object.keys(this.selectedCheckouts);
        keys.forEach(key => {
            if(this.selectedCheckouts[key] === true)
                selected.push(this.CheckedOutItems.find(c => c.id === key));
        });
        let counter = selected.length;
        
        if(counter > 0) {
            let options = { content: `Processing ${counter} renewal requests. This may take up to one minute`};
                               
            let loader = this.loadingCtrl.create(options);
            loader.present();

            this.checkoutsProvider.renewSelected(this.account, selected).subscribe(s=> {
                this.selectMode = false;
                this.actions.getCheckouts(this.account);

                this.actionToast(`Renewed ${s.renewed} of ${selected.length}`);
                
                loader.dismissAll();
            });
        }
        else {
            this.selectMode = false;
        }
    }

    cancelSelect() {
        this.selectMode = false;
    }
}