import { Injectable } from '@angular/core';
import { PikaProvider, AccountService, LocalAccount, PikaTempProvider } from '../../../providers/providers';
import { AppConfig } from '../../../config';

export class CheckedOutItem {
    author: string;
    barcode: string;
    canrenew: boolean;
    checkoutSource: string;
    coverUrl: string;
    daysUntilDue: number;
    dueDate: Date;
    fine: string;
    format: string;
    fullId: string;
    groupedWorkId: string;
    id: string;
    itemid: string;
    itemindex: string;
    link: string;
    overdue: boolean;
    ratingData: {};
    recordId: string;
    renewCount: number;
    renewIndicator: string;
    renewMessage: string;
    request: string;
    shortId: string;
    title: string;
    user: string;
    userId: string;
    overDriveId: string;
    formats: any;
    recordUrl: string;

    static fromJson(obj: any): CheckedOutItem {
        var p = new CheckedOutItem();

        p.author = obj.author
        p.barcode = obj.barcode || null;
        p.canrenew = obj.canrenew
        p.checkoutSource = obj.checkoutSource
        p.coverUrl = obj.coverUrl
        p.daysUntilDue = obj.daysUntilDue
        p.dueDate = new Date(obj.dueDate * 1000);
        p.fine = obj.fine
        p.format = obj.format
        p.fullId = obj.fullId
        p.groupedWorkId = obj.groupedWorkId
        p.id = obj.id
        p.itemid = obj.itemid
        p.itemindex = obj.itemindex
        p.link = obj.link
        p.overdue = obj.overdue
        p.ratingData = obj.ratingData;
        p.recordId = obj.recordId
        p.renewCount = obj.renewCount
        p.renewIndicator = obj.renewIndicator
        p.renewMessage = obj.renewMessage
        p.request = obj.request
        p.shortId = obj.shortId
        p.title = obj.title
        p.user = obj.user
        p.userId = obj.userId
        p.overDriveId = obj.overDriveId || null;
        p.formats = obj.formats || [];
        
        var pikaUrl = AppConfig.PIKA_BASE_URL.substring(0, AppConfig.PIKA_BASE_URL.length-1);
        p.recordUrl = pikaUrl + obj.linkUrl;

        return p;
    }
}

@Injectable()
export class CheckoutsProvider {
    private checkouts = [];

    constructor(private pikaProvider: PikaProvider
        , private pikaTempProvider: PikaTempProvider
        , private accountService: AccountService) { }

    getCheckouts(account: LocalAccount) {
        return this.pikaProvider.userApi("getPatronCheckedOutItems", account.username, account.password)
            .map(res => {
                this.checkouts = [];
                let body = res.json();
                if (body.result.success) {
                    var holdTempArray = [];
                    var anyOverdue = false;
                    var checkoutsWarning = false;
                    var datePlusThree = new Date();
                    datePlusThree.setDate(datePlusThree.getDate() + 3);

                    if(!body.result.checkedOutItems) return [];

                    if (body.result.checkedOutItems.length) {
                        for (var i = 0; i < body.result.checkedOutItems.length; i++) {
                            var checkedOutItem = CheckedOutItem.fromJson(body.result.checkedOutItems[i]);
                            holdTempArray.push(checkedOutItem);
                            if (checkedOutItem.overdue) {
                                anyOverdue = true;
                            }
                            else if (datePlusThree >= checkedOutItem.dueDate) {
                                checkoutsWarning = true;
                            }
                        }
                    }
                    else {
                        var keys = Object.keys(body.result.checkedOutItems);
                        keys.forEach(key => {
                            var checkedOutItem = CheckedOutItem.fromJson(body.result.checkedOutItems[key]);
                            holdTempArray.push(checkedOutItem);
                            if (checkedOutItem.overdue) {
                                anyOverdue = true;
                            }
                            else if (datePlusThree >= checkedOutItem.dueDate) {
                                checkoutsWarning = true;
                            }
                        });
                    }
                    this.accountService.updateLocalAccountItem(account, ["checkoutsAlert","checkoutsWarning"], [anyOverdue, checkoutsWarning]);

                    this.checkouts = holdTempArray;
                }
                else {
                    this.checkouts = [];
                }
                return this.checkouts;
            });

    }

    renewItem(account: LocalAccount, checkout: CheckedOutItem) {
        return this.pikaTempProvider.authenticateAction(account, this.pikaTempProvider.renewItem(account.patronId, checkout.recordId, checkout.renewIndicator));
    }

    renewAll(account: LocalAccount) {
        return this.pikaTempProvider.authenticateAction(account, this.pikaTempProvider.renewAll());
    }

    renewSelected(account: LocalAccount, checkouts: CheckedOutItem[]) {
        return this.pikaTempProvider.authenticateAction(account, this.pikaTempProvider.renewSelected(account.patronId, checkouts))
    }

    getDownloadLink(account: LocalAccount, checkout: CheckedOutItem, formatId: string) {
        return this.pikaTempProvider.authenticateAction(account, this.pikaTempProvider.getDownloadLink(account.patronId, checkout.overDriveId, formatId))
    }

    returnOverDriveItem(account: LocalAccount, checkout: CheckedOutItem) {
        return this.pikaTempProvider.authenticateAction(account, this.pikaTempProvider.returnOverDriveItem(account.patronId, checkout.overDriveId));
    }

    selectOverDriveDownloadFormat(account: LocalAccount, checkout: CheckedOutItem, formatId: string) {
        return this.pikaTempProvider.authenticateAction(account, this.pikaTempProvider.selectOverDriveDownloadFormat(account.patronId, checkout.overDriveId, formatId));
    }
}