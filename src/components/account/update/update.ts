import { Component } from '@angular/core';
import { ViewController, NavParams, AlertController } from 'ionic-angular';

import { AccountService, LocalAccount } from '../../../providers/providers';
import { AnalyticsProvider } from '../../../providers/analyticsProvider';
import { ITracking } from '../../../app/app.component';
import { AppConfig } from '../../../config';

@Component({
  selector: 'page-update',
  templateUrl: 'update.html'
})
export class UpdatePage implements ITracking {
  inputs = {
    username: "",
    password: "",
  }
  public currentAccount: LocalAccount;
  title:string;

  constructor(private viewCtrl: ViewController
    , private navParams: NavParams
    , public alertCtrl: AlertController
    , private accountService: AccountService
    , private analytics: AnalyticsProvider) {
        
    this.title = AppConfig.APP_TITLE_HTML;
    this.currentAccount = this.navParams.get('currentAccount')
    if(this.currentAccount) 
        this.inputs.username = this.currentAccount.username;
  }

  track() {
      this.analytics.trackView('UpdatePage');
  }

  dismiss(update) : void {
    console.log("dismiss called", update);
    this.viewCtrl.dismiss(update);
  }

  update() {
      this.currentAccount.username = this.inputs.username;
      this.currentAccount.password = this.inputs.password;
      this.accountService.updateLocalAccount(this.currentAccount);
      this.dismiss(true);
  }

  removeAccount() {
      let confirm = this.alertCtrl.create({
            title: 'Remove this account?',
            message: 'This account will be removed from your saved accounts.',
            buttons: [
                {
                    text: 'Cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Remove',
                    handler: () => {
                        console.log('Remove clicked');
                        this.accountService.logOut(this.currentAccount);
                        this.accountService.showToast("Account Removed");
                        this.dismiss(false);
                    }
                }
            ]
        });
        confirm.present();
  }

  requestPinReset() {
    window.open(`${AppConfig.PIKA_BASE_URL}MyAccount/RequestPinReset`, '_system');
  }

}
