import { Component, ViewChild } from '@angular/core';
import { AlertController, NavController, ModalController, Content } from 'ionic-angular';
import { AccountService, LocalAccount, Profile } from '../../../providers/providers';
import { LoginPage, AccountsManagementPage } from '../index'
import { AnalyticsProvider } from '../../../providers/providers';
import { ITracking } from '../../../app/app.component';
import { AccountActions } from '../account.actions';
import { NgRedux, select } from '@angular-redux/store';
import { Observable } from 'rxjs/Observable';
import { Keyboard } from '@ionic-native/keyboard';
import { AppConfig } from '../../../config';

@Component({
    selector: 'page-reorder',
    templateUrl: 'reorder.html'
})
export class ReorderPage implements ITracking {
    @ViewChild(Content) content: Content;
    message:string;
    editing:boolean = false;
    accounts:LocalAccount[];
    private subscriptions:any[] = [];

    @select('localAccounts')
    readonly localAccounts$ : Observable<LocalAccount[]>;
    title:string;

    constructor(private accountService: AccountService
        , private actions: AccountActions
        , public alertCtrl: AlertController
        , private navCtrl: NavController
        , private modalCtrl: ModalController
        , private analytics: AnalyticsProvider
        , private keyboard:Keyboard) {

            this.title = AppConfig.APP_TITLE_HTML;
    }

    ionViewWillEnter() {
      this.subscriptions.push(this.localAccounts$.subscribe(accounts => {
        this.accounts = accounts;
      }));
      //this.keyboard.disableScroll(true);
    }

    ionViewWillLeave() {
      console.log("Reorder Accounts Updated", this.accounts);
      this.accountService.reorder(this.accounts).subscribe().unsubscribe();
      this.subscriptions.forEach(sub => {
        sub.unsubscribe();
      });
      this.subscriptions = [];
      //this.keyboard.disableScroll(false);
    }

    track() {
        this.analytics.trackView('ReorderPage');
    }

    reorderItems(indexes) {
        this.localAccounts$.subscribe(accounts => {

            let element = accounts[indexes.from];
            accounts.splice(indexes.from, 1);
            accounts.splice(indexes.to, 0, element);

            this.accounts = accounts;
        }).unsubscribe();
    }

    removeAccount(account: LocalAccount) {
        let confirm = this.alertCtrl.create({
            title: 'Remove this account?',
            message: 'This account will be removed from your saved accounts.',
            buttons: [
                {
                    text: 'Cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Remove',
                    handler: () => {
                        console.log('Remove clicked');

                        this.accounts = this.accounts.filter(f=> f.username !== account.username);

                        this.actions.removeAccount(account);

                        this.accountService.showToast("Account Removed");
                    }
                }
            ]
        });
        confirm.present();
    }

    openModal() {
        let loginModal = this.modalCtrl.create(LoginPage);
        loginModal.onDidDismiss(accountAdded => {
            if (accountAdded) {
                this.accountService.getSavedAccounts().subscribe(accounts => {
                    this.accountService.showToast("Account Added");
                });
            }
        });
        loginModal.present();
    }

    goToAccount(account: LocalAccount) {
        var profile = new Profile();
        this.accountService.getProfile(account, true).subscribe(s => {
            profile = s;
            this.navCtrl.push(AccountsManagementPage, {profile: profile, account: account});
        },
            err => { console.log(err) }
        )
    }

    setNickname(account:LocalAccount, nickname) {
        console.log("Change nickname",  nickname);
        account.nickname = nickname.value;
        this.accountService.updateLocalAccount(account);

        this.message = "Updated!";
        setTimeout(() => { this.message = ""}, 0);
    }

}
