import { Component, Injector } from '@angular/core';
import { LoadingController, AlertController, NavController, ToastController } from 'ionic-angular';
import { CatalogProvider } from '../../catalog/catalogProvider'
import { FormatPage } from '../../components';
import { Observable } from 'rxjs/';
import _ from 'lodash';

@Component({
    template: ``
})
export class BaseLibraryItem {
    public loadingCtrl:LoadingController
    public catalogProvider:CatalogProvider
    public navCtrl:NavController
    public toastCtrl: ToastController
    public alertCtrl: AlertController

    constructor(public injector: Injector) {
            this.loadingCtrl = injector.get(LoadingController)
            this.catalogProvider = injector.get(CatalogProvider)
            this.navCtrl = injector.get(NavController)
            this.toastCtrl = injector.get(ToastController)
            this.alertCtrl = injector.get(AlertController)
    }

    resetPage() {
        //overwrite this method
    }

    doRefresh(refresher) {
        console.log('Begin async operation', refresher);

        setTimeout(() => {
            console.log('Async operation has ended');
            this.resetPage();
            refresher.complete();
        }, 200);
    }

    presentToast(message) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: 4000,
            position: 'bottom'
        });
        toast.present();
    }

    actionToast(message) {
        let actionToast = this.toastCtrl.create({
            message: message,
            showCloseButton: true,
            closeButtonText: 'ok',
            position: 'bottom'
        });
        actionToast.present();
    }

    goToBook(item: any) {
        if(item.overDriveId && !item.groupedWorkId) {
            window.open(item.recordUrl, '_system');
        }
        else {
            var self = this;
            this.catalogProvider.getBookById(item.id || item.groupedWorkId).subscribe(s => {
                if (s.results.length === 1) {
                    var title = s.results[0];

                    var bibId = item.id || 'overdrive:' + item.overDriveId.toLowerCase();
                    var record = _.find(title.records, { "bibId": bibId });
                    self.navCtrl.push(FormatPage, { book: title, record: record });
                }
                else {
                    let alert = this.alertCtrl.create({
                        title: 'Error',
                        subTitle: 'Unable to find title',
                        buttons: ['OK']
                    });
                    alert.present();
                }
            });
        }
    }

    confirmAction(item:any, verb:string, observable, cancelTextButton:string, yesTextButton:string, failMessage:string) : Observable<any> {
        return new Observable(observer => {
            let confirm = this.alertCtrl.create({
                title: `${verb} ${item.title}?`,
                buttons: [
                    {
                        text: cancelTextButton,
                        handler: () => {
                            console.log('Cancel clicked');
                        }
                    },
                    {
                        text: yesTextButton,
                        handler: () => {
                            this.performAction(observable, failMessage).subscribe(s=> {
                                console.log("From Confirm Action");
                                observer.next(s);
                            });
                        }
                    }
                ]
            });
            confirm.present();
        });
    }

    performAction(observable, failMessage:string) : Observable<any> {
        return new Observable(observer => {
            let loader = this.loadingCtrl.create();
            loader.present();

            observable.subscribe(s => {
                console.log("From Perform Action");
                observer.next(s);

                if(!s.success) {
                    if(s.message) this.actionToast(s.message);
                    else if(s.renewed > 0) this.actionToast('Not all were renewals successful - check due dates')
                    else this.actionToast(failMessage);
                }
                
                loader.dismissAll();
            },  
            err => {
                this.actionToast(failMessage);
                loader.dismissAll();
                observer.error(err);
            });

        });
    }

}