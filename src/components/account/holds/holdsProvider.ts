import { Injectable } from '@angular/core';
import { PikaProvider, PikaTempProvider, LocalAccount } from '../../../providers/providers';
import { AppConfig } from '../../../config';

export class Hold {
    available: boolean;
    author: string;
    coverUrl: string;
    currentPickupId: number;
    currentPickupName: string;
    id: string;
    isbn: string;
    title: string;
    format: string;
    status: string;
    locationSelect: string;
    freezeable: boolean;
    frozen: boolean;
    cancelable: boolean;
    automaticCancellation: Date;
    cancelId: string;
    overDriveId: string;
    holdQueuePosition: number;
    recordUrl: string;
    expire: Date;

    static fromJson(obj: any): Hold {
        var pikaUrl = AppConfig.PIKA_BASE_URL.substring(0, AppConfig.PIKA_BASE_URL.length-1);
        var h = new Hold();

        h.author = obj.author;

        let coverIndex = obj.coverUrl.indexOf(';')
        if (coverIndex > 1) {
            h.coverUrl = obj.coverUrl.substring(0, coverIndex) + "&size=medium&type=grouped_work";
        }
        else if (obj.coverUrl.indexOf('/') === 0) {
            h.coverUrl = pikaUrl + obj.coverUrl;
        }

        h.currentPickupId = obj.currentPickupId;
        h.currentPickupName = obj.currentPickupName;
        h.id = obj.id;
        h.isbn = obj.isbn;
        h.title = obj.title;
        h.format = obj.format[0];
        h.status = obj.status;
        h.locationSelect = obj.locationSelect;
        h.freezeable = obj.freezeable;
        h.frozen = obj.frozen;
        h.cancelable = obj.cancelable;
        h.cancelId = obj.cancelId;
        h.overDriveId = obj.overDriveId;
        h.automaticCancellation = new Date(obj.automaticCancellation * 1000);
        h.expire = new Date(obj.expire * 1000);
        h.holdQueuePosition = obj.holdQueuePosition;

        h.recordUrl = pikaUrl + obj.recordUrl;

        return h;
    }
}

@Injectable()
export class HoldsProvider {
    private holds = []

    constructor(private pikaProvider: PikaProvider
        , private pikaTempProvider: PikaTempProvider) { }

    getHolds(account: LocalAccount) {
        return this.pikaProvider.userApi("getPatronHolds", account.username, account.password)
            .map(res => {
                let body = res.json();
                if (body.result.success) {
                    var holdTempArray = [];
                    if (!Array.isArray(body.result.holds.available)) {
                        var objectKeys = Object.keys(body.result.holds.available);

                        for (var i = 0; i < objectKeys.length; i++) {
                            var key = objectKeys[i];
                            var hold = Hold.fromJson(body.result.holds.available[key]);
                            hold.available = true;
                            holdTempArray.push(hold);
                        }
                    }

                    if (!Array.isArray(body.result.holds.unavailable)) {
                        var objectKeys = Object.keys(body.result.holds.unavailable);

                        for (var i = 0; i < objectKeys.length; i++) {
                            var key = objectKeys[i];
                            var hold = Hold.fromJson(body.result.holds.unavailable[key]);
                            hold.available = false;
                            holdTempArray.push(hold);
                        }
                    }
                    this.holds = holdTempArray;
                }
                else {
                    this.holds = [];
                }
                return this.holds;
            });
    }

    cancelHold(account: LocalAccount, hold: Hold) {
        return this.pikaTempProvider.authenticateAction(account, this.pikaTempProvider.cancelHold(account.patronId, hold.id, hold.cancelId));
    }

    cancelOverDriveHold(account: LocalAccount, hold: Hold) {
        return this.pikaTempProvider.authenticateAction(account, this.pikaTempProvider.cancelOverDriveHold(account.patronId, hold.overDriveId));
    }

    thawHold(account: LocalAccount, hold: Hold) {
        return this.pikaTempProvider.authenticateAction(account, this.pikaTempProvider.thawHold(account.patronId, hold.id, hold.cancelId));
    }

    freezeHold(account: LocalAccount, hold: Hold) {
        return this.pikaTempProvider.authenticateAction(account, this.pikaTempProvider.freezeHold(account.patronId, hold.id, hold.cancelId));
    }

    changeHoldLocation(account: LocalAccount, hold: Hold, newLocation: string) {
        return this.pikaTempProvider.authenticateAction(account, this.pikaTempProvider.changeHoldLocation(account.patronId, newLocation, hold.cancelId));
    }
}