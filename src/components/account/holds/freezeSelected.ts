import { Component } from '@angular/core';

import { ViewController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { HoldsProvider, Hold } from './holdsProvider';
import { LocalAccount } from '../../../providers/providers';
import { AppConfig } from '../../../config';


@Component({
    templateUrl: 'freezeSelected.html'
})
export class FreezeSelected {
    selectedItem: Hold;
    account: LocalAccount;
    freezeUntil: Date;
    title:string;

    constructor(private viewCtrl: ViewController
        , public alertCtrl: AlertController
        , public params: NavParams
        , private loadingCtrl: LoadingController
        , private holdsProvider: HoldsProvider) {

        this.title = AppConfig.APP_TITLE_HTML;
        this.selectedItem = this.params.get('hold');
        this.account = this.params.get('account');

        console.log(this.selectedItem);
    }

    dismiss(data?): void {
        this.viewCtrl.dismiss(data);
    }

    getDates(add) {
        let today = new Date();
        let year = (today.getFullYear() + add).toString()
        let month = (today.getMonth() + 1).toString()
        let day = today.getDate().toString()

        if (month.length === 1) {
            month = '0' + month;
        }
        if (day.length === 1) {
            day = '0' + day;
        }

        return year + '-' + month + '-' + day;
    }

    freezeItems(selectedItem) {
        let confirm = this.alertCtrl.create({
            title: `Freeze ${selectedItem.title}?`,
            buttons: [
                {
                    text: 'Cancel',
                    handler: () => {
                        this.dismiss();
                    }
                },
                {
                    text: 'Freeze',
                    handler: () => {
                        let loader = this.loadingCtrl.create();
                        loader.present();

                        this.holdsProvider.freezeHold(this.account, selectedItem).subscribe(s => {
                            loader.dismissAll();
                            this.dismiss(s);
                        },
                            err => {
                                loader.dismissAll();
                                this.dismiss(err);
                            });
                    }
                }
            ]
        });
        confirm.present();
    }


}
