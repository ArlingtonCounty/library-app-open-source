import { Component, Injector } from '@angular/core';
import { NavController, NavParams, ModalController, ActionSheetController } from 'ionic-angular';
import { HoldsProvider, Hold } from './holdsProvider';
import { LocalAccount } from '../../../providers/providers';
import { FreezeSelected } from './freezeSelected';

import { Observable } from 'rxjs/Observable';
import { select } from '@angular-redux/store';

import { BaseLibraryItem } from '../shared/baseLibraryItem';
import { AnalyticsProvider } from '../../../providers/providers';
import { LibraryInfoProvider } from '../../../providers/libraryInfoProvider';
import { ITracking } from '../../../app/app.component';
import { AppConfig } from '../../../config';

import _ from 'lodash';

@Component({
    selector: 'page-holds',
    templateUrl: 'holds.html'
})
export class HoldsPage extends BaseLibraryItem  implements ITracking{
    holds: Hold[];
    holdsReady: Hold[];
    holdsNotReady: Hold[];
    holdSubscription;
    showReady: boolean = true;
    showNotReady: boolean = true;
    sortBy: string = 'title';
    displayThumbnails: boolean = true;
    account: LocalAccount;
    title:string;

    constructor(public navCtrl: NavController
        , private navParams: NavParams
        , private holdsProvider: HoldsProvider
        , public actionSheetCtrl: ActionSheetController
        , private libraryInfoProvider: LibraryInfoProvider
        , private modalCtrl: ModalController
        , public injector: Injector
        , private analytics: AnalyticsProvider) {
        super(injector)
        this.title = AppConfig.APP_TITLE_HTML;
        this.holds = [];
        this.holdsNotReady = [];
        this.holdsReady = [];
    }

    track() {
        this.analytics.trackView('HoldsPage');
    }

    ionViewWillEnter() {
        this.loadPage();
    }

    ionViewWillLeave() {
        this.holdsNotReady = [];
        this.holdsReady = [];
    }

    ionViewDidUnload() {
        if (this.holdSubscription) this.holdSubscription.dispose();
    }

    loadPage() {
        this.account = this.navParams.get('account');
        let loader = this.loadingCtrl.create();
        console.log("Loader from holds")

        //If a previous loader isn't fully closed when this presents, it breaks and never closes.
        setTimeout(() => loader.present(), 100);
        
        this.holdSubscription = this.holdsProvider.getHolds(this.account).subscribe(
            s => {
                this.holds = s;
                console.log(this.holds);
                this.holds.forEach(a => {
                    if (a.available) {
                        this.holdsReady.push(a);
                    }
                    else {
                        this.holdsNotReady.push(a);
                    }
                });

                this.sort(this.sortBy);
                loader.dismissAll();
            }, error => {
                loader.dismissAll();
                let alert = this.alertCtrl.create({
                    title: 'Error',
                    message: 'Unable to retrieve data.  Please check your internet connection.',
                    buttons: ['OK']
                });
                alert.present();
            });
    }

    resetPage() {
        this.holds = [];
        this.holdsNotReady = [];
        this.holdsReady = [];
        this.loadPage();
    }

    sort(event) {
        this.holdsNotReady = _.sortBy(this.holdsNotReady, [event])
        this.holdsReady = _.sortBy(this.holdsReady, [event])
    }

    holdsReadyAction(hold) {
        let actionSheet = this.actionSheetCtrl.create({
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        if (!hold.overDriveId) {
            actionSheet.addButton(
                {
                    text: 'View Title in Catalog',
                    icon: 'book',
                    handler: () => {
                        this.goToBook(hold)
                        console.log('View title clicked');
                    }
                })
        }
        if (hold.cancelable || hold.overDriveId) {
            actionSheet.addButton(
                {
                    text: 'Cancel Hold',
                    icon: 'close',
                    role: 'destructive',
                    handler: () => {
                        this.cancelHold(hold)
                        console.log('View title clicked');
                    }
                }
            )
        }
        actionSheet.present()
    }

    holdsNotReadyAction(hold) {
        let actionSheet = this.actionSheetCtrl.create({
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });

        if (!hold.overDriveId) {
            actionSheet.addButton(
                {
                    text: 'Change Pickup Location',
                    icon: 'swap',
                    handler: () => {
                        this.changeHoldPickup(hold)
                        console.log('View title clicked');
                    }
                })
            actionSheet.addButton(
                {
                    text: 'View Title in Catalog',
                    icon: 'book',
                    handler: () => {
                        this.goToBook(hold)
                        console.log('View title clicked');
                    }
                })
        }
        if (hold.frozen) {
            actionSheet.addButton(
                {
                    text: 'Thaw',
                    icon: 'water',
                    handler: () => {
                        this.thawHold(hold);
                        console.log('Renew clicked');
                    }
                })
        }
        else if (hold.freezeable && !hold.frozen) {
            actionSheet.addButton(
                {
                    text: 'Freeze',
                    icon: 'snow',
                    handler: () => {
                        actionSheet.dismiss().then(() => {
                            this.openFreezeModal(hold);
                        });
                        return false;
                    }
                }
            )
        }

        if (hold.cancelable || hold.overDriveId) {
            actionSheet.addButton(
                {
                    text: 'Cancel Hold',
                    icon: 'close',
                    role: 'destructive',
                    handler: () => {
                        this.cancelHold(hold)
                        console.log('View title clicked');
                    }
                }
            )
        }
        actionSheet.present()
    }

    cancelHold(hold) {
        if (!hold.overDriveId) {
            this.confirmAction(hold, 
                                'Cancel', 
                                this.holdsProvider.cancelHold(this.account, hold), 
                                'No', 
                                'Yes', 
                                `Unable to cancel ${hold.title}`)
                .subscribe(s => { 
                    this.resetPage(); 
                    this.presentToast(`Updated ${hold.title}.`);
                });
        }
        else {
            this.confirmAction(hold, 
                                'Cancel', 
                                this.holdsProvider.cancelOverDriveHold(this.account, hold), 
                                'No', 
                                'Yes', 
                                `Unable to cancel ${hold.title}`)
                .subscribe(s =>{ 
                this.resetPage(); 
                this.presentToast(`Updated ${hold.title}.`);
            });
        }
    }

    thawHold(hold) {
        this.confirmAction(hold, 
                            'Thaw', 
                            this.holdsProvider.thawHold(this.account, hold), 
                            'Cancel', 
                            'Thaw', 
                            `Unable to thaw ${hold.title}`)
            .subscribe(s => { 
                this.resetPage(); 
                this.presentToast(`Thawed ${hold.title}.`);
            });
    }

    changeHoldPickup(hold: Hold) {
        let alert = this.alertCtrl.create();
        alert.setTitle(`Change pickup location for ${hold.title}?`);

        var locations = this.libraryInfoProvider.returnLibraryLocationTags();
        locations.forEach(loc => {
            var checked = false;
            if(hold.currentPickupName == loc.name)
                checked = true;

            alert.addInput({
                type: 'radio',
                label: loc.name,
                value: loc.campusId,
                checked: checked
            });
        })

        alert.addButton('Cancel');
        alert.addButton({
            text: 'OK',
            handler: newLocation => {
                this.performAction(this.holdsProvider.changeHoldLocation(this.account, hold, newLocation), 'Unable to change pickup location')
                    .subscribe(s => { 
                        this.resetPage(); 
                        this.presentToast(`Updated ${hold.title}.`);
                    });
            }
        });
        alert.present();
    }

    openFreezeModal(hold) {
        let freezeModal = this.modalCtrl.create(FreezeSelected, { hold: hold, account: this.account });
        freezeModal.onDidDismiss(data => {
            console.log(data)
            if (data) {
                if (data.success) {
                    this.presentToast('Title Frozen')
                    this.resetPage();
                }
                else {
                    this.presentToast('Failed to freeze title')
                }
            }
        });
        freezeModal.present();
    }
}