import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { Profile, LocalAccount } from '../../../providers/accountService';
import { AnalyticsProvider, LibraryInfoProvider } from '../../../providers/providers';
import { ITracking } from '../../../app/app.component';
import { select } from '@angular-redux/store';
import { Observable } from 'rxjs/Observable';
import { AccountActions } from '../account.actions';
import { AppConfig } from '../../../config';

@Component({
    selector: 'page-accountsManagement',
    templateUrl: 'accountsManagement.html'
})
export class AccountsManagementPage implements ITracking {
    public account: LocalAccount;
    public libraryList:any[];
    public homeLibraryCode:string;
    title:string;

    @select("profile")
    readonly profile$ : Observable<Profile>;
    
    constructor(private navParams: NavParams
              , private analytics:AnalyticsProvider
              , private libaryInfoProvider:LibraryInfoProvider
              , private actions:AccountActions) { 
        
        this.title = AppConfig.APP_TITLE_HTML;
        this.account = this.navParams.get('account');
        this.libraryList = this.libaryInfoProvider.returnLibraryLocationTags();
    }

    track() {
      this.analytics.trackView('AccountsManagementPage');
    }

    ionViewWillEnter() {
        this.account = this.navParams.get('account');
        this.homeLibraryCode = this.account.homeLibraryCode;
    }

    getCurrentAccount() {
        return this.account;
    }
    
    changeLocation(libraryCode) {
        this.account.homeLibraryCode = libraryCode; 

        this.actions.updateAccount(this.account);
    }
}