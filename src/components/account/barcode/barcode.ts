import { Component } from '@angular/core';
import { NavParams, ViewController } from 'ionic-angular';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { AnalyticsProvider, LocalAccount } from '../../../providers/providers';
import { ITracking } from '../../../app/app.component';
import { AppConfig } from '../../../config';

declare var require: any
var JsBarcode = require('jsbarcode');

@Component({
  selector: 'page-barcode',
  templateUrl: 'barcode.html'
})
export class BarcodePage implements ITracking {
    account:LocalAccount = null;
    profile:any = null;
    currentOrientation: string = null;
    title:string;
    
    constructor(private navParams: NavParams,
                private analytics:AnalyticsProvider,
                private viewCtrl:ViewController,
                private screenOrientation:ScreenOrientation) { 

                    this.title = AppConfig.APP_TITLE_HTML;
                    this.account = this.navParams.get('account');
                    this.profile = this.navParams.get('profile');
                }

    track() {
        var page = 'BarcodePage';
        this.analytics.trackView(page);
        return page;
    }

    ionViewWillEnter(){
        this.currentOrientation = this.screenOrientation.type;
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.LANDSCAPE);

        var self = this;
        window.setTimeout(function(){
            JsBarcode("#code128", self.profile.cat_username);
          }, 100);
    }

    ionViewWillLeave() {
        this.screenOrientation.lock(this.currentOrientation);
        setTimeout(() => {
            this.screenOrientation.unlock();
        }, 200);
        
    }

    dismiss(): void {
        this.viewCtrl.dismiss();
    }
}