import { Component } from '@angular/core';

import { NavParams, NavController, AlertController, App } from 'ionic-angular';
import { CatalogProvider } from './catalogProvider';
import { AnalyticsProvider } from '../../providers/analyticsProvider'

import { BookPage } from './book';
import $ from 'jquery';
import { AppConfig } from '../../config';

@Component({
  selector: 'page-events',
  templateUrl: 'list.html'
})
export class ListPage {
  titles: any[];
  label: string;
  parentLabel: string;
  subCategories: any[];
  page: number = 1;
  nextPageAvailable: boolean = true;
  title:string;
  pikaUrl:string;

  constructor(private catalogProvider: CatalogProvider
    , private navParams: NavParams
    , private navCtrl: NavController
    , private alertCtrl: AlertController
    , private analytics: AnalyticsProvider) {
    this.title = AppConfig.APP_TITLE_HTML;
    this.pikaUrl = AppConfig.PIKA_BASE_URL.substring(0, AppConfig.PIKA_BASE_URL.length-1);
    this.titles = [];
    this.subCategories = [];
  }

  track() {
    var page = "ListPage";
    this.analytics.trackView(page);
    return page;
  }

  parseTitles(response) {

    var html = $.parseHTML(response.records);

    $(html).find('img').each((a, b) => {
      var image = $(b);
      var src = image.attr('src');
      var url = this.pikaUrl + src;
      this.titles.push({
        url: url,
        id: src.replace('/bookcover.php?id=', '').replace('&size=medium&type=grouped_work', ''),
        title: image.attr('title')
      });
    });
  }

  parseSubcategories(response) {
    this.subCategories = [];
    var html = $.parseHTML(response.subcategories);
    $(html).each((a, b) => {
      var id = $(b).attr('data-sub-category-id');
      var label = $(b).text();
      this.subCategories.push({
        id: id,
        label: label
      });
    });
  }

  ionViewWillEnter() {
    var categoryId = this.navParams.get('categoryId');
    this.parentLabel = this.navParams.get('parentLabel');

    if(this.titles.length <= 0) {
      this.catalogProvider.getTitlesForCategory(categoryId).subscribe(s => {
        if (s.subcategories) {
          this.parseSubcategories(s);
        }
        else {
          this.parseTitles(s);
          if(s.numRecords < 24) {
            this.nextPageAvailable = false;
          }
        }
        this.label = s.label;
      })
    }
  }

  goToBook(id: string) {
    var self = this;
    this.catalogProvider.getBookById(id).subscribe(s => {
      if (s.results.length === 1) {
        self.navCtrl.push(BookPage, { book: s.results[0] });
      }
      else {
        let alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: 'Unable to find book',
          buttons: ['OK']
        });
        alert.present();
      }
    });
  }

  loadMore() {
    if (this.titles.length > 0) {
      var categoryId = this.navParams.get('categoryId');
      this.catalogProvider.getMoreTitlesForCategory(categoryId, this.page).subscribe(s => {
        this.parseTitles(s);
        if(s.numRecords < 24) {
          this.nextPageAvailable = false;
        }
      })
    }
  }

  goToCategory(id: string) {
    this.navCtrl.push(ListPage, { categoryId: id, parentLabel: this.label });
  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');

    setTimeout(() => {
      if(this.nextPageAvailable) {
        this.page++
        this.loadMore();
      }
      console.log('Async operation has ended');
      infiniteScroll.complete();
    }, 1500);
  }
}