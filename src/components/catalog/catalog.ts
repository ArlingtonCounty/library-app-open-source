import { Component, ViewChild, ElementRef } from '@angular/core';
import { Keyboard } from '@ionic-native/keyboard';
import { NavController, PopoverController, AlertController, Content } from 'ionic-angular';

import { CatalogProvider, Book, SearchResult, FacetSet } from './catalogProvider';
import { BookPage, CatalogFilterPage } from '../components';
import { AnalyticsProvider } from '../../providers/providers';
import { ITracking } from '../../app/app.component';
import { AppConfig } from '../../config';

declare var cordova;

@Component({
  selector: 'page-catalog',
  templateUrl: 'catalog.html'
})
export class CatalogPage implements ITracking  {
  results:Book[];
  searchResult:SearchResult;
  hideCovers:boolean;
  facets:FacetSet[];
  filters:any[];
  searchText:string;
  page:number = 1;
  maxPage:number = 1;
  timer = null;
  title:string;
  pikaUrl:string;

  @ViewChild(Content) content: Content;
  @ViewChild('searchbar') searchbar:ElementRef;

  constructor(public navCtrl: NavController
            , private catalogProvider:CatalogProvider
            , private popover:PopoverController
            , private analytics:AnalyticsProvider
            , private alertCtrl:AlertController
            , private keyboard:Keyboard) {
    this.title = AppConfig.APP_TITLE_HTML;
    this.pikaUrl = AppConfig.PIKA_BASE_URL;
    this.results = [];
    this.searchResult = null;
    this.catalogProvider.clearFacets();
    this.searchText = '';


  }

  track() {
    this.analytics.trackView('CatalogPage');
  }

  ionViewDidEnter() {
    this.keyboard.disableScroll(true);
    if(this.results.length <= 0) {
      setTimeout(() => this.searchbar.nativeElement.focus(), 100);
    }
  }

  ionViewWillLeave() {
    this.keyboard.disableScroll(false);
    //make sure keyboard is closed
    this.closeKeyboard();
  }

  closeKeyboard() {
    this.searchbar.nativeElement.blur();
    this.keyboard.close();
  }

  clearSearch() {
    this.searchText = '';
  }

  goToBook(book:Book){
    this.navCtrl.push(BookPage, { book: book, record: book.records[0] });
  }

  searchEvent(event:any){
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      this.page = 1;
      this.maxPage = 1;
      this.searchCatalog(event.target.value);
    }, 350);
  }

  searchCatalog(val) {
    if(val !== undefined){

      this.catalogProvider.searchCatalog(val, this.page).debounceTime(500).subscribe(s =>{
        this.hideCovers = this.catalogProvider.getHideCovers();
        this.results.length = 0;
        Array.prototype.push.apply(this.results, s.results);
        this.searchResult = s;
        this.maxPage = Math.ceil(this.searchResult.recordCount / 20) || 1;
        this.facets = s.facets;
        console.log("Facets", this.facets);
      }, error => {
        let alert = this.alertCtrl.create({
            title: 'Error',
            message: 'Unable to retrieve data.  Check your internet connection.',
            buttons: ['OK']
        });
        alert.present();

      });
    } else {
      this.results.length = 0;
      this.facets = null;
    }
  }

  scan() {
    var self = this;
    cordova.plugins.barcodeScanner.scan(
      function (result) {
        if(!result.cancelled){
          self.catalogProvider.getBookById(result.text).subscribe(s => {
            if(s.results.length === 1){
              self.navCtrl.push(BookPage, { book: s.results[0] });
            }
            else {
              let alert = self.alertCtrl.create({
                title: 'Error',
                subTitle: 'Sorry Unable to find scanned title, try keyword search.',
                buttons: ['OK']
              });
              alert.present();
            }
          });
        }
      },
      function (error) {
          let alert = self.alertCtrl.create({
            title: 'Scanning Failure',
            subTitle: error,
            buttons: ['OK']
          });
          alert.present();
      },
      {
          "preferFrontCamera" : false, // iOS and Android
          "showFlipCameraButton" : true, // iOS and Android
          "prompt" : "Place a barcode inside the scan area", // supported on Android only
          "resultDisplayDuration": 0, //Android prevent result display
          "disableAnimations" : true
      }
   );
  }

  applyFilters() {
    let p = this.popover.create(CatalogFilterPage, {
      facets: this.searchResult.facets
    }, {
      cssClass: 'popover-content'
    });
    p.onDidDismiss(d=> {
      if (d) {
        var searchValue = this.searchbar.nativeElement.value;
        if(searchValue) {
          this.first();
        }
      }
    });
    p.present();
  }

  removeFacet(key, value){
    this.catalogProvider.removeFacet(key, value);
    this.searchCatalog(this.searchbar.nativeElement.value);
  }

  next() {
    if(this.page != this.maxPage) {
      this.page++;
      this.topAndSearch();
    }
  }

  prev() {
    if(this.page > 1) {
      this.page--;
      this.topAndSearch();
    }
  }

  first() {
    this.page = 1;
    this.topAndSearch();
  }

  topAndSearch() {
    this.content.scrollToTop(100);
    this.searchCatalog(this.searchText);
  }
}
