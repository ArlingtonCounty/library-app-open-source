import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';
import { ListPage } from '../components';
import { CatalogProvider } from './catalogProvider';
import { AnalyticsProvider } from '../../providers/providers';
import { ITracking } from '../../app/app.component';
import { AppConfig } from '../../config';

@Component({
  selector: 'page-events',
  templateUrl: 'browse.html'
})
export class BrowsePage  implements ITracking {
    categories:any[];
    title:string;
    constructor(private catalogProvider:CatalogProvider
              , private navController:NavController
              , private analytics:AnalyticsProvider){ 
                this.title = AppConfig.APP_TITLE_HTML;
              }

    track() {
      this.analytics.trackView('BrowsePage');
    }

    ionViewWillEnter(){
      this.catalogProvider.getBrowseCategories().subscribe(s=> {
        this.categories = s;
      })
    }

    goToCategory(id:string){
      this.navController.push(ListPage, {categoryId: id} );
    }
}