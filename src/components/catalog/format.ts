import { Component } from '@angular/core';
import { NavParams, ModalController, NavController } from 'ionic-angular';
import { CatalogProvider } from './catalogProvider';
import { Book, BookPage, RequestHoldPage, CheckoutOverdrivePage } from '../components';
import { AnalyticsProvider } from '../../providers/providers';
import { ITracking } from '../../app/app.component';
import { AppConfig } from '../../config';

@Component({
  selector: 'page-book',
  templateUrl: 'format.html'
})
export class FormatPage implements ITracking {
  book:Book;
  record:any = {};
  noHold:boolean = false;
  overdrive:boolean = false;
  webview:boolean = false;
  eBookDetails: string = '';
  title:string;
  pikaUrl:string;

  copiesAndHoldsLoaded: boolean = false;
  copies: number = 0;
  holds: number = 0;

  constructor(private navParams: NavParams
            , private catalogProvider:CatalogProvider
            , private modalCtrl:ModalController
            , private analytics:AnalyticsProvider
            , private navCtrl:NavController) {
    this.title = AppConfig.APP_TITLE_HTML;
    this.pikaUrl = AppConfig.PIKA_BASE_URL;
    this.book = new Book();
  }

  track() {
    var dimensions = {};
    
    this.analytics.trackView('FormatPage', dimensions);
  }

  ionViewWillEnter() {
      this.book =  this.navParams.get('book');
      this.record = this.navParams.get('record');
      this.getStatus();
      this.checkEBookFormat();
      this.getCopyAndHoldCounts();
  }

  getCopyAndHoldCounts() {
      this.catalogProvider.getCopyAndHoldCounts(this.record.bibId).subscribe(ch => {
        this.copies = ch.result.copies;
        this.holds = ch.result.holds
        this.copiesAndHoldsLoaded = true;
      })

  }

  openHoldWindow(bookRecord:any) {
    let requestModal = this.modalCtrl.create(RequestHoldPage, { book: this.book, bookRecord: bookRecord });
    requestModal.onDidDismiss(dismiss => {
      console.log("Request Hold Dismissed.");
    });
    requestModal.present(); 
  }

  getStatus() {
    this.record.items.forEach(item => {
      if(item.status === "Available Online") {
        this.noHold = true;
        this.webview = true;
      }
    })
      var overdrive = this.record.bibId.toLowerCase().indexOf('overdrive')
      if(overdrive >= 0) {
        this.overdrive = true;
      }
  }

  checkEBookFormat() {
    if(this.record.format) {
      if(this.record.format.indexOf('e') == 0) {
        this.book.item_details.forEach(item => {
          if(item.indexOf(this.record.bibId) == 0) {
            var formatBuilder = '';
            if(item.indexOf('Adobe EPUB eBook') >= 0) {
              formatBuilder += 'Adobe EPUB eBook, ';
            }
            if(item.indexOf('OverDrive Read') >= 0) {
              formatBuilder += 'OverDrive Read, ';
            }
            if(item.indexOf('Adobe PDF eBook') >= 0) {
              formatBuilder += 'Adobe PDF eBook, ';
            }
            if(item.indexOf('Kindle Book') >= 0) {
              formatBuilder += 'Kindle Book ';
            }
            if(item.indexOf('OverDrive MP3 Audiobook') >= 0) {
              formatBuilder += 'OverDrive MP3 Audiobook, ';
            }
            if(item.indexOf('OverDrive Listen') >= 0) {
              formatBuilder += 'OverDrive Listen ';
            }
            this.eBookDetails = formatBuilder
          }
        })
      }
    }
  }

  goToWebView(record) {
    if(record.bibId.indexOf('overdrive:') === 0) {
      var overdriveId = record.bibId.replace('overdrive:', '');
      window.open(`${this.pikaUrl}OverDrive/${overdriveId}/Home`, '_system');
    } else if(record.bibId.indexOf('external_econtent:') === 0) {
      var eContentId = record.bibId.replace('external_econtent:', ''); 
      window.open(`${this.pikaUrl}ExternalEContent/${eContentId}`, '_system');
    }
    else
      window.open(`${this.pikaUrl}Record/${record.bibId}`, '_system');
  }

  goToBibRecord(){
    this.navCtrl.push(BookPage, { book: this.book, record: null });
  }

  checkOut(record) {
    this.navCtrl.push(CheckoutOverdrivePage, { book: this.book, bookRecord: record });
  }

}
