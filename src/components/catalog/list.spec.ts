import { ListPage } from './list';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA, DebugElement } from '@angular/core';

import { NavController, AlertController, NavParams } from 'ionic-angular';
import { AnalyticsProvider, AnalyticsProviderMock } from '../../providers/providers';
import { CatalogProvider } from '../catalog/catalogProvider';

import { NavParamsMock } from '../../app/app.mocks';

describe('List Page:', () => {

    let comp: ListPage;
    let fixture: ComponentFixture<ListPage>;
    let de: DebugElement;

    beforeEach(() => {
        TestBed.configureTestingModule({
            schemas: [CUSTOM_ELEMENTS_SCHEMA],
            declarations: [ListPage],
            providers: [
                { provide: NavController, useValue: NavController},
                { provide: AlertController, useValue: AlertController },
                { provide: CatalogProvider, useValue: CatalogProvider },
                { provide: AnalyticsProvider, useValue: new AnalyticsProviderMock() },
                { provide: NavParams, useValue: new NavParamsMock() }
            ],
        });
        fixture = TestBed.createComponent(ListPage);
        // #trick
        // If you want to trigger ionViewWillEnter automatically de-comment the following line
        // fixture.componentInstance.ionViewWillEnter();
        fixture.detectChanges();
        comp = fixture.componentInstance;
        de = fixture.debugElement;
    });

    describe('.constructor()', () => {
        it('Should be defined', () => {
            expect(comp).toBeDefined();
        });
    });

    describe('Google Analytics', () => {
        it('Should track the correct page', () => {
            var page = comp.track();
            expect(page).toEqual("ListPage");
        })
    });
});
