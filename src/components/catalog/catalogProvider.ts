import { Injectable } from '@angular/core';

import { PikaProvider, PikaTempProvider } from '../../providers/providers';
import _ from 'lodash';

export class SearchResult {
    constructor() {
        this.results = [];
        this.recordStart = this.recordEnd = this.recordCount = 0;
    }
    results:Book[];
    recordStart:number;
    recordEnd:number;
    recordCount:number;
    facets:FacetSet[];
}

export class Facet {
    constructor(json:any) {
        this.label = json.display;
        this.value = json.value;
        this.count = json.count;
        this.isApplied = json.isApplied;
    }

    label:string;
    value:string;
    count:number;
    isApplied:boolean;
}

export class FacetSet {
    constructor(json:any, filterName:string) {
        this.label = json.label;
        this.filterParam = filterName;

        this.facets = [];

        var facetKeys = Object.keys(json.list);
        facetKeys.forEach(f=> {
            if(f){
                this.facets.push(new Facet(json.list[f]));
            }
        });
    }

    label:string;
    filterParam:string;
    facets:Facet[];
    expand:boolean;
    
    raw:any;
}

export class Book { 
    title_display:string;
    author_display:string;
    contributors:string[];
    publishDate:string;
    primary_isbn:string;
    id:string;
    display_description:string;
    publisher:string;
    series:string[];
    subjects:string[];
    formats:string[];
    availability:any[];
    scoping:any[];
    records:any[];
    item_details:string[];

    ar_interest_level:string;
    ar_point_value:number;
    ar_reading_level:number;

    lexile_score:number;

    raw:any;

    static fromJson(obj:any) : Book {
        var b = new Book();
        
        b.id = obj.id;
        b.title_display = obj.title_display;
        b.author_display = obj.author_display;
        b.contributors = obj.auth_author2 || [];
        b.publishDate = obj.publishDate;
        b.primary_isbn = obj.primary_isbn;
        b.display_description = obj.display_description;
        b.publisher = obj.publisherStr;
        b.series =  obj.series || [];
        b.subjects = obj.subject_facet || [];
        b.formats = obj.format_arlington;
        b.ar_interest_level = obj.accelerated_reader_interest_level;
        b.ar_point_value = obj.accelerated_reader_point_value;
        b.ar_reading_level = obj.accelerated_reader_reading_level;
        b.lexile_score = obj.lexile_score;
        b.item_details = obj.item_details || [];
        b.availability = [];
        b.records = [];
        b.scoping = [];
        
        obj.item_details = obj.item_details || [];
        obj.item_details.forEach(detail => {
            var splitDetail = detail.split('|');
            //"ils:.b12987608|
            //.i13593821|
            //Westover Kids Picture Books|
            //JP MEADO|
            //|
            //|
            //1|
            //false|
            //false|
            //|
            //|
            //|
            //|
            //On Shelf|
            //Nov 16, 2016|
            //wjfp||"
            var bib = splitDetail[0];
            var bibId = bib.replace("ils:", "");
            b.availability.push({
                "bib": bib,
                "bibId": bibId,
                "location": splitDetail[2],
                "call": splitDetail[3],
                "status": splitDetail[13],
                "lastCheckIn": splitDetail[14]
            })
        });
        
        //ils:.b11450964|
        //.i2087330x|
        //On Shelf|
        //On Shelf|
        //false|
        //true|
        //true|
        //false|
        //false|
        //true|
        //999||
        obj.scoping_details_arlington = obj.scoping_details_arlington || [];
        obj.scoping_details_arlington.forEach(detail => {    
            var s = detail.split("|");
            var scoping = {
                bib: s[0],
                status: s[2],
                statusDetail: s[3],
                available: s[5]
            }
            b.scoping.push(scoping);
        });
    
        //ils:.b12987608|
        //Book|
        //Books|
        //1st ed.|
        //English|
        //Simon & Schuster Books for Young Readers,|
        //c2008.|
        //1 v. (unpaged) : col. ill. ; 24 cm.
        obj.record_details = obj.record_details || [];
        obj.record_details.forEach(detail => {
            var d = detail.split("|");
            d[0] = d[0] || "";
            d[6] = d[6] || "";
            
            d[6] = d[6].replace(/\D/g, "");
            if(d[6].length >= 4) {
                d[6] = d[6].substring(0,4);
            }
            var record = {
                bib: d[0],
                bibId: d[0].replace("ils:", ""),
                format: d[1],
                category: d[2],
                edition: d[3],
                language: d[4],
                publisher: d[5],
                detail: d[6],
                physicalDescription: d[7],
                items: [],
                scoping: [],
                available: null
            };
            record.items = _.filter(b.availability, { "bib": record.bib });
            record.scoping = _.filter(b.scoping, {"bib": record.bib });
            var a = _.find(record.scoping, {available: "true" });
            
            record.available = a ? true : false;
            b.records.push(record);
            
        });
        b.raw = obj;
        return b;
    }
}


@Injectable()
export class CatalogProvider {
    books:Book[];

    constructor(public pika:PikaProvider, private pikaTempProvider:PikaTempProvider){
        this.books = [];
    }

    findBook(keyword:string){
        return this.pika.searchApi("search", keyword, null)
        .map(r => {        
            var body = r.json();
            
            if(body.result.recordCount > 0) {
                this.books.length = 0;
                body.result.recordSet.forEach(record => {
                    this.books.push(Book.fromJson(record));
                });  
                var returnValue = new SearchResult();
                returnValue.results = this.books;
                returnValue.recordCount = body.result.recordCount;
                returnValue.recordEnd = body.result.recordEnd;
                returnValue.recordStart = body.result.recordStart;
                return returnValue;
            }
            else {
                this.books.length = 0;
                return new SearchResult();
            }   
            
        });
    }

    getBookById(bookId:string){
        return this.pika.searchApi("search", bookId, null).map(m=>{
            var body = m.json();
            if(body.result.recordCount > 0) {
                this.books.length = 0;
                body.result.recordSet.forEach(record => {
                    this.books.push(Book.fromJson(record));
                });  
                var returnValue = new SearchResult();
                returnValue.results = this.books;
                returnValue.recordCount = body.result.recordCount;
                returnValue.recordEnd = body.result.recordEnd;
                returnValue.recordStart = body.result.recordStart;
                
                return returnValue;
            }
            else {
                this.books.length = 0;
                return new SearchResult();
            }
        });
    }

    searchCatalog(keyword:string, page:number) {
        return this.pika.searchApi("search", keyword, this.facets, this.sort, page)
        .map(r => {
            var body = r.json();
            console.log(`Search for ${keyword}`, body);
            
            if(body.result.recordCount > 0) {
                this.books.length = 0;
                if(!body.result.recordSet) console.log("Record Set is null");
                body.result.recordSet.forEach(record => {
                    this.books.push(Book.fromJson(record));
                });  
                var returnValue = new SearchResult();
                returnValue.results = this.books;
                returnValue.recordCount = body.result.recordCount;
                returnValue.recordEnd = body.result.recordEnd;
                returnValue.recordStart = body.result.recordStart;
                returnValue.facets = [];

                //We can get all facets later if wanted but get these specific ones
                //as those were specified in the requirements.
                //Object.keys(body.result.facetSet);
                if(body.result.facetSet.availability_toggle_arlington){
                    returnValue.facets.push(new FacetSet(body.result.facetSet.availability_toggle_arlington, "availability_toggle_arlington")); }
                if(body.result.facetSet.format_category_arlington) {
                    returnValue.facets.push(new FacetSet(body.result.facetSet.format_category_arlington, "format_category_arlington")); }
                if(body.result.facetSet.available_at_arlington) {
                    returnValue.facets.push(new FacetSet(body.result.facetSet.available_at_arlington, "available_at_arlington")); }
                if(body.result.facetSet.econtent_source_arlington) {
                    returnValue.facets.push(new FacetSet(body.result.facetSet.econtent_source_arlington, "econtent_source_arlington")); }
                if(body.result.facetSet.target_audience) {
                    returnValue.facets.push(new FacetSet(body.result.facetSet.target_audience, "target_audience")); }

                return returnValue;
            }
            else {
                this.books.length = 0;
                return new SearchResult();
            }   
            
        });
    }

    barcodeSearch(barcode:string){
        return this.pika.barcodeSearch(barcode)
                .map(r => {
                    var body = r.json();

                    return body;
                });
    }

    placeHold(username, password, bibliographyId, locationId) {

        return this.pika.placeHold(username, password, bibliographyId, locationId).map(m=> {
            var body = m.json();
            return body;
        });
    }

    // placeOverdriveHold(username, password, overdriveId){
    //     return this.pika.placeOverdriveHold(username, password, overdriveId).map(m=> {
    //         let body = m.json();
    //         return body;
    //     });
    // }

    placeOverdriveHold(account:any, overdriveId:string, overdriveEmail:string){
        return this.pikaTempProvider.authenticateAction(account, this.pikaTempProvider.placeOverdriveHold(account.patronId, overdriveId, overdriveEmail)).map(m=> {
            //let body = m.json();
            return m;
        });
    }

    checkoutOverdrive(account:any, overdriveId:string, overdriveEmail:string){
        return this.pikaTempProvider.authenticateAction(account, this.pikaTempProvider.checkoutOverdrive(account.patronId, overdriveId, overdriveEmail)).map(m=> {
            //let body = m.json();
            return m;
        });
    }

    readOnline(account: any, overdriveId:string){
        return this.pikaTempProvider.authenticateAction(account, this.pikaTempProvider.readOnline(account.patronId, overdriveId, 'ebook-overdrive')).map(m=> { 
            if(m.success){
                window.location.href = m.downloadUrl;
            }
            return m;
        });
    }

    getFeaturedTitles() {
        //hardcoded listid here
        return this.pika.getListTitles(1541).map(m => {
            var body = m.json();
            return body.titles;
        })
    }

    getBrowseCategories() {
        return this.pika.getCategories().map(m => {
            var body = m.json();
            return body;
        });
    }

    getTitlesForCategory(categoryId){
        return this.pika.getTitlesForCategory(categoryId).map(m => {
            var body = m.json();
            return body;
        });
    }

    getMoreTitlesForCategory(categoryId, page){
        return this.pika.getMoreTitlesForCategory(categoryId, page).map(m => {
            var body = m.json();
            return body;
        });
    }

    getRecordDescription(bibId:string){
        return this.pika.getItemDescription(bibId).map(m=> {
            var body = m.json();
            return body;
        });
    }

    getCopyAndHoldCounts(id) {
        return this.pika.getCopyAndHoldCounts(id).map(m => {
            var body = m.json();
            return body;
        })
    }

    facets:any = {};
    setFacets(facets) {
        this.facets = facets;
    }

    getAppliedFacets() {
        return this.facets;
    }

    removeFacet(key, value){
        this.facets[key][value] = false;
    }

    clearFacets(){
        this.facets = {};
    }
    
    hideCovers:boolean = false;
    setHideCovers(h){
        this.hideCovers = h;
    }
    getHideCovers(){
        return this.hideCovers;
    }

    sort:string = "relevance";
    setSort(sort) {
        this.sort = sort;
    }
    getSort() {
        return this.sort;
    }
    clearSort() {
        this.sort = "relevance";
    }   
}