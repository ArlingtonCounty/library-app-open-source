import { Component } from '@angular/core';

import { NavController, NavParams, AlertController } from 'ionic-angular';
import { CatalogProvider } from './catalogProvider';

import { Book, FormatPage } from '../components';
import { AnalyticsProvider, AnalyticsDimensions } from '../../providers/providers';
import { ITracking } from '../../app/app.component';
import { AppConfig } from '../../config';

@Component({
  selector: 'page-book',
  templateUrl: 'book.html'
})
export class BookPage implements ITracking {
  book:Book;
  formats = {};
  title:string;
  pikaUrl:string;

  constructor(private navParams: NavParams
            , private analytics:AnalyticsProvider
            , private catalogProvider: CatalogProvider
            , private alertCtrl: AlertController
            , private navCtrl:NavController) {
    this.title = AppConfig.APP_TITLE_HTML;
    this.pikaUrl = AppConfig.PIKA_BASE_URL;
    let b = this.navParams.get('book');
    this.book = b;

    this.sortByFormat();
  }

  sortByFormat () {
    // initialize each format as empty array
    if(!this.book || !this.book.formats) return;

    this.book.formats.forEach(format => {
      this.formats[format] = [];
    })

    this.book.formats.forEach(format => {
      this.book.records.forEach(record => {
        if(format == record.format) {
          if(record.category.indexOf('s') == record.category.length-1) {
            record.category = record.category.substring(0, record.category.length-1);
          }
          this.formats[format].push(record)
        }
      })
    })
  }

  track() {
    var dimensions = {};
    
    dimensions[AnalyticsDimensions.BookTitle] = this.book.title_display;

    this.analytics.trackView('BookPage', dimensions);
  }

  goToFormat(record){
    console.log(this.book);
    this.navCtrl.push(FormatPage, { book: this.book, record: record });
  }

  goToWebView(book) {
    window.open(`${this.pikaUrl}GroupedWork/${book.id}`, '_system');
  }

  goToBook(id: string) {
    var self = this;
    var strippedId = id;
    if(id.indexOf("grouped_work") >=  0) {
      strippedId = id.substring(13);
    }
    this.catalogProvider.getBookById(strippedId).subscribe(s => {
      if (s.results.length === 1) {
        self.navCtrl.push(BookPage, { book: s.results[0] });
      }
      else {
        let alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: 'Unable to find book',
          buttons: ['OK']
        });
        alert.present();
      }
    });
  }

}
