import { Component } from '@angular/core';
import { NavParams, ViewController, AlertController, LoadingController, NavController, ModalController } from 'ionic-angular';
import { CatalogProvider } from './catalogProvider';
import { AccountService, LocalAccount, Profile } from '../../providers/accountService';
import { AccountPage, LoginPage } from '../components';

import { AnalyticsProvider } from '../../providers/providers';
import { ITracking } from '../../app/app.component';
import { AppConfig } from '../../config';

@Component({
    selector: 'page-checkoutOverdrive',
    templateUrl: 'checkoutOverdrive.html'
})
export class CheckoutOverdrivePage implements ITracking {
    accounts: LocalAccount[];
    bibliographyId: string;
    isOverdrive: boolean;
    bookRecord: any;
    book: any;
    selectedAccount: LocalAccount;
    profile: Profile;
    emailSelect: string;
    Email: string = '';
    title:string;

    constructor(private catalogProvider: CatalogProvider,
        private accountService: AccountService,
        private params: NavParams,
        private viewCtrl: ViewController,
        private alertCtrl: AlertController,
        private modalCtrl: ModalController,
        private navCtrl: NavController,
        private loadingCtrl: LoadingController,
        private analytics: AnalyticsProvider) {
        this.title = AppConfig.APP_TITLE_HTML;
        this.accountService.getSavedAccounts().subscribe(accounts => {
            this.accounts = accounts;
            this.checkLoggedIn();
            this.selectedAccount = this.accounts[0] || null;
        });
    }

    track() {
        this.analytics.trackView('CheckoutOverdrive');
    }

    ionViewWillEnter() {
        this.emailSelect = 'enter';
        this.bookRecord = this.params.get('bookRecord');
        this.book = this.params.get('book');
        console.log("book", this.book);

        if (this.bookRecord.bib.indexOf('overdrive:') >= 0) {
            this.bibliographyId = this.bookRecord.bib.replace('overdrive:', '');
            this.isOverdrive = true;
        }
        else {
            this.bibliographyId = this.bookRecord.bibId;
            this.isOverdrive = false;
        }
        console.log(this.bookRecord, this.bibliographyId)

        this.getProfile();  
    }

    getProfile() {
        if (this.selectedAccount && this.accounts.length > 0) {
            var loading = this.loadingCtrl.create();
            loading.present();
            this.accountService.getProfile(this.selectedAccount, true).subscribe(s => {
                this.profile = s;
                if (this.profile.promptForOverdriveEmail === "0") {
                    this.emailSelect = 'overdrive';
                    this.Email = this.profile.overdriveEmail;
                    console.log(this.Email, this.profile, this.profile.overdriveEmail);
                }
                else {
                    this.emailSelect = 'enter';
                    this.Email = '';
                }
                console.log(this.profile);
                loading.dismissAll();
            })
        }
    }

    changeEmailSelect(event) {
        if (event === 'enter') {
            this.Email = '';
        }
        else if (event === 'overdrive') {
            this.Email = this.profile.overdriveEmail;
        }
        else if (event === 'account') {
            this.Email = this.profile.email;
        }
        console.log(this.Email)
    }

    checkout() {
        if (!this.selectedAccount) {
            let alert = this.alertCtrl.create({
                title: 'Please login',
                subTitle: 'Login to checkout this item',
                buttons: ['OK']
            });
            alert.present();
        }
        else {
            var loading = this.loadingCtrl.create();
            loading.present();
            this.catalogProvider.checkoutOverdrive(this.selectedAccount, this.bibliographyId, this.Email).subscribe(s => {
                loading.dismissAll();
                var buttons = [];
                if (s.success) {
                    buttons.push(
                        {
                            text: 'Go to Account',
                            handler: data => {
                                this.navCtrl.push(AccountPage, { account: this.selectedAccount });
                            }
                        });
                }
                buttons.push('OK');

                let alert = this.alertCtrl.create({
                    title: 'Checkout',
                    subTitle: s.message,
                    buttons: buttons
                });
                alert.present();
                this.viewCtrl.dismiss();
            }, e => {
                console.log(e);
                loading.dismissAll();
                let alert = this.alertCtrl.create({
                    title: 'Error',
                    subTitle: 'A technical error has ocurred completing your request',
                    buttons: ['OK']
                });
                alert.present();
            });
        }
    }

    closeModal() {
        this.viewCtrl.dismiss();
    }

    checkLoggedIn() {
        if (this.accounts.length < 1) {
            setTimeout(() => {
                this.redirectToLogin()
            }, 100);
        }
    }

    redirectToLogin() {
        var self = this;
        //show login page
        let loginModal = self.modalCtrl.create(LoginPage);
        loginModal.onDidDismiss(data => {
            if (data) {
                this.accountService.getSavedAccounts().subscribe(accounts => {
                    this.accounts = accounts;
                    this.selectedAccount = this.accounts[0];
                    this.getProfile();
                })
            }
            else {
                this.closeModal();
            }
        });
        loginModal.present();
    }
}