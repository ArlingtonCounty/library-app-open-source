import { Component } from '@angular/core';
import { NavParams, ViewController, AlertController, LoadingController, ModalController } from 'ionic-angular';
import { CatalogProvider } from './catalogProvider';
import { AccountService, LocalAccount, Profile } from '../../providers/accountService';
import { LoginPage } from '../components'

import { AnalyticsProvider } from '../../providers/providers';
import { LibraryInfoProvider } from '../../providers/libraryInfoProvider';
import { ITracking } from '../../app/app.component';
import { AppConfig } from '../../config';

@Component({
    selector: 'page-requestHold',
    templateUrl: 'requestHold.html'
})
export class RequestHoldPage implements ITracking {
    accounts: LocalAccount[];
    locations: any[];
    bibliographyId: string;
    isOverdrive: boolean;
    bookRecord: any;
    book: any;
    selectedAccount: LocalAccount;
    selectedLocation: string;
    profile: Profile;
    emailSelect: string;
    Email: string = '';
    title:string;

    constructor(private catalogProvider: CatalogProvider,
        private accountService: AccountService,
        private params: NavParams,
        private viewCtrl: ViewController,
        private libraryInfoProvider: LibraryInfoProvider,
        private modalCtrl: ModalController,
        private alertCtrl: AlertController,
        private loadingCtrl: LoadingController,
        private analytics: AnalyticsProvider) {
            this.title = AppConfig.APP_TITLE_HTML;
    }

    track() {
        this.analytics.trackView('RequestHoldPage');
    }

    ionViewWillEnter() {

        this.accountService.getSavedAccounts().subscribe(accounts => {
            this.accounts = accounts;
            this.checkLoggedIn();
            this.selectedAccount = this.accounts[0] || null;

            this.emailSelect = 'enter';
            this.bookRecord = this.params.get('bookRecord');
            this.book = this.params.get('book');

            if (this.bookRecord.bib.indexOf('overdrive:') >= 0) {
                this.bibliographyId = this.bookRecord.bib.replace('overdrive:', '');
                this.isOverdrive = true;
            }
            else {
                this.bibliographyId = this.bookRecord.bibId;
                this.isOverdrive = false;
            }
            
            this.locations = this.libraryInfoProvider.returnLibraryLocationTags();
            this.getProfile();
            this.selectedLocation = this.selectedAccount ? this.selectedAccount.homeLibraryCode : "c";
        });
    }

    getProfile() {
        if (this.selectedAccount && this.accounts.length > 0) {
            var loading = this.loadingCtrl.create();
            loading.present();
            this.accountService.getProfile(this.selectedAccount, true).subscribe(s => {
                this.profile = s;
                if (this.profile && this.profile.promptForOverdriveEmail === "0") {
                    this.emailSelect = 'overdrive';
                    this.Email = this.profile.overdriveEmail;
                    console.log(this.Email, this.profile, this.profile.overdriveEmail);
                }
                else {
                    this.emailSelect = 'enter';
                    this.Email = '';
                }
                console.log(this.profile);
                this.selectedLocation = this.selectedAccount.homeLibraryCode;

                loading.dismissAll();
            })
        }
    }

    changeEmailSelect(event) {
        if (event === 'enter') {
            this.Email = '';
        }
        else if (event === 'overdrive') {
            this.Email = this.profile.overdriveEmail;
        }
        else if (event === 'account') {
            this.Email = this.profile.email;
        }
        console.log(this.Email)
    }

    requestHold() {
        if (!this.selectedAccount) {
            let alert = this.alertCtrl.create({
                title: 'Please login',
                subTitle: 'Login to place this item on hold',
                buttons: [
                    {
                        text: 'Login',
                        handler: () => {
                            this.redirectToLogin();
                            console.log('Login clicked');
                        }
                    },
                    {
                        text: 'Cancel',
                        handler: () => {
                            console.log('Cancel clicked');
                        }
                    }
                ]
            });
            alert.present();
        }
        else {
            var loading = this.loadingCtrl.create();
            loading.present();
            if (!this.isOverdrive) {
                this.catalogProvider.placeHold(this.selectedAccount.username, this.selectedAccount.password, this.bibliographyId, this.selectedLocation)
                    .subscribe(s => {
                        loading.dismissAll();
                        var title = "Hold Request";
                        var message = s.result.message;
                        if (s.result.message === "There are no holdable items for this title.") {
                            title = "Place Hold";
                            message = "Sorry, this item is not holdable.";
                        }
                        else if (s.result.message === "Unable to contact the circulation system.  Please try again in a few minutes.") {
                            title = "Place Hold";
                            message = "Please try to place this hold in the web view.";
                        }
                        let alert = this.alertCtrl.create({
                            title: title,
                            subTitle: message,
                            buttons: ['OK']
                        });
                        alert.present();
                        this.viewCtrl.dismiss();
                    }, e => {
                        console.log(e);
                        loading.dismissAll();
                        let alert = this.alertCtrl.create({
                            title: 'Error',
                            subTitle: 'A technical error has ocurred completing your request',
                            buttons: ['OK']
                        });
                        alert.present();
                    });
            } else {
                this.catalogProvider.placeOverdriveHold(this.selectedAccount, this.bibliographyId, this.Email).subscribe(s => {
                    loading.dismissAll();
                    let alert = this.alertCtrl.create({
                        title: 'Hold Request',
                        subTitle: s.message,
                        buttons: ['OK']
                    });
                    alert.present();
                    this.viewCtrl.dismiss();
                }, e => {
                    console.log(e);
                    loading.dismissAll();
                    let alert = this.alertCtrl.create({
                        title: 'Error',
                        subTitle: 'A technical error has ocurred completing your request',
                        buttons: ['OK']
                    });
                    alert.present();
                });
            }
        }
    }

    closeModal() {
        this.viewCtrl.dismiss();
    }

    checkLoggedIn() {
        if (this.accounts.length < 1) {
            setTimeout(() => {
                this.redirectToLogin()
            }, 100);
        }
    }

    redirectToLogin() {
        var self = this;
        //show login page
        let loginModal = self.modalCtrl.create(LoginPage);
        loginModal.onDidDismiss(data => {
            if (data) {
                this.accountService.getSavedAccounts().subscribe(accounts => {
                    this.selectedAccount = this.accounts[0];
                });
            }
            else {
                this.closeModal();
            }
        });
        loginModal.present();
    }
}