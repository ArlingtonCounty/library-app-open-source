import { Component } from '@angular/core';
import { ViewController, NavParams } from 'ionic-angular';
import { CatalogProvider, FacetSet } from './catalogProvider';

@Component({
  selector: 'page-catalog',
  templateUrl: 'catalogFilter.html'
})
export class CatalogFilterPage {
    hideCovers:boolean;
    facets:FacetSet[];
    locations:string[];
    facetModel:any;
    sort:string;

    constructor(public viewCtrl: ViewController, private catalogProvider:CatalogProvider, private navParams:NavParams) { 
      this.hideCovers = this.catalogProvider.getHideCovers() || false;
      this.facetModel = {
        "availability_toggle_arlington": { },
        "format_category_arlington": { },
        "available_at_arlington": { },
        "econtent_source_arlington": { },
        "target_audience": { }
      };
      this.sort = this.catalogProvider.getSort();
    }

    ionViewWillEnter() {
      this.facets = this.navParams.get("facets");
      
      console.log("Facets from filters", this.facets);
      
      //deep copy the object so we can cancel changes
      var copy = this.catalogProvider.getAppliedFacets() || {};
      var keys = Object.keys(copy);
      keys.forEach(key => {
        this.facetModel[key] = copy[key];
      });
    }

    toggleFacet(facet) {
      console.log("toggle", facet);
      facet.expand = !facet.expand;
    }

    onHideCoversChange(ev) {
      this.catalogProvider.setHideCovers(ev);
      this.close();
    }

    onSortChange(ev) {
      this.catalogProvider.setSort(ev);
      this.close();
    }

    onChange(ev){
      this.close();
    }

    close() {
        this.catalogProvider.setFacets(this.facetModel);
        this.catalogProvider.setSort(this.sort);
        this.viewCtrl.dismiss(true);
    }

    reset() {
      console.log("reset called");
      this.facetModel = { 
        "availability_toggle_arlington": { },
        "format_category_arlington": { },
        "available_at_arlington": { },
        "econtent_source_arlington": { },
        "target_audience": { }
      };
      this.catalogProvider.clearFacets();
      this.catalogProvider.clearSort();
      this.sort = this.catalogProvider.getSort();
      
      this.close();
    }

    cancel() {
      this.viewCtrl.dismiss();
    }

    getAppliedFilters() {
      if(!this.facets) return [];

      var returnArray = [];
      
      this.facets.forEach(facet => {
        facet.facets.forEach(f => {
          if(f.isApplied){
            returnArray.push({"facet": facet.label, "value": f.label, "id": facet.filterParam});
          }
        });
      });
      return returnArray;
    }

    removeFacet(facet) {
      this.facetModel[facet.id][facet.value] = null;
      this.close();
    }
}