import { Component } from '@angular/core';

import { AnalyticsProvider } from '../../providers/providers';
import { FaqProvider, FaqItem } from './faqProvider'
import { ITracking } from '../../app/app.component';
import { AppConfig } from '../../config';

@Component({
  selector: 'page-faq',
  templateUrl: 'faq.html'
})
export class FAQPage implements ITracking {
  faqs:FaqItem[];
  title:string;

    constructor(private analytics:AnalyticsProvider, private faqProvider:FaqProvider)
    { 
      this.title = AppConfig.APP_TITLE_HTML;
      this.faqs = [];
    }

    track() {
      this.analytics.trackView('FAQPage');
    }

    ionViewWillEnter() {
      this.faqProvider.getFaqs().subscribe(s => {
      s.forEach(faqItem => {
        this.faqs.push(faqItem);
      })
      console.log(this.faqs);
    })
    }
}