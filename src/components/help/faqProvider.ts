import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { WordpressProvider } from '../../providers/providers';

export class FaqItem {

    constructor(json: any) {
        this.raw = json;
        this.title = json.title.rendered;
        this.content = json.content.rendered;
        this.link = json.link;
        this.id = json.id;
    }

    id: number;
    link: string;
    title: string;
    content: string;
    raw: Object;

}


@Injectable()
export class FaqProvider {

    constructor(private wp: WordpressProvider) { }

    getFaqs(): Observable<FaqItem[]> {
        return this.wp.getFaqs().map(m => {
            let body = m.json();
            let faqItems = new Array<FaqItem>();

            body.forEach(post => {
                faqItems.push(new FaqItem(post));
            });
            return faqItems;
        });
    }
}