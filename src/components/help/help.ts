import { Component } from '@angular/core';
import { EmailComposer } from '@ionic-native/email-composer';
import { NavController, AlertController } from 'ionic-angular';

import { FAQPage, LibrariesPage } from '../components';

import { AnalyticsProvider } from '../../providers/providers';
import { ITracking } from '../../app/app.component';
import { AppConfig } from '../../config';

@Component({
  selector: 'page-help',
  templateUrl: 'help.html'
})
export class HelpPage implements ITracking {

    constructor(private nav:NavController
            , private alertCtrl:AlertController
            , private analytics:AnalyticsProvider
            , private emailComposer:EmailComposer)
    { 

    }

    track() {
      this.analytics.trackView('HelpPage');
    }

    email(address, subject){
        this.emailComposer.isAvailable().then((available: boolean) =>{
            let email = {
                to: address,
                subject: subject,
            };

            // Send a text message using default options
            this.emailComposer.open(email);
        }, error => {

            let alert = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Email is unavailable',
                buttons: ['OK']
            });
            alert.present();
        });
    }

    accountHelp() {
        window.open(AppConfig.HELP_ACCOUNT_URL, "_system")
    }

    feedbackHelp() {
        this.email(AppConfig.HELP_FEEDBACK_EMAIL, 'Library Mobile App Feedback');
    }

    goToFAQs() {
        this.nav.push(FAQPage);
    }

    goToLibraries() {
        this.nav.push(LibrariesPage);
    }
}