import { Component } from '@angular/core';
import { NavParams, ViewController, AlertController, ToastController, LoadingController } from 'ionic-angular';
import { LibcalProxyProvider } from '../../providers/providers';
import * as moment from 'moment';

import { AccountService, LocalAccount } from '../../providers/accountService';
import { AnalyticsProvider } from '../../providers/providers';
import { ITracking } from '../../app/app.component';
import { AppConfig } from '../../config';

import _ from 'lodash';
import 'rxjs/add/operator/switchMap';

@Component({
    selector: 'page-reserve',
    templateUrl: 'reserve.html'
})
export class ReservePage implements ITracking {
    public space;
    public Name: string = "";
    public Email: string = "";
    public showAs: string = "";
    public availableTimes: string;
    public showHelp: boolean = false;

    public accounts:LocalAccount[];
    public selectedAccount:any = -1;

    public selectedTimes = [];

    public eventSource;
    public viewTitle: string;
    public isToday: boolean;
    public calendar = {
        mode: 'month',
        currentDate: new Date()
    };
    title:string;

    constructor(private viewCtrl: ViewController
              , private libcalProxyProvider: LibcalProxyProvider
              , private toastCtrl: ToastController
              , private accountService:AccountService
              , private loadingCtrl: LoadingController
              , private navParams: NavParams
              , public alertCtrl: AlertController
              , private analytics:AnalyticsProvider) {
        this.space = this.navParams.get('space');
        this.title = AppConfig.APP_TITLE_HTML;
    }

    track() {
      this.analytics.trackView('ReservePage');
    }

    dismiss(): void {
        console.log("dismiss called");
        this.viewCtrl.dismiss();
    }

    ionViewWillEnter() {
        this.accountService.getSavedAccounts().subscribe(accounts => {
            this.accounts = accounts;
            if(this.accounts.length > 0) {
                this.selectedAccount = this.accounts[0] || null;
                this.getProfile();
            }
            this.loadEvents();
        });
    }

    ionViewDidLeave() {
        this.clearSelected();
    }

    clearSelected() {
        this.eventSource.forEach(es => {
            es.selected = false;
        })
        this.selectedTimes = []
    }

    select(available) {
        if(this.selectedTimes.length == 4) {
            let alert = this.alertCtrl.create({
                title: 'Reserve Limit!',
                subTitle: 'A maximum of 2 hours can be selected per day',
                buttons: ['OK']
            });
            alert.present();
        }
        else {
            var av = {
                "from": moment(available.startTime).format(),
                "to": moment(available.endTime).format(),
                "newCall": false
            }
            this.selectedTimes.push(av);

            var index = _.findIndex(this.eventSource, {startTime: available.startTime});
            this.eventSource[index].selected = true;
        }
    }

    deselect(available) {
        var from = moment(available.startTime).format();

        var index = _.findIndex(this.selectedTimes, {'from': from});
        this.selectedTimes.splice(index, 1);

        var esIndex = _.findIndex(this.eventSource, {'startTime': available.startTime});
        this.eventSource[esIndex].selected = false;
    }

    reserveSelected() {
        this.selectedTimes.forEach(st => {
            st.newCall = false;
        })
        this.selectedTimes = _.orderBy(this.selectedTimes, ['from']);
        this.selectedTimes[0].newCall = true;

        var apiCalls = 1;
        if(this.selectedTimes.length > 1) {
            for(let i=1; i<this.selectedTimes.length; i++) {
                // check if contiunous time
                if(this.selectedTimes[i].from != this.selectedTimes[i-1].to) {
                    this.selectedTimes[i].newCall = true;
                    apiCalls++;
                }
            }
        }

        var data = this.constructData(apiCalls);
        this.reserveAlert(data);

    }

    constructData(apiCalls) {
        var data = [];
        var indexes = []
        for (i = 0; i < this.selectedTimes.length; i++) {
            if (this.selectedTimes[i].newCall) {
                indexes.push(i);
            }
        }

        for (var i = 0; i < apiCalls; i++) {
            var toindex = indexes[i + 1] || this.selectedTimes.length;

            var to = this.selectedTimes[toindex - 1].to;
            if (apiCalls == 1) {
                to = this.selectedTimes[this.selectedTimes.length - 1].to
            }
            data.push({
                'start': this.selectedTimes[indexes[i]].from,
                'fname': this.Name,
                'lname': " ",
                'nickname': this.showAs,
                'email': this.Email,
                'bookings': [{
                    "id": this.space.id,
                    "to": to
                }]
            })
        }
        return data;
    }

    getProfile() {
        var loading = this.loadingCtrl.create();
        if (this.selectedAccount != -1) {
            loading.present();
            this.accountService.getProfile(this.selectedAccount, true).subscribe(s => {
                console.log("Get Profile Complete", s);
                this.Name = s.fullname;
                this.Email = s.email;
                loading.dismissAll();
            })
        }
        else {
            this.Name = "";
            this.Email = "";
        }
    }

    toggleHelp() {
        this.showHelp = this.showHelp ? false : true;
    }

    presentToast(message) {
        let toast = this.toastCtrl.create({
            message: message,
            showCloseButton: true,
            closeButtonText: 'ok'
        });
        toast.present();
    }

    reserve(data) {
        data.forEach(d => {
            var start = moment(d.start).format('M-D-YY h:mm')
            var end = moment(d.bookings[0].to).format('h:mm a')
            var startTime = new Date(d.start)
            var endTime = new Date(d.bookings[0].to)
            var startIndex = _.findIndex(this.eventSource, {startTime: startTime});
            var endIndex = _.findIndex(this.eventSource, {endTime: endTime});

            this.libcalProxyProvider.reserveSpace(d).subscribe(
                r => {
                    if(r.booking_id) {
                        this.presentToast("Meeting room reserved " + start + ' - ' + end)
                        this.failOrReserve(startIndex, endIndex, true)
                    }
                    else {
                        var error = ''
                        if(r.errors) {
                            error = r.errors[0].error;
                        }
                        this.presentToast("Failed to reserve " + start + ' - ' + end + '. ' + error)
                        this.failOrReserve(startIndex, endIndex, false)
                    }
                },
                err => {
                    this.presentToast("Failed to reserve " + start + ' - ' + end)
                    this.failOrReserve(startIndex, endIndex, false)
                })
        })
    }

    failOrReserve(startIndex, endIndex, reserved) {
        for (var i = startIndex; i <= endIndex; i++) {
            if(reserved) {
                this.eventSource[i].reserved = true;
            }
            else {
                this.eventSource[i].failed = true;
            }
        }
        this.clearSelected();
    }

    reserveAlert(data) {
        if(this.Email == "" || this.Name == "" || this.showAs == "") {
            let alert = this.alertCtrl.create({
                title: 'Information Required!',
                subTitle: "Please enter a Name, Email, and 'Show as' (how to be displayed on the public calendar)",
                buttons: ['OK']
            });
            alert.present();
        }
        else {
            var times = "";
            data.forEach(d => {
                var start = moment(d.start).format('MMM D h:mm')
                var end = moment(d.bookings[0].to).format('h:mm a')
                times += `<li>${start} - ${end}</li>`
            })

            let confirm = this.alertCtrl.create({
                title: 'Reserve this time?',
                message: `This room will be reserved for the following duration. A maximum of 2 hours can be reserved per day. <br><br> <ul>${times}</ul>`,
                buttons: [
                    {
                        text: 'Cancel',
                        handler: () => {
                            console.log('Cancel clicked');
                        }
                    },
                    {
                        text: 'Yes',
                        handler: () => {
                            console.log('yes clicked');
                            this.reserve(data);
                        }
                    }
                ]
            });
            confirm.present();
        }
    }

    loadEvents() {
        var events = [];

        this.space.availability.forEach(space => {
            var startTime = new Date(space.from)
            var endTime = new Date(space.to)

            events.push({
                title: 'Event',
                startTime: startTime,
                endTime: endTime,
                allDay: false,
                selected: false,
                reserved: false,
                failed: false
            })
        })
        this.eventSource = events;
    }

    onViewTitleChanged(title) {
        this.viewTitle = title;
    }

    onEventSelected(event) {
        console.log('Event selected:' + event.startTime + '-' + event.endTime + ',' + event.title);
    }

    onTimeSelected(ev) {
        console.log('Selected time: ' + ev.selectedTime + ', hasEvents: ' +
            (ev.events !== undefined && ev.events.length !== 0) + ', disabled: ' + ev.disabled);
            var date = moment(ev.selectedTime).format('MMM D')
            this.availableTimes = "Available Times for " + date;
            this.clearSelected();
    }

    onCurrentDateChanged(event:Date) {
        var today = new Date();
        today.setHours(0, 0, 0, 0);
        event.setHours(0, 0, 0, 0);
        this.isToday = today.getTime() === event.getTime();
    }

    onRangeChanged(ev) {
        console.log('range changed: startTime: ' + ev.startTime + ', endTime: ' + ev.endTime);
    }
}
