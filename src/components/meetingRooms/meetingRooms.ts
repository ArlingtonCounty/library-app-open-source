import { Component } from '@angular/core';
import { NavController, LoadingController, AlertController } from 'ionic-angular';

import { MeetingRoomsProvider } from './meetingRoomsProvider';
import { CategoriesPage } from './category';

import { AnalyticsProvider } from '../../providers/providers';
import { ITracking } from '../../app/app.component';
import { AppConfig } from '../../config';

@Component({
  selector: 'page-meetingrooms',
  templateUrl: 'meetingRooms.html'
})
export class MeetingRoomsPage implements ITracking {
    locations:any[];
    title:string;

    constructor(private provider:MeetingRoomsProvider
            , private navCtrl:NavController
            , private loadingCtrl:LoadingController
            , private analytics:AnalyticsProvider
            , private alertCtrl:AlertController) {
        this.locations = [];
        this.title = AppConfig.APP_TITLE_HTML;
    }

    track() {
      this.analytics.trackView('MeetingRoomsPage');
    }

    ionViewWillEnter() {
        let loader = this.loadingCtrl.create();
        loader.present();
        this.provider.getLocations().subscribe(s => {
            this.locations = s;
            
            // remove large meeting rooms
            for(var i=0; i<this.locations.length; i++) {
                if(this.locations[i].name.toLowerCase().indexOf('large') >= 0) {
                    this.locations.splice(i, 1);
                }
            }

            loader.dismissAll()
        }, error => {
            loader.dismissAll()
            let alert = this.alertCtrl.create({
                title: 'Error',
                message: 'Unable to retrieve data.  Check your internet connection.',
                buttons: ['OK']
            });
            alert.present();
        });
    }

    goToSpaces(id:string){
        console.log("go to category", id);
        this.navCtrl.push(CategoriesPage, { locationId: id });
    }
}