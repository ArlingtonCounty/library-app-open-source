import { Component } from '@angular/core';
import { NavParams, ModalController, LoadingController } from 'ionic-angular';
import { MeetingRoomsProvider } from './meetingRoomsProvider';
import { ReservePage } from './reserve';

import _ from 'lodash';
import { AnalyticsProvider } from '../../providers/providers';
import { ITracking } from '../../app/app.component';
import { AppConfig } from '../../config';

@Component({
    selector: 'page-categories',
    templateUrl: 'categories.html'
})
export class CategoriesPage implements ITracking {
    categories: any[]
    locationName: string;
    categoryId: number;
    spaces: any[];
    title:string;

    constructor(private meetingRoomsProvider: MeetingRoomsProvider,
        public modalCtrl: ModalController,
        private loadingCtrl:LoadingController,
        private navParams: NavParams, 
        private analytics:AnalyticsProvider) {
        this.title = AppConfig.APP_TITLE_HTML;
        this.spaces = [];
    }

    track() {
      this.analytics.trackView('CategoriesPage');
    }

    ionViewWillEnter() {
        let loader = this.loadingCtrl.create();
        loader.present()
        var locationId = this.navParams.get('locationId');
        this.meetingRoomsProvider.getCategories(locationId).subscribe(s => {
            this.locationName = s[0].name;
            this.categories = s[0].categories;
            this.categories.forEach(item => {
                if (item.cid === 416 || item.cid === 417 || item.cid === 418) {
                    this.categoryId = item.cid
                }
            });
            console.log(this.categories, this.categoryId, s)
        },
            err => console.log(err),
            () => {
                this.loadSpaces();
                loader.dismissAll();
            }
        );
    }

    loadSpaces(space?) {
        this.meetingRoomsProvider.getSpaces(this.categoryId).subscribe(s => {
            this.spaces = s[0].items;
            this.spaces.forEach(element => {
                var slashes = element.image.indexOf("//")
                if (slashes === 0) {
                    element.image = 'http://' + element.image.substring(2);
                }
                element.show = false;
            });
            if(space) {
                var spaceIndex = _.findIndex(this.spaces, {name: space.name})
                this.spaces[spaceIndex].show = true;
            }
        });
    }

    openModal(space) {
        let profileModal = this.modalCtrl.create(ReservePage, { space: space });
        profileModal.onDidDismiss(() => {

            this.loadSpaces(space);
        });
        profileModal.present();
    }

}