import { Injectable } from '@angular/core';

import { LibcalProxyProvider } from '../../providers/providers';

@Injectable()
export class MeetingRoomsProvider {
    
    constructor(private libcalProxy:LibcalProxyProvider) {
        console.log('meeting room provider constructor');
    }

    getLocations() {
        return this.libcalProxy.getLocations().map(m =>{
            return m;
        });
    }

    getCategories(locationId:number) {
        return this.libcalProxy.getCategories(locationId).map(m => {
            return m;
        });
    }

    getSpaces(categoryId:number) {
        return this.libcalProxy.getSpaces(categoryId).map(m => {
            return m;
        });
    }

}