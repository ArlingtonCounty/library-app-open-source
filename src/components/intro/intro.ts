import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';
import { HomePage } from '../components';
import { StorageProvider } from '../../providers/providers';
import { ITracking } from '../../app/app.component';
import { AnalyticsProvider } from '../../providers/providers';
import { AppConfig } from '../../config';

@Component({
    selector: 'page-intro',
    templateUrl: 'intro.html'
})
export class IntroPage implements ITracking {
    slides = []
    constructor(private analytics:AnalyticsProvider, public navCtrl: NavController, private storageProvider: StorageProvider, private menuCtrl: MenuController) {

        this.slides = [
            {
                title: "<strong>Library to go!</strong>",
                description: `Use popular library services on your mobile device with the ${AppConfig.APP_NAME}.`,
                image: "assets/icon/tutorialicon_1.png",
            },
            {
                title: "",
                description: "Search the catalog or scan a barcode to find a title in the Library’s collection.",
                image: "assets/icon/tutorialicon_2.png",
            },
            {
                title: "",
                description: "Connect your library card to the app for quick account access. Families can add multiple accounts",
                image: "assets/icon/tutorialicon_3.png",
            },
            {
                title: "",
                description: "Choose to receive alerts to your device for holds, overdues and fines.",
                image: "assets/icon/tutorialicon_4.png",
            },
            {
                title: "",
                description: `<ul>
                                <li>Find Library locations and hours </li>
                                <li>Book a small meeting room</li>
                                <li>Browse news and events</li>
                                <li>Discover lists of new and popular titles</li>
                              </ul>`,
                image: "assets/icon/tutorialicon_5.png",
            }
        ];
    }

    track() {
        this.analytics.trackView('IntroPage');
      }

    navHome() {
        this.storageProvider.updateShowIntro(false).subscribe(s=> {
            console.log("Go To Home");
            this.navCtrl.setRoot(HomePage);
        });
    }

    ionViewWillEnter() {
        this.menuCtrl.enable(false);
    }

    ionViewWillLeave() {
        this.menuCtrl.enable(true);
    }

}